import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../shared/patientService.service';
import { Chest } from '../../../shared/chest';
import { AlertsService } from './../../../shared/alerts.service';
import { FieldCleanup } from './../../../shared/AllergyFactors/fieldcleanup';

@Component({
  selector: 'app-patient-chest-details',
  templateUrl: './patient-chest-details.component.html',
  styleUrls: ['./patient-chest-details.component.css']
})
export class PatientChestDetailsComponent implements OnInit {

  wheezOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedWheezOption = false;

  breathOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedBreathOption = false;

  chestPainOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedChestPainOption = false;

  loading = false;


  wheezingDuringBreath = [{ 'value': 'While breathing in', 'name': 'While breathing in' }, { 'value': 'While breathing out', 'name': 'While breathing out' }, { 'value': 'Through Out', 'name': 'Through Out' }];
  wheezingDuringTime = [{ 'value': 'Morning', 'name': 'Morning' }, { 'value': 'Afternoon', 'name': 'Afternoon' }, { 'value': 'Evening', 'name': 'Evening' }, { 'value': 'Night', 'name': 'Night' }];
  selectedWheez = '';
  selectedWheezTime = '';

  patientChestForm: FormGroup;
  wheezingType: string;
  wheezingTime: string;
  chestPainArea: string;
  breathlessnessTime: string;

  constructor(public patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.patientChestForm = this.formBuilder.group({
      Wheezing: this.formBuilder.control('', Validators.required),
      WheezingType: this.formBuilder.control('', Validators.required),
      WheezingTime: this.formBuilder.control('', Validators.required),
      Breathlessness: this.formBuilder.control('', Validators.required),
      BreathlessnessTime: this.formBuilder.control('', Validators.required),
      ChestPain: this.formBuilder.control('', Validators.required),
      ChestPainArea: this.formBuilder.control('', Validators.required),

    });

    this.selectedWheezOption = this.patientService.patientRespiratoryDetails['Wheezing'];
    this.selectedBreathOption = this.patientService.patientRespiratoryDetails['Breathlessness'];
    this.selectedChestPainOption = this.patientService.patientRespiratoryDetails['ChestPain'];
    this.wheezingType = this.patientService.patientRespiratoryDetails['WheezingType'];
    this.wheezingTime = this.patientService.patientRespiratoryDetails['WheezingTime'];
    this.chestPainArea = this.patientService.patientRespiratoryDetails['ChestPainArea'];
    this.breathlessnessTime = this.patientService.patientRespiratoryDetails['BreathlessnessTime'];
  }

  submitPatientChestDetailsForm(form) {
    this.loading = true;
    let respiratory = form.value;
    // let patientChestDetails = this.patientService.patientRespiratoryDetails;

    let chest = new Chest();
    if (this.selectedWheezOption == true) {
      chest.Wheezing = this.patientChestForm.get('Wheezing').value;
      chest.WheezingType = this.patientChestForm.get('WheezingType').value;
      chest.WheezingTime = this.patientChestForm.get('WheezingTime').value;
      chest.Breathlessness = this.patientChestForm.get('Breathlessness').value;
      chest.BreathlessnessTime = this.patientChestForm.get('BreathlessnessTime').value;
      chest.ChestPain = this.patientChestForm.get('ChestPain').value;
      chest.ChestPainArea = this.patientChestForm.get('ChestPainArea').value;

      // this.patientService.patientRespiratoryDetails.Wheezing = form.value.Wheezing;
      // this.patientService.patientRespiratoryDetails.WheezingType = form.value.WheezingType;
      // this.patientService.patientRespiratoryDetails.WheezingTime = form.value.WheezingTime;
      // this.patientService.patientRespiratoryDetails.Breathlessness = form.value.Breathlessness;
      // this.patientService.patientRespiratoryDetails.BreathlessnessTime = form.value.BreathlessnessTime;
      // this.patientService.patientRespiratoryDetails.ChestPain = form.value.ChestPain;
      // this.patientService.patientRespiratoryDetails.ChestPainArea = form.value.ChestPainArea;
      this.patientService.patientRespiratoryDetails.IsActive = true;
      let apiData = this.patientService.patientRespiratoryDetails;
      delete apiData.RespiratoryId;
      var respiratoryDetails = Object.assign(apiData, chest)

      this.patientService.createChestRespiratorySystem(apiData).subscribe(response => {
        console.log('Patient Chest Details Response', response);
        this.patientService.patientRespiratoryDetails = response;
        this.loading = false;
        this.alertsService.showToast(true);
      },
        error => {
          //alert(error);
        });

    }

  }


}
