export class PlantTypes {
    BotanicalId : number;
    PlantName : string;
    Description : string;
    TypeOfPlants : string;
    StateId : number;
    State : string;
    CountryId : number;
    Country : string;
    imageUrl : string;
}
