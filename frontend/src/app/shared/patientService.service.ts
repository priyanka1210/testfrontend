import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { PatientDemographicsDetails } from './patient-demographics-details';
import { Observable, throwError } from 'rxjs';
// import 'rxjs/add/operator/map';
import { PatientDetails } from './../shared/patient-details';
import { AllergyDetails } from './allergy-details';
import { AllergyHistory } from './allergy-history';
import { PhysicalDetails } from './physical-details';
import { Chest } from './chest';
import { Habits } from "./habits";
import { Blood } from './blood';
import { Nativity } from './nativity';
import { DietDetails } from './diet-details';
import { GastroIntestine } from './gastro-intestine';
import { map, tap, retry, catchError } from 'rxjs/operators';
import { FemaleInfo } from './female-info';
import { ENTDetails } from './entdetails';
import { Tests } from './tests';
import { SkinDetails } from './skin-details';
import { Bowel } from './bowel';
import { HairDetails } from './hair-details';
import { PastPresentHistoryDetails } from './past-present-history-details';
import { NonAllergicGerdDetails } from './non-allergic-gerd-details';
// export class FanalInitialAdviseDetails {
import { PrickTest } from './prick-test';
import { FanalInitialAdviseDetails } from './fanal-initial-advise-details';

@Injectable({
  providedIn: 'root'
})
export class PatientService implements OnInit {

  public patientDetails: PatientDemographicsDetails;
  public patientDemographicsDetails = new PatientDemographicsDetails();
  public patientPhysicalDetails = new PhysicalDetails();
  public patientAllergyDetails = new AllergyDetails();
  public patientAllergyHistoryDetails = new AllergyHistory();
  public patientRespiratoryDetails = new Chest();

  public patientHabitDetails = new Habits();

  public patientBloodDetails = new Blood();

  public patientNativityDetails = new Nativity();

  public patientDietDetails = new DietDetails(1, 1, "", "", "", "", "", "");
  public patientGastroIntestinalDetails = new GastroIntestine();
  public patientFemaleInfoDetails = new FemaleInfo();
  public patientENTDetails = new ENTDetails();
  public patientTestsDetails = new Tests();
  public patientSkinDetails = new SkinDetails();
  public patientBowelDetails = new Bowel();
  public patientHairDetails = new HairDetails();
  public patientPastPresentHistoryDetails = new PastPresentHistoryDetails();
  public patientNonAllergicGerdDetails = new NonAllergicGerdDetails();
  public patientFanalInitialAdviseDetails = new FanalInitialAdviseDetails();
  public patientPrickTest = new PrickTest();
  public allergyGroupIdToGroupName = new Map();     // Group Id to Group Name


  public allergyDataIdToDataName = new Map();        // Data Id to Data Name


  public allergyDataNameToDataId = new Map();       // Group DataName to Data Id

  public allergyDataNameToGroupId = new Map();       //data name to group id

  public WritePatient: PatientDetails;
  public foundPatient: PatientDetails;

  // private base_Url: string = 'http://52.163.247.33:8586';
  // private searchPatient_Url: string = 'http://52.163.247.33:8586/Patient';
  // private addPatientUrl: string = 'http://52.163.247.33:8586/Patient/AddPatient';
  // private getMasterAllergyDataId : string = 'http://52.163.247.33:8586/MasterAllergyData/GetAllMasterAllergyDataItems';

  // Azure APIs

  private Azure_Base_Url: string = "https://functionapp20190515071947.azurewebsites.net";
  private Azure_API_Code: string = "?code=ZLhkBB/iAaTaKr/BFfvP6GtNQnQmHTDZJYtBYlgf7pppfCDPx7Hv0w==";


  constructor(private http: HttpClient) {
    this.WritePatient = new PatientDetails();
    //this.WritePatient.Habits = new Habits();
  }

  // Patient Related Methods
  // getAllPatientDetails() {


  // }


  ngOnInit() {

  }


  searchPatientDetails(PhoneNumber: number): Observable<PatientDetails> {
    //alert(this.Azure_Base_Url + '/Patient' + '/' + PhoneNumber + this.Azure_API_Code);
    return this.http.get<PatientDetails>(this.Azure_Base_Url + '/Patient' + '/' + PhoneNumber + this.Azure_API_Code)
      .pipe(
        catchError(this.handleError)
      );
  }

  createPatientDetails(patientDemographics, patientPhysicalDetails): Observable<PatientDetails> {
    var dataToSend = {};
    console.log("patientPhysicalDetails in servise=======", patientPhysicalDetails);
    dataToSend["PatientDemographics"] = patientDemographics;
    dataToSend["PatientPhysicalDetails"] = patientPhysicalDetails;
    //console.log(JSON.stringify(dataToSend));
    //alert(this.Azure_Base_Url + '/Patient' + '/AddPatient' + this.Azure_API_Code);
    return this.http.post<PatientDetails>(this.Azure_Base_Url + '/Patient' + '/AddPatient' + this.Azure_API_Code, dataToSend)
      .pipe(
        catchError(this.handleError)
      );
  }

  handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.status === 409) {
      console.log('Client Side Error');
      // alert("This phone number is already registered with iDoctor.")
    } else if (error.status === 500) {
      console.log('Server Side Error');
    }
    return throwError(error);
  }
  updatePatientContactDetails(patientDemographics): Observable<PatientDemographicsDetails> {
    var dataToSend = {};
    dataToSend["PatientDemographics"] = patientDemographics;

    // dataToSend["PatientPhysicalDetails"] = patientPhysicalDetails;
    alert(this.Azure_Base_Url + '/Patient' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePatient' + this.Azure_API_Code)
    return this.http.put<PatientDemographicsDetails>(this.Azure_Base_Url + '/Patient' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePatient' + this.Azure_API_Code, dataToSend);

  }
  updatePatientPhysicalDetails(patientDemographics, patientPhysicalDetails): Observable<PatientDetails> {
    var dataToSend = {};
    dataToSend["PatientDemographics"] = patientDemographics;
    dataToSend["PatientPhysicalDetails"] = patientPhysicalDetails;
    // alert(this.Azure_Base_Url + '/Patient'+ '/' + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePatient' + this.Azure_API_Code);
    return this.http.put<PatientDetails>(this.Azure_Base_Url + '/Patient' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePatient' + this.Azure_API_Code, dataToSend);

  }
  createAllergyFactors(allergyFactors): Observable<AllergyDetails> {
    return this.http.post<AllergyDetails>(this.Azure_Base_Url + '/AllergyFactors' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'allergyFactorData' + this.Azure_API_Code, allergyFactors);
  }

  updateAllergyFactors(allergyFactors): Observable<AllergyDetails> {
    var dataToSend = {};
    dataToSend["AllergyFactors"] = allergyFactors;
    // alert(this.Azure_Base_Url + '/AllergyFactors' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'allergyFactorData' + this.Azure_API_Code);
    return this.http.put<AllergyDetails>(this.Azure_Base_Url + '/AllergyFactors' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'allergyFactorData' + this.Azure_API_Code, dataToSend);
  }

  createAllergyHistory(allergyHistory): Observable<AllergyHistory> {
    var dataToSend = {};
    // dataToSend["AllergyHistory"] = allergyHistory;
    dataToSend = allergyHistory;
    // alert(this.Azure_Base_Url + '/AllergyHistory' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'UpdateAllergyHistory' + this.Azure_API_Code);
    //for creating allergy history PUT is used in the API
    return this.http.put<AllergyHistory>(this.Azure_Base_Url + '/AllergyHistory' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'UpdateAllergyHistory' + this.Azure_API_Code, dataToSend);
  }


  createChestRespiratorySystem(chestRespiratoryDetails): Observable<Chest> {
    // alert(this.Azure_Base_Url + '/Respiratory' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'RespiratoryData' + this.Azure_API_Code);
    return this.http.put<Chest>(this.Azure_Base_Url + '/Respiratory' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/' + 'RespiratoryData' + this.Azure_API_Code, chestRespiratoryDetails);
  }

  updatePatientHabitDetails(patientHabits): Observable<Habits> {
    console.log("patientHabits=== in patient serveise",patientHabits);
    // alert(this.Azure_Base_Url + '/PatientHabit' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/PatientHabitData' + this.Azure_API_Code);
    return this.http.put<Habits>(this.Azure_Base_Url + '/PatientHabit' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/PatientHabitData' + this.Azure_API_Code, patientHabits);
    // http://localhost:8586/PatientHabit/{PatientDemographicsId}/PatientHabitData
  }

  updatePatientBloodDetails(patientBloodDetails): Observable<Blood> {
    console.log("patientBloodDetails====", patientBloodDetails);
    // alert(this.Azure_Base_Url + '/BloodTransfer' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/BloodTransfer' + this.Azure_API_Code);
    return this.http.put<Blood>(this.Azure_Base_Url + '/BloodTransfer' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/BloodTransfer' + this.Azure_API_Code, patientBloodDetails);
    // http://localhost:8586/BloodTransfer/{PatientDemographicsId}/DietSleepData
  }

  updatePatientNativityDetails(patientNativityDetails): Observable<Blood> {
    // console.log("patientBloodDetails====", patientBloodDetails);
    // alert(this.Azure_Base_Url + '/BloodTransfer' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/BloodTransfer' + this.Azure_API_Code);
    return this.http.put<Blood>(this.Azure_Base_Url + '/NativityDomilcile' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/NativityDomilcileData' + this.Azure_API_Code, patientNativityDetails);
    // http://localhost:8586/NativityDomilcile/{PatientDemographicsId}/NativityDomilcileData

  }

  updatePatientDietDetails(patientDietDetails): Observable<DietDetails> {
    // console.log("patientBloodDetails====", patientBloodDetails);
    // alert(this.Azure_Base_Url + '/BloodTransfer' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/BloodTransfer' + this.Azure_API_Code);
    return this.http.put<DietDetails>(this.Azure_Base_Url + '/DietSleep' + '/' + this.patientDemographicsDetails.PatientDemographicsId + '/DietSleepData' + this.Azure_API_Code, patientDietDetails);
    // http://localhost:8586/NativityDomilcile/{PatientDemographicsId}/NativityDomilcileData

  }

  updatePatientGastroIntestinalDetails(patientGastroIntestinalDetailsDetails): Observable<GastroIntestine> {
    return this.http.put<GastroIntestine>(
      this.Azure_Base_Url + '/GastroIntestinal' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/GastroIntestinalData'
      + this.Azure_API_Code, patientGastroIntestinalDetailsDetails);
  }

  updatepatientFemaleInfoDetails(patientFemaleInfoDetails): Observable<FemaleInfo> {
    return this.http.put<FemaleInfo>(
      this.Azure_Base_Url + '/FemaleInfo' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/FemaleInfoData'
      + this.Azure_API_Code, patientFemaleInfoDetails);
  }

  updatepatientENTDetails(patientENTDetails): Observable<ENTDetails> {
    return this.http.put<ENTDetails>(
      this.Azure_Base_Url + '/ENT' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/ENTData'
      + this.Azure_API_Code, patientENTDetails);
  }

  updatepatientTests(patientTestsDetails): Observable<Tests> {
    return this.http.put<Tests>(
      this.Azure_Base_Url + '/PatientTest' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/PatientTestData'
      + this.Azure_API_Code, patientTestsDetails);
  }

  updatepatientSkinDetails(patientSkinDetails): Observable<SkinDetails> {
    return this.http.put<SkinDetails>(
      this.Azure_Base_Url + '/Skin' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/SkinData'
      + this.Azure_API_Code, patientSkinDetails);
  }

  updatePatientBowelDetails(patientBowelDetails): Observable<Bowel> {
    return this.http.put<Bowel>(
      this.Azure_Base_Url + '/Bowel' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdateBowelDetails'
      + this.Azure_API_Code, patientBowelDetails);
  }

  updatePatientHairDetails(patientHairDetails): Observable<HairDetails> {
    return this.http.put<HairDetails>(
      this.Azure_Base_Url + '/HairHeadache' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdateHairHeadache'
      + this.Azure_API_Code, patientHairDetails);
  }

  updatePatientPastPresentHistoryDetails(patientPastPresentHistoryDetails): Observable<PastPresentHistoryDetails> {
    return this.http.put<PastPresentHistoryDetails>(
      this.Azure_Base_Url + '/PastHistory' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePastHistory'
      + this.Azure_API_Code, patientPastPresentHistoryDetails);
    // http://localhost:7071/PastHistory/{PatientDemographicsId}/UpdatePastHistory
  }

  updatePatientNonAllergicGerdDetails(patientNonAllergicGerdDetails): Observable<NonAllergicGerdDetails> {
    return this.http.put<NonAllergicGerdDetails>(
      this.Azure_Base_Url + '/PresentNonAllergic' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePresentNonAllergic'
      + this.Azure_API_Code, patientNonAllergicGerdDetails);
  }
  // http://localhost:7071/PresentNonAllergic/{PatientDemographicsId}/UpdatePresentNonAllergic

  updatePatientFanalInitialAdviseDetails(patientFanalInitialAdviseDetails): Observable<FanalInitialAdviseDetails> {
    return this.http.put<FanalInitialAdviseDetails>(
      this.Azure_Base_Url + '/PatientDiagnosticsDetail' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePatientDiagnosticsDetail'
      + this.Azure_API_Code, patientFanalInitialAdviseDetails);
  }
  //http://localhost:7071/PatientDiagnosticsDetail/{PatientDemographicsId}/UpdatePatientDiagnosticsDetail

  updatepatientPrickTest(patientPrickTest): Observable<PrickTest> {
    console.log("patientPrickTest====", patientPrickTest);
    return this.http.put<PrickTest>(
      this.Azure_Base_Url + '/PrickTest' + '/'
      + this.patientDemographicsDetails.PatientDemographicsId + '/UpdatePrickTest'
      + this.Azure_API_Code, patientPrickTest);
  }
  // http://localhost:7071/PrickTest/{PatientDemographicsId}/UpdatePrickTest (edited) 

  // http://localhost:7071/GetPatientList/{Skip}/{Take}
  // getPatientList():
  getAllPatientDetails() {
    var url = this.Azure_Base_Url + "/GetPatientList/0/50" + this.Azure_API_Code;
    // alert(url)
    return this.http.get(url);
  }

  getAllMasterAllergyDataGroupNames() {
    var url = this.Azure_Base_Url + "/MasterAllergyDataGroup/GetAllMasterAllergyDataGroupNames" + this.Azure_API_Code;
    return this.http.get(url);
  }
  // http://localhost:8586/MasterAllergyDataGroup/{MasterAllergyDataGroupName}

  getAllMasterAllergyDataItems() {
    var url = this.Azure_Base_Url + "/MasterAllergyData/GetAllMasterAllergyDataItems" + this.Azure_API_Code;
    return this.http.get(url);
  }



}
