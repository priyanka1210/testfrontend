export class Tests {
    /*[Key]*/
    public PatientTestId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public BloodTest: boolean;
    public BloodTestDetails: string;
    public CTScan: boolean;
    public CTScanDetails: string;
    public CytologyTest: boolean;
    public CytologyTestDetails: string;
    public HigherTest: boolean;
    public HigherTestDetails: string;
    public LungFunctionalTest: boolean;
    public LungFunctionalTestDetails: string;
    public MRITest: boolean;
    public MRITestDetails: string;
    public PeakFlowTest: boolean;
    public PeakFlowTestDetails: string;
    public StoolTest: boolean;
    public StoolTestDetails: string;
    public UrineTest: boolean;
    public UrineTestDetails: string;
    public XRayTest: boolean;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    public XRayTestDetails: string;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: String;
}
