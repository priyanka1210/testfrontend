export class FanalInitialAdviseDetails {
     //1
     // InitialAdviseDetails:string;

     // //2
     // Immunotherapy:string;
     // Medication:string;
     // General:string;
 
     //3
     // FollowupDetails:string;

     PatientDiagnosticsDetailId : number;
     SpecialPointsDetails : string;
     InitialAdviceDetails : string;
     FollowupDetails : string;
     Immunotheraphy: string;
     Medication: string;
     General : string;
     PrognosisDetails : string;
     ProvisionalDiagnosisDetails: string;
     CreatedByUserId:number;
     CreatedDateTime: Date;
     IsActive : boolean;
     PatientDemographicsId : number;
     PatientDemographics : string;
}
// public int PatientDiagnosticsDetailId { get; set; }
// public string SpecialPointsDetails { get; set; }
// public string InitialAdviceDetails { get; set; }
// public string FollowupDetails { get; set; }
// public string Immunotheraphy { get; set; }
// public string Medication { get; set; }
// public string General { get; set; }
// public string PrognosisDetails { get; set; }
// public string ProvisionalDiagnosisDetails { get; set; }
// public int CreatedByUserId { get; set; }
// public DateTime CreatedDateTime { get; set; }
// public bool IsActive { get; set; }
// [Required]
// public int PatientDemographicsId { get; set; }
// [ForeignKey("PatientDemographicsId")]
// public PatientDemographics PatientDemographics { get; set; }