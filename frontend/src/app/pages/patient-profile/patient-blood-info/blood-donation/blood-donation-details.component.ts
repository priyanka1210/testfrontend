import { OnInit, Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'app-blood-donation-details',
    templateUrl: './blood-donation-details.component.html',
    styleUrls: ['./blood-donation-details.component.css']
})


export class BloodDonationDetailsComponent implements OnInit {
    bloodDonationOptions = [
        { value: true, name: 'YES' },
        { value: false, name: 'NO' },
    ];

    bloodDonationImpactOptions = [
        { value: true, name: 'YES' },
        { value: false, name: 'NO' },
    ];

    bloodReceiveOptions = [
        { value: true, name: 'YES' },
        { value: false, name: 'NO' },
    ];

    bloodRecieveImpactOptions = [
        { value: true, name: 'YES' },
        { value: false, name: 'NO' },
    ];

    selectedBloodDonation = false;
    bloodDonationForm: FormGroup;
    noofTimesBloodDonated: number;
    bloodDonatedDate: any;

    selectedBloodDonationImpact = false;
    bloodDonationProblem: string;

    selectedBloodRecievedImpact = false;
    selectedBloodReceive = false;
    noofTimesBloodReceived: number;
    bloodReceiveDate: any;
    bloodReceiveProblem: string;

    constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private datePipe: DatePipe, private alertsService: AlertsService) { }

    loading = false;
    ngOnInit() {
        this.bloodDonationForm = this.FormBuilder.group({
            BloodDonation: this.FormBuilder.control('', Validators.required),
            NoofTimesBloodDonated: this.FormBuilder.control('', Validators.required),
            BloodDonationDate: this.FormBuilder.control('', Validators.required),
            DonationImpact: this.FormBuilder.control('', Validators.required),
            BloodDonationProblem: this.FormBuilder.control('', Validators.required),

            BloodReceive: this.FormBuilder.control('', Validators.required),
            NoofTimesBloodReceived: this.FormBuilder.control('', Validators.required),
            BloodReceiveDate: this.FormBuilder.control('', Validators.required),
            RecievedImpact: this.FormBuilder.control('', Validators.required),
            BloodReceiveProblem: this.FormBuilder.control('', Validators.required),

        })
        this.noofTimesBloodDonated = this.patientService.patientBloodDetails.NoofTimesBloodDonated;
        this.bloodDonatedDate = this.patientService.patientBloodDetails.BloodDonatedDate;
        if (this.bloodDonatedDate != null)
            this.bloodDonatedDate = this.bloodDonatedDate.toString().substr(0, 10);
        this.bloodDonationProblem = this.patientService.patientBloodDetails.BloodDonationProblems;
        this.selectedBloodDonation = this.patientService.patientBloodDetails.BloodDonated;
        this.selectedBloodDonationImpact = this.patientService.patientBloodDetails.BloodDonationImpact;

        this.noofTimesBloodReceived = this.patientService.patientBloodDetails.NoofTimesBloodReceived;
        this.bloodReceiveDate = this.patientService.patientBloodDetails.BloodReceivedDate;
        if (this.bloodDonatedDate != null) {
            //this.bloodReceiveDate = this.bloodReceiveDate.split("T");
            this.bloodReceiveDate = this.bloodReceiveDate.toString().substr(0, 10);

            //this.bloodReceiveDate = this.bloodReceiveDate[0];
        }
        this.bloodReceiveProblem = this.patientService.patientBloodDetails.BloodReceivingProblems;
        this.selectedBloodReceive = this.patientService.patientBloodDetails.BloodReceived;
        this.selectedBloodRecievedImpact = this.patientService.patientBloodDetails.BloodReceivedImpact;
    }


    submitBloodDonationForm(form) {
        this.loading = true;
        let patientBloodDetails = form.value;
        if (form.value.BloodDonationDate != null || form.value.BloodDonationDate != undefined || form.value.BloodDonationDate != "") {
            patientBloodDetails.BloodDonatedDate = this.datePipe.transform((form.value.BloodDonationDate), 'yyyy-MM-dd');
            this.patientService.patientBloodDetails['BloodDonatedDate'] = patientBloodDetails.BloodDonatedDate;

        }
        else {
            patientBloodDetails.BloodDonatedDate = "2019-11-11"
        }
        this.patientService.patientBloodDetails['NoofTimesBloodDonated'] = form.value.NoofTimesBloodDonated;
        this.patientService.patientBloodDetails['BloodDonated'] = form.value.BloodDonation;
        this.patientService.patientBloodDetails['BloodDonationImpact'] = form.value.DonationImpact;
        this.patientService.patientBloodDetails['BloodDonationProblems'] = form.value.BloodDonationProblem;
        patientBloodDetails.IsActive = true;
        let apiData = this.patientService.patientBloodDetails;
        delete apiData.BloodTransferId;
        console.log("api data=====", apiData);
        this.patientService.updatePatientBloodDetails(apiData).subscribe(response => {
            console.log("responce===", response);
            // this.patientService.patientHabitDetails = response;//response["patientDemographics"];
            this.loading = false;
            this.alertsService.showToast(true);
        })
    }

}