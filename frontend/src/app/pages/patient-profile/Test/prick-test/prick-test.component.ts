import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';
import { PrickTest } from './../../../../shared/prick-test';

@Component({
  selector: 'app-prick-test',
  templateUrl: './prick-test.component.html',
  styleUrls: ['./prick-test.component.css']
})
export class PrickTestComponent implements OnInit {

  prickTestForm: FormGroup;
  public rows: FormArray;
  // test: string;

  // "[{"Name":"ggy","Intensity":"gg","sign":"ghvgh","pseudocode":"hgvhgv"}]", CreatedByUserId: 0, CreatedDateTime: "0001-01-01T00:00:00", IsActive: true, …}
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) {

    this.prickTestForm = this.FormBuilder.group({
      // items: [null, Validators.required],
      // items_value: ['no', Validators.required]
    });
    this.rows = this.FormBuilder.array([]);
  }
  loading = false;
  // test: any = [{ "Name": "ggy", "Intensity": "gg", "sign": "ghvgh", "pseudocode": "hgvhgv" }, { "Name": "jdnjd", "Intensity": "kknknk", "sign": "ghvgh", "pseudocode": "hgvhgv" }];
  test: any = []
  test1: any = []
  ngOnInit() {
    // this.prickTestForm = this.FormBuilder.group({
    //   UrineTest: this.FormBuilder.control('', Validators.required)
    // })

    console.log(" this.patientService.patientPrickTest", this.patientService.patientPrickTest);
    this.test = JSON.parse(this.patientService.patientPrickTest['PrickTestResults']);
    console.log("test=========", this.test);
    this.prickTestForm.addControl('rows', this.rows);

  }

  // this.selectedUrine = this.patientService.patientTestsDetails['UrineTest'];

  onAddRow() {
    console.log("onAddRow...........");
    // this.test.push({Name:'',Intensity:'',sign:'',pseudocode:''})
    // if (this.patientService.patientPrickTest != null) {
    //   // this.rows.push(this.patientService.patientPrickTest['']);
    // }
    this.rows.push(this.createItemFormGroup());
    console.log("this.rows====", this.rows);

    // this.alertsService.showToast(false);
  }

  createItemFormGroup(): FormGroup {
    console.log("inside createItemFormGroup ")
    return this.FormBuilder.group({
      Name: null,
      Intensity: null,
      sign: null,
      pseudocode: null
    });
  }

  submitPrickTestForm(form) {
    console.log("form======", form);
    this.loading = true;
    console.log("this.rows=======", this.rows);
    let prickTest = new PrickTest();
    // this.rows.push(this.patientService.patientPrickTest);
    console.log("ghgfhjfjhhjhjhu===", this.patientService.patientPrickTest['PrickTestResults'])
    prickTest.PrickTestResults = JSON.stringify(form.value['rows']);
    // this.patientService.patientPrickTest['PrickTestResults'] = JSON.stringify(form.value['rows']);
    this.patientService.patientPrickTest['IsActive'] = true;
    let apiData = this.patientService.patientPrickTest;
    console.log("apiData====", apiData);
    delete apiData.PrickTestId;
    // delete apiData.CreatedByUserId;
    // delete apiData.CreatedDateTime;
    // delete apiData.PatientDemographics;
    // delete apiData.PatientDemographicsId;
    // var result = { ...apiData, ...prickTest }
    let extisting = JSON.parse(this.patientService.patientPrickTest.PrickTestResults);
    let newval = form.value['rows'];
    let patientPrickTest = extisting.concat(newval);
    // // var result = Object.assign(prickTest, apiData);
    // console.log("result====", result);
    // console.log("apiData===", apiData);
    apiData.PrickTestResults = JSON.stringify(patientPrickTest);
    this.patientService.updatepatientPrickTest(apiData).subscribe(response => {
      console.log("prick test response===", response);
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}



// let chest = new Chest();
// if (this.selectedWheezOption == true) {
//   chest.Wheezing = this.patientChestForm.get('Wheezing').value;
//   chest.WheezingType = this.patientChestForm.get('WheezingType').value;
//   chest.WheezingTime = this.patientChestForm.get('WheezingTime').value;
//   chest.Breathlessness = this.patientChestForm.get('Breathlessness').value;
//   chest.BreathlessnessTime = this.patientChestForm.get('BreathlessnessTime').value;
//   chest.ChestPain = this.patientChestForm.get('ChestPain').value;
//   chest.ChestPainArea = this.patientChestForm.get('ChestPainArea').value;

//   // this.patientService.patientRespiratoryDetails.Wheezing = form.value.Wheezing;
//   // this.patientService.patientRespiratoryDetails.WheezingType = form.value.WheezingType;
//   // this.patientService.patientRespiratoryDetails.WheezingTime = form.value.WheezingTime;
//   // this.patientService.patientRespiratoryDetails.Breathlessness = form.value.Breathlessness;
//   // this.patientService.patientRespiratoryDetails.BreathlessnessTime = form.value.BreathlessnessTime;
//   // this.patientService.patientRespiratoryDetails.ChestPain = form.value.ChestPain;
//   // this.patientService.patientRespiratoryDetails.ChestPainArea = form.value.ChestPainArea;
//   this.patientService.patientRespiratoryDetails.IsActive = true;
//   let apiData = this.patientService.patientRespiratoryDetails;
//   delete apiData.RespiratoryId;
//   var respiratoryDetails = Object.assign(apiData, chest)

