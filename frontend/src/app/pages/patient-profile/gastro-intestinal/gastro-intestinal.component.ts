import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-gastro-intestinal',
  templateUrl: './gastro-intestinal.component.html',
  styleUrls: ['./gastro-intestinal.component.css']
})
export class GastroIntestinalComponent implements OnInit {

  nauseaOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  vomitingOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  relieveOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  abdominalPainOptions =
    [
      { value: true, name: 'YES' },
      { value: false, name: 'NO' },
    ];


  AbdominalPainAreaObj: Object[] = [
    { value: 1, name: 'Left Side' },
    { value: 2, name: 'Right Side' },
    { value: 3, name: 'Back' },
    { value: 4, name: 'Front' },
    { value: 5, name: 'Vague' },
    { value: 6, name: 'Center' },
  ];
  AbdominalPainTypeObj: Object[] = [
    { value: 1, name: 'Burning' },
    { value: 2, name: 'Griping' },
    { value: 3, name: 'Dull Ache' },
    { value: 4, name: 'Pricking' },
  ];

  public selectedAbdominalPainArea: Object[];
  public selectedAbdominalPainType: Object[];

  selectedNausea = false;
  selectedVomiting = false;
  selectedRelieve = false;
  selectedAbdominalPain = false;
  abdominalPainArea = '';
  abdominalPainType = '';

  gastroIntestinalForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;
  ngOnInit() {
    this.gastroIntestinalForm = this.formBuilder.group({
      Nausea: this.formBuilder.control('', Validators.required),
      Vomiting: this.formBuilder.control('', Validators.required),
      DoesVomitingGiveRelief: this.formBuilder.control('', Validators.required),
      AbdominalPain: this.formBuilder.control('', Validators.required),
      AbdominalPainArea: this.formBuilder.control('', Validators.required),
      AbdominalPainType: this.formBuilder.control('', Validators.required)
    });
    this.selectedNausea = this.patientService.patientGastroIntestinalDetails['Nausea'];
    this.selectedVomiting = this.patientService.patientGastroIntestinalDetails['Vomiting'];
    this.selectedRelieve = this.patientService.patientGastroIntestinalDetails['DoesVomitingGiveRelief'];
    this.selectedAbdominalPain = this.patientService.patientGastroIntestinalDetails['AbdominalPain'];
    // this.abdominalPainArea = this.patientService.patientGastroIntestinalDetails['AbdominalPainArea'];
    // this.abdominalPainType = this.patientService.patientGastroIntestinalDetails['AbdominalPainType'];


    let previousAbdominalPainArea = JSON.parse(this.patientService.patientGastroIntestinalDetails['AbdominalPainArea']);
    this.selectedAbdominalPainArea = this.AbdominalPainAreaObj.filter(asp => previousAbdominalPainArea.map(psp => psp['value']).indexOf(asp['value']) !== -1);

    let previousAbdominalPainType = JSON.parse(this.patientService.patientGastroIntestinalDetails['AbdominalPainType']);
    this.selectedAbdominalPainType = this.AbdominalPainTypeObj.filter(asp => previousAbdominalPainType.map(psp => psp['value']).indexOf(asp['value']) !== -1);


    // let previousAllergySymptoms = JSON.parse(this.patientService.patientPastPresentHistoryDetails['PreviousAllergy']);
    // this.selectedPreviousAllergySymptoms = this.PreviousAllergySymptomsObj.filter(asp => previousAllergySymptoms.map(psp => psp['value']).indexOf(asp['value']) !== -1);

  }

  submitGastroIntestinalForm(form) {
    this.loading = true;

    const gastroIntestinalObj = {
      Nausea: form.value.Nausea ? 1 : 0,
      Vomiting: form.value.Vomiting ? 1 : 0,
      DoesVomitingGiveRelief: form.value.DoesVomitingGiveRelief ? 1 : 0,
      AbdominalPain: form.value.AbdominalPain ? 1 : 0,
      // AbdominalPainArea: form.value.AbdominalPain ? form.value.AbdominalPainArea : null,
      AbdominalPainArea: form.value['AbdominalPainArea'] ? JSON.stringify(form.value.AbdominalPainArea) : JSON.stringify(this.selectedAbdominalPainArea),

      // AbdominalPainType: form.value.AbdominalPain ? form.value.AbdominalPainType : null,
      AbdominalPainType: form.value['AbdominalPainType'] ? JSON.stringify(form.value.AbdominalPainType) : JSON.stringify(this.selectedAbdominalPainType),

      IsActive: true
    };

    this.patientService.updatePatientGastroIntestinalDetails(gastroIntestinalObj).subscribe(response => {
      console.log('responce===', response);
      this.loading = false;
      this.alertsService.showToast(true);
    })
  }

}
