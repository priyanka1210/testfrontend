export class DietDetails {
    /*[Key]*/
    public DietSleepId: number;
    public DietFollowed: string;
    public Eating: string;
    public Appetite: string;
    public SleepPattern: string;
    public Snoring: string;
    public PhysicalActivity: string;
    public IncreasedGas: string;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: string;

    constructor(
        DietID: number, PatientID: number, DietType: string, Eat: string, Snore: string, Gas: string,
        appetite: string, phyactivity: string, isActive: boolean = true, patientDemographics: string = "") {
        this.DietSleepId = DietID;
        this.PatientDemographicsId = PatientID;
        this.DietFollowed = DietType;
        this.Eating = Eat;
        this.Snoring = Snore;
        this.IncreasedGas = Gas;
        this.Appetite = appetite;
        this.PhysicalActivity = phyactivity;
        this.IsActive = isActive;
        this.PatientDemographics = "";
    }
}
