export class PhysicalDetails {
    /*[Key]*/
    public PatientPhysicalDetailsId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public Size: string;
    public Color: string;
    public MentalStatus: string;
    public Height: number;
    public Weight: number;
    public SymptomsAtOccupation: boolean;
    public SymptomesSelectedAtOccupation: string;
    public ExposureToSun: boolean;
    public CreatedByUserId: number;
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
}
