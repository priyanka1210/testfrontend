import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';
import { DatePipe } from '@angular/common';
// import { Blood } from 'src/app/shared/blood';

@Component({
  selector: 'app-blood-receiving-details',
  templateUrl: './blood-receiving-details.component.html',
  styleUrls: ['./blood-receiving-details.component.css']
})
export class BloodReceivingDetailsComponent implements OnInit {

  bloodReceiveOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  bloodRecieveImpactOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedBloodRecievedImpact = false;
  selectedBloodReceive = false;
  noofTimesBloodReceived: number;
  bloodReceiveDate: any;
  bloodReceiveProblem: string;

  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private datePipe: DatePipe, private alertsService: AlertsService) { }

  loading = false;
  bloodReceiveForm: FormGroup;

  ngOnInit() {
    this.bloodReceiveForm = this.FormBuilder.group({
      BloodReceive: this.FormBuilder.control('', Validators.required),
      NoofTimesBloodReceived: this.FormBuilder.control('', Validators.required),
      BloodReceiveDate: this.FormBuilder.control('', Validators.required),
      RecievedImpact: this.FormBuilder.control('', Validators.required),
      BloodReceiveProblem: this.FormBuilder.control('', Validators.required),
    })
    console.log("patient blood details==", this.patientService.patientBloodDetails);
    this.noofTimesBloodReceived = this.patientService.patientBloodDetails.NoofTimesBloodReceived;
    console.log("this.noofTimesBloodReceived===", this.noofTimesBloodReceived);
    this.bloodReceiveDate = this.patientService.patientBloodDetails.BloodReceivedDate;
    if (this.bloodReceiveDate != null) {
      //this.bloodReceiveDate = this.bloodReceiveDate.split("T");
      this.bloodReceiveDate = this.bloodReceiveDate.toString().substr(0, 10);

      //this.bloodReceiveDate = this.bloodReceiveDate[0];
    }
    this.bloodReceiveProblem = this.patientService.patientBloodDetails.BloodReceivingProblems;
    this.selectedBloodReceive = this.patientService.patientBloodDetails.BloodReceived;
    this.selectedBloodRecievedImpact = this.patientService.patientBloodDetails.BloodReceivedImpact;
  }

  submitBloodReceiveForm(form) {
    this.loading = true;
    let patientBloodDetails = form.value;
    console.log("form value===", form);
    // this.patientService.patientBloodDetails['BloodReceivedDate']
    if (form.value.BloodReceiveDate != null || form.value.BloodReceiveDate != undefined || form.value.BloodReceiveDate != "") {
      patientBloodDetails.BloodReceivedDate = this.datePipe.transform((form.value.BloodReceiveDate), 'yyyy-MM-dd');
      this.patientService.patientBloodDetails['BloodReceivedDate'] = patientBloodDetails.BloodReceivedDate;
    }
    else {
      patientBloodDetails.BloodReceivedDate = "2019-11-11";
      this.patientService.patientBloodDetails['BloodReceivedDate'] = patientBloodDetails.BloodReceivedDate;
    }
    this.patientService.patientBloodDetails['NoofTimesBloodReceived'] = form.value.NoofTimesBloodReceived;
    this.patientService.patientBloodDetails['BloodReceived'] = form.value.BloodReceive;
    this.patientService.patientBloodDetails['BloodReceivedImpact'] = form.value.RecievedImpact;
    this.patientService.patientBloodDetails['BloodReceivingProblems'] = form.value.BloodReceiveProblem;
    this.patientService.patientBloodDetails['IsActive'] = true;

    let apiData = this.patientService.patientBloodDetails;
    delete apiData.BloodTransferId;
    console.log("apiData===", apiData);
    this.patientService.updatePatientBloodDetails(apiData).subscribe(response => {
      console.log("response.....", response);
      this.loading = false;
      this.alertsService.showToast(true);
    })
  }

}
