import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-skin2',
  templateUrl: './skin2.component.html',
  styleUrls: ['./skin2.component.css']
})
export class Skin2Component implements OnInit {

  patchColorIncreaseOptions = [
    { value: true, name: 'Increase' },
    { value: false, name: 'Decrease' },
  ];

  skin2Form: FormGroup;

  skinAppearandDisAppear: string;
  skinPatchColor: string;
  patchColorTiming: string;
  selectedPatchColorIncrease: boolean = false;
  patchColorAppear: string;
  patchColorDisappear: string;

  loading = false;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.skin2Form = this.FormBuilder.group({
      SkinAppearandDisAppear: this.FormBuilder.control('', Validators.required),
      SkinPatchColor: this.FormBuilder.control('', Validators.required),
      PatchColorTiming: this.FormBuilder.control('', Validators.required),
      PatchColorIncrease: this.FormBuilder.control('', Validators.required),
      PatchColorAppear: this.FormBuilder.control('', Validators.required),
      PatchColorDisappear: this.FormBuilder.control('', Validators.required)
    });

    this.skinAppearandDisAppear = this.patientService.patientSkinDetails['SkinAppearandDisAppear'] ? 'With Patch' : 'Without Patch';
    this.skinPatchColor = this.patientService.patientSkinDetails['SkinPatchColor'];
    this.patchColorTiming = this.patientService.patientSkinDetails['PatchColorTiming'];
    this.selectedPatchColorIncrease = this.patientService.patientSkinDetails['PatchColorIncrease'];
    this.patchColorAppear = this.patientService.patientSkinDetails['PatchColorAppear'];
    this.patchColorDisappear = this.patientService.patientSkinDetails['PatchColorDisappear'];
  }

  submitSkin2Form(form) {
    this.patientService.patientSkinDetails['SkinAppearandDisAppear'] = form.value['SkinAppearandDisAppear'] === 'With Patch';
    this.patientService.patientSkinDetails['SkinPatchColor'] = form.value['SkinPatchColor'];
    this.patientService.patientSkinDetails['PatchColorTiming'] = form.value['PatchColorTiming'];
    this.patientService.patientSkinDetails['PatchColorIncrease'] = form.value['PatchColorIncrease'];
    this.patientService.patientSkinDetails['PatchColorAppear'] = form.value['PatchColorAppear'];
    this.patientService.patientSkinDetails['PatchColorDisappear'] = form.value['PatchColorDisappear'];
    this.loading = true;
    let apiData = this.patientService.patientSkinDetails;
    delete apiData.SkinId;

    this.patientService.updatepatientSkinDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
