import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";

import { PatientService } from './../../../../shared/patientService.service';
import { AFFurniture } from './../../../../shared/AllergyFactors/affurniture';
import { AFCarpet } from './../../../../shared/AllergyFactors/afcarpet';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-allergy-factor2',
  templateUrl: './allergy-factor2.component.html',
  styleUrls: ['./allergy-factor2.component.css']
})
export class AllergyFactor2Component implements OnInit {

  carpetOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedCarpet = false;
  selectedCarpetOption = "";
  loading: boolean = false;

  items = [{'value':'Silk', 'name':'Silk'}, {'value':'Jute', 'name': 'Jute'}, {'value':'Cotton', 'name': 'Cotton'},{'value':'Very Old Furniture', 'name': 'Very Old Furniture'}, {'value':'Coir', 'name': 'Coir'}];
  allergyFactor2Form : FormGroup;

  //  screen-3
  //  AFFurniture:AFFurniture[]=[];
  //  Carpets:boolean;
  //  CarpetsSelect:string[];

  selectedFurnitures : AFFurniture[]=[];
  Carpets:boolean;
  
  selectedCarpetAllergy : AFCarpet[]=[];
  selectedCarpets:string[] = [];


  public LeatherCheckBox:boolean = false;
  public FoamleatherCheckBox:boolean = false;
  public WoodenCheckBox:boolean = false;
  public SteelCheckBox:boolean = false;
  public OldfurnitureCheckBox:boolean = false;
  public PolysterCheckBox:boolean = false;
  public CottonCheckBox:boolean = false;
  public SilkCheckBox:boolean = false;
  public NewFurnitureCheckBox:boolean = false;
  public ModeratelyNewCheckBox:boolean = false;



  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor2Form = this.formBuilder.group({
     Carpet: this.formBuilder.control('', Validators.required),
     CarpetSelect: this.formBuilder.control('', Validators.required),
     Furniture_Leather  : this.formBuilder.control('', Validators.required),      
     Furniture_FoamLeather: this.formBuilder.control('', Validators.required),
     Furniture_Wooden   : this.formBuilder.control('', Validators.required),
     Furniture_Steel    : this.formBuilder.control('', Validators.required),
     Furniture_Oldfurniture: this.formBuilder.control('', Validators.required),
     Furniture_Polyster : this.formBuilder.control('', Validators.required),
     Furniture_Cotton   : this.formBuilder.control('', Validators.required),
     Furniture_Silk     : this.formBuilder.control('', Validators.required),
     Furniture_NewFurniture: this.formBuilder.control('', Validators.required),
     Furniture_ModeratelyNew: this.formBuilder.control('', Validators.required)
    });
    
    for (let furniture of this.patientService.patientAllergyDetails.AFFurniture) {
      switch(furniture.AllergyItemDescription) {
        
          case "Leather":
              this.LeatherCheckBox=true
              break;
          case "Foam Leather":
              this.FoamleatherCheckBox=true
              break;
          case "Wodden":
              this.WoodenCheckBox=true
              break;
          case "Steel":
              this.SteelCheckBox=true
              break;
          case "Polyester":
              this.PolysterCheckBox=true
              break;
          case "Cotton":
              this.CottonCheckBox=true
              break;
          case "Silk":
              this.SilkCheckBox=true
              break;
          case "New Furniture":
              this.NewFurnitureCheckBox=true
              break;
          case "Moderately New":
              this.ModeratelyNewCheckBox=true
              break;
      }
    }
    //set carpets
    if ((this.patientService.patientAllergyDetails.AFCarpet != null)) {
        this.selectedCarpet = this.patientService.patientAllergyDetails.AFCarpet.length > 0; 
        for (let carpet of this.patientService.patientAllergyDetails.AFCarpet) {
          let carpetData = this.patientService.allergyDataIdToDataName.get(carpet.MasterAllergyDataId);
          this.selectedCarpets.push(carpetData);
        }
    }
  }

  submitAllergyFactor2Form(){
    this.loading = true;
    if (this.LeatherCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Leather";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Leather");//89;
      afFurniture.AllergyStatus = this.LeatherCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.FoamleatherCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Foam Leather";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Foam Leather");//90;
      afFurniture.AllergyStatus = this.FoamleatherCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.WoodenCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Wodden";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Wodden");//91;
      afFurniture.AllergyStatus = this.WoodenCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.SteelCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Steel";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Steel");//92;
      afFurniture.AllergyStatus = this.SteelCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.OldfurnitureCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Old Furniture";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Old Furniture");//93;
      afFurniture.AllergyStatus = this.OldfurnitureCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.PolysterCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Polyester";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Polyester");//94;
      afFurniture.AllergyStatus = this.PolysterCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.CottonCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Cotton";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cotton");//95;
      afFurniture.AllergyStatus = this.CottonCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }

    if (this.SilkCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Silk";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Silk");//96;
      afFurniture.AllergyStatus = this.SilkCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.NewFurnitureCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "New Furniture";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("New Furniture");//97;
      afFurniture.AllergyStatus = this.NewFurnitureCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }
    if (this.ModeratelyNewCheckBox)
    {
      let afFurniture = new AFFurniture();
      afFurniture.AllergyItemDescription = "Moderately New";
      afFurniture.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFurniture.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately New");//98;
      afFurniture.AllergyStatus = this.ModeratelyNewCheckBox;
      this.selectedFurnitures.push(afFurniture);
    }

    
    var allergyFactor2 = {};
    allergyFactor2["AFFurniture"] = this.selectedFurnitures;
    console.log(JSON.stringify(allergyFactor2))
    //alert('before api call');

    // Carpet
    for (var i in this.selectedCarpets) {
   
      let afCarpet = new AFCarpet();
    

      // let insectMasterAllergyDataId = this.patientService.allergyDataNameToDataId.get('this.selectedInsect[i]');
      // console.log("Insect Master Allergy data id",insectMasterAllergyDataId);

      afCarpet.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afCarpet.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get(this.selectedCarpets[i]);//75;
      afCarpet.AllergyStatus = true;
      afCarpet.AllergyItemDescription = this.selectedCarpets[i];
      this.selectedCarpetAllergy.push( afCarpet);
    }
    allergyFactor2["AFCarpet"] = this.selectedCarpetAllergy;
    console.log(JSON.stringify(allergyFactor2));


    let allergyFactor1 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor1, allergyFactor2);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response); 
      this.loading = false; 
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);

    },
    error => {
      //alert(error);
    });

  }

}
