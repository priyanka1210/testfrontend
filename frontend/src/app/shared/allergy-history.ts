export class AllergyHistory {
    /*[Key]*/
    public AllergyHistoryId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public DifferPerPlace: boolean;
    public Place1: string;
    public Place1Symptom: string;
    public Place2: string;
    public Place2Symptoms: string;
    public Place3: string;
    public Place3Symptoms: string;
    public Place4: string;
    public Place4Symptoms: string;
    public Season: string;
    public VariesbySeason: boolean;
    public Summer: string;
    public Rainy: string;
    public Winter: string;
    public Monsoon: string;
    public SummerSymptoms: string;
    public WinterSymptoms: string;
    public MonsoonSymptoms: string;
    public Symptoms1MoreAt: string;
    public Symptoms2MoreAt: string;
    public Symptoms3MoreAt: string;
    public Symptoms4MoreAt: string;
    public Symptom51MoreAt: string;
    public SymptomStatus: string;
    public SymptomsUnderFan: string;
    public SymptomsUnderAirCondition: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
}
