import { OnInit, Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from '../../../shared/alerts.service';


@Component({
    selector: 'app-nativity-and-domicile',
    templateUrl: './nativity-and-domicile.component.html',
    styleUrls: ['./nativity-and-domicile.component.css']
})

export class NativityAndDomicileComponent implements OnInit {

    ChangesatplaceOptions = [
        { value: true, name: 'YES' },
        { value: false, name: 'NO' },
    ];

    Changesatplace = false;

    nativityAndDomilcileForm: FormGroup;
    birthPlace: any;
    boughtUpPlace: string;
    nativityOfParents: string;
    Stay1Place: string;
    Stay1Year: number;
    Stay2Year: number;
    Stay2Place: string;
    Stay3Place: string;
    Stay3Year: number;
    Stay4Place: string;
    Stay4Year: number;
    CurrentPlace: string;
    // CurrentPlaceYear: number;
    currentPlaceYear: number;
    constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
    loading = false;
    ngOnInit() {
        this.nativityAndDomilcileForm = this.FormBuilder.group({
            Changesatplace: this.FormBuilder.control('', Validators.required),
            BirthPlace: this.FormBuilder.control('', Validators.required),
            BoughtUpPlace: this.FormBuilder.control('', Validators.required),
            NativityOfParents: this.FormBuilder.control('', Validators.required),
            Stay1Place: this.FormBuilder.control('', Validators.required),
            Stay1Year: this.FormBuilder.control('', Validators.required),
            Stay2Place: this.FormBuilder.control('', Validators.required),
            Stay2Year: this.FormBuilder.control('', Validators.required),
            Stay3Place: this.FormBuilder.control('', Validators.required),
            Stay3Year: this.FormBuilder.control('', Validators.required),
            Stay4Place: this.FormBuilder.control('', Validators.required),
            Stay4Year: this.FormBuilder.control('', Validators.required),
            CurrentPlace: this.FormBuilder.control('', Validators.required),
            CurrentPlaceYear: this.FormBuilder.control('', Validators.required),
        })

        this.Changesatplace = this.patientService.patientNativityDetails.ChangeinStay;
        this.birthPlace = this.patientService.patientNativityDetails.BirthPlace;
        this.boughtUpPlace = this.patientService.patientNativityDetails.BroughtupPlace;
        this.nativityOfParents = this.patientService.patientNativityDetails.ParentsNativity;
        this.Stay1Place = this.patientService.patientNativityDetails.Stay1Place;
        this.Stay1Year = this.patientService.patientNativityDetails.Stay1Years;
        this.Stay2Place = this.patientService.patientNativityDetails.Stay2Place;
        this.Stay2Year = this.patientService.patientNativityDetails.Stay2Years;
        this.Stay3Place = this.patientService.patientNativityDetails.Stay3Place;
        this.Stay3Year = this.patientService.patientNativityDetails.Stay3Years;
        this.Stay4Place = this.patientService.patientNativityDetails.Stay4Place;
        this.Stay4Year = this.patientService.patientNativityDetails.Stay4Years;
        this.CurrentPlace = this.patientService.patientNativityDetails.CurrentPlace;
        this.currentPlaceYear = this.patientService.patientNativityDetails.NoofYearsinCurrentPlace;
    }

    submitNativityAndDomilcileForm(form) {
        this.loading = true;
        let patientNativityDetails = form.value;
        patientNativityDetails.ChangeinStay = form.value.Changesatplace;
        patientNativityDetails.BroughtupPlace = form.value.BoughtUpPlace;
        patientNativityDetails.ParentsNativity = form.value.NativityOfParents;
        patientNativityDetails.Stay1Years = form.value.Stay1Year;
        patientNativityDetails.Stay2Years = form.value.Stay2Year;
        patientNativityDetails.Stay3Years = form.value.Stay3Year;
        patientNativityDetails.Stay4Years = form.value.Stay4Year;
        patientNativityDetails.NoofYearsinCurrentPlace = form.value.CurrentPlaceYear;
        patientNativityDetails.IsActive = true;
        this.patientService.updatePatientNativityDetails(patientNativityDetails).subscribe(response => {
            // this.patientService.patientNativityDetails = response;//response["patientDemographics"];
            this.loading = false;
            this.alertsService.showToast(true);
        })

    }
}