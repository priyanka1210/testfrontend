import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';
@Component({
  selector: 'app-throat-info',
  templateUrl: './throat-info.component.html',
  styleUrls: ['./throat-info.component.css']
})
export class ThroatInfoComponent implements OnInit {

  chokingOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ]
  voiceBoxComplaintsOptions = [
    { value: 1, name: 'strained voice' },
    { value: 2, name: 'broken voice' },
    { value: 3, name: 'hoorse voice' },
    { value: 4, name: 'loss of voice' }
  ]

  public throatComplaintObj: Object[] = [
    { value: 1, name: 'Itching' },
    { value: 2, name: 'Difficulty in Swallowing' },
    { value: 3, name: 'Pain' },
  ];
  // public throatComplaint;
  public selectedThroatComplaint: Object[];
  public throatChoking;
  public voiceBoxComplaints;
  public throatInfoForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.throatInfoForm = this.formBuilder.group({
      ThroatComplaint: this.formBuilder.control('', Validators.required),
      ThroatChoking: this.formBuilder.control('', Validators.required),
      VoiceBoxComplaints: this.formBuilder.control('', Validators.required)
    });

    console.log("patientENTDetails===", this.patientService.patientENTDetails);
    // this.throatComplaint = this.patientService.patientENTDetails['ThroatComplaint'];
    this.throatChoking = this.patientService.patientENTDetails['ThroatChoking'];
    this.voiceBoxComplaints = this.patientService.patientENTDetails['VoiceBoxComplaints'];
    let previousThroatComplaint = JSON.parse(this.patientService.patientENTDetails['ThroatComplaint']);
    this.selectedThroatComplaint = this.throatComplaintObj.filter(asp => previousThroatComplaint.map(psp => psp['value']).indexOf(asp['value']) !== -1);

  }

  submitThroatInfoForm(form) {
    console.log("form value:::::::", form.value);
    // PatientService.patientENTDetails.ThroatComplaint = 
    // this.patientService.patientENTDetails.ThroatComplaint = form.value.ThroatComplaint;
    this.patientService.patientENTDetails['ThroatComplaint'] = form.value['ThroatComplaint'] ? JSON.stringify(form.value.ThroatComplaint) : JSON.stringify(this.selectedThroatComplaint);

    console.log("this.patientService.patientENTDetails.ThroatComplaint==", this.patientService.patientENTDetails.ThroatComplaint);
    this.patientService.patientENTDetails['ThroatChoking'] = form.value.ThroatChoking;
    this.patientService.patientENTDetails['VoiceBoxComplaints'] = form.value.VoiceBoxComplaints;
    this.patientService.patientENTDetails['IsActive'] = true;
    this.loading = true;
    let patientpatientENTDetailsObj = Object.assign({}, this.patientService.patientENTDetails);
    console.log("patientpatientENTDetailsObj========", patientpatientENTDetailsObj);
    this.patientService.updatepatientENTDetails(patientpatientENTDetailsObj).subscribe(response => {
      console.log("response===", response);
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}
