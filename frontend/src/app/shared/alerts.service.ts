import { Injectable } from '@angular/core';
import { NbToastrService } from '@nebular/theme';


@Injectable({
  providedIn: 'root'
})
export class AlertsService {

  constructor(private toastrService: NbToastrService) {}
  showToast(destroyByClick) {
    this.toastrService.show(
      'Submitted Successfully',
      `Message`,
      { destroyByClick });
  }
  




}
