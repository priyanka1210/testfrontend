export class Habits {
    /*[Key]*/
    public PatientHabitId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public AlcoholConsumption: boolean;
    public AlcoholName: string;
    public AlcoholQuantity: string;
    public AlcoholFrequency: string;
    public AnyChangesAfterAlcoholConsumption: boolean;
    public ChangesAfterAlcoholConsumption: string;
    public HistoricalAlcoholConsumption: boolean;
    public HistoricalAlcoholName: string;
    public HistoricalAlcoholQuantity: string;
    public HistoricalAlcoholFrequency: string;
    public HistoricalAlcoholQuitDate: Date;
    public HistoricalAnyChangesAfterAlcoholConsumption: boolean;
    public HistoricalChangesAfterAlcoholConsumption: string;
    public Chewing: boolean;
    public ChewingSubstance: string;
    public HistoricalChewing: boolean;
    public HistoricalChewingSubstance: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    public ChewingQuitDate: Date;
    public IsActive: boolean;
    public PatientDemographics: string;
    /*[ForeignKey("PatientDemographicsId")]*/
}
