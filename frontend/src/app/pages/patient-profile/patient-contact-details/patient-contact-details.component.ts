import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PatientService } from './../../../shared/patientService.service';
import { PatientDetails } from './../../../shared/patient-details';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';
import { PhysicalDetails } from './../../../shared/physical-details';

@Component({
  selector: 'app-patient-contact-details',
  templateUrl: './patient-contact-details.component.html',
  styleUrls: ['./patient-contact-details.component.css']
})
export class PatientContactDetailsComponent implements OnInit {

  patientContactForm:  FormGroup;
  patientDetails : PatientDetails;

  constructor(private formBuilder: FormBuilder, private patientService: PatientService) { }

  ngOnInit() {
    this.patientContactForm = this.formBuilder.group({
      FirstName: this.formBuilder.control('', Validators.required),
      MiddleName: this.formBuilder.control('', Validators.required),
      LastName: this.formBuilder.control('', Validators.required),
      Mobile:this.formBuilder.control('', Validators.required),
      EmailID: this.formBuilder.control(' ', Validators.required),
      DateofBirth:this.formBuilder.control('', Validators.required),
      SecondaryContactNumber: this.formBuilder.control(0, Validators.required),
      ReferringDoctorName: this.formBuilder.control('', Validators.required),
      ReferringDoctortNumber: this.formBuilder.control('', Validators.required),
      Address1: this.formBuilder.control('', Validators.required),
      Address2: this.formBuilder.control('', Validators.required),
      
      /*'City':  this.formBuilder.control('Pune', Validators.required),
      'ZipCode': this.formBuilder.control(1212, Validators.required),
      'State':this.formBuilder.control('Maharashtra', Validators.required),
      'Gender': this.formBuilder.control('', Validators.required),
      'MarriatalStatus': this.formBuilder.control('', Validators.required),
      'Occupation': this.formBuilder.control('', Validators.required),
      'BodySize': this.formBuilder.control('', Validators.required),
      'SkinColour': this.formBuilder.control('', Validators.required),
      'Height': this.formBuilder.control('', Validators.required),
      'Weight': this.formBuilder.control('', Validators.required),*/

    });
    

   // this.patientContactPhysicalDetailsForm .disable();
   console.log("Received patient with First Name:" + this.patientDetails.PatientDemographicsDetails.FirstName);
   
  // this.patientContactPhysicalDetailsForm.patchValue({'FirstName':this.patientDetails.PatientDemographicsDetails.FirstName});
  }
  /*
  handleSubmit(){
    console.log("patient-contact-details && patient-physical-details submitted");
    /*alert(this.patientDetails.PatientDemographicsDetails.PatientDemographicsId);
    if (this.patientDetails.PatientDemographicsDetails.PatientDemographicsId == null)
        {
          this.patientService.createPatientDetails(this.patientContactPhysicalDetailsForm.value);
          
        }
        else
        {
          this.patientService.updatePatientDetails(this.patientContactPhysicalDetailsForm.value);
        }
}*/


  handleSubmit(){
    console.log("patient-contact-details && patient-physical-details submitted");
    if (this.patientContactForm.invalid) {
      return;
    }
    console.log("Received patient with First Name:" + this.patientDetails.PatientDemographicsDetails.FirstName);
    /*let patientDemographicsDetails = new PatientDemographicsDetails();
    patientDemographicsDetails.FirstName = this.patientContactPhysicalDetailsForm["FirstName"]
    
    let patientPhysicalDetails = new PhysicalDetails();
    patientPhysicalDetails.Height = this.patientContactPhysicalDetailsForm["Height"]
    
    this.patientService.updatePatientDetails(patientDemographicsDetails,patientPhysicalDetails)
      .subscribe(data => {
        console.log("Patient Contact and Personal Details",data);
      },
      error => {
        alert(error);
      });*/
   


  }
/*
  onUpdateUserRegistration() {
    if (this.loginForm.invalid) {
      return;
    }
    const updateUserPayload = {
      FirstName: this.loginForm.controls.firstname.value,
      LastName: this.loginForm.controls.lastname.value,
      UserName: this.loginForm.controls.username.value,
      Email:this.loginForm.controls.email.value,
      MobileNumber: this.loginForm.controls.mobilenumber.value,
     // RoleId : selectedRole;
     // RoleName: this.roleIdToRoleNameMap.get(selectedRole);
      //countryCode:number = 0
      //this.loginForm.controls.countrycode.value 
      isActive: true
    }
   
    this.apiService.updateUser(updateUserPayload)
      .subscribe(
        data => {
              console.log(data);
              //this.router.navigate(['assets']);
              //console.log(data.isJWTtokenCreated)
              alert("Congrats!" + " " + "Your credentials are Updated Successfully");
              this.router.navigate(['users']);
      },
            error => {
              alert(error);
             // this.error = error
            }

    );
  }*/
}
