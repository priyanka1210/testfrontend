import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-hair-details',
  templateUrl: './hair-details.component.html',
  styleUrls: ['./hair-details.component.css']
})
export class HairDetailsComponent implements OnInit {
  dandruffOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  hairlossOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  selectedHairType = '';
  selectedDandruffbtn = false;
  selectedHairlossbtn = false;
  hairDetailsForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.hairDetailsForm = this.formBuilder.group({
      HairType: this.formBuilder.control('', Validators.required),
      Dandruff: this.formBuilder.control('', Validators.required),
      HairLoss: this.formBuilder.control('', Validators.required),
    });

    this.selectedHairType = this.patientService.patientHairDetails.HairType;
    this.selectedDandruffbtn = this.patientService.patientHairDetails.Dandruff;
    this.selectedHairlossbtn = this.patientService.patientHairDetails.HairLoss;
  }
  submitHairDetailsForm(form) {
    this.patientService.patientHairDetails['HairType'] = form.value['HairType'];
    this.patientService.patientHairDetails['Dandruff'] = form.value['Dandruff'];
    this.patientService.patientHairDetails['HairLoss'] = form.value['HairLoss'];
    this.loading = true;
    let apiData = this.patientService.patientHairDetails;
    delete apiData.HairHeadacheId;

    this.patientService.updatePatientHairDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}
