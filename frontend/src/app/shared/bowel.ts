export class Bowel {
     /*[Key]*/
     public BowelId: number;
     public BowelHabits: string;
     public Urination: string;
     /*[Required]*/
     public PatientDemographicsId: number;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;
     /*[ForeignKey("PatientDemographicsId")]*/
 
     // constructor(HabitsInfo:string,PatientID:number){
     //     this.BowelHabitsMADId = HabitsInfo;
     //     this.PatientDemographicsId = PatientID;
     // }
     IsActive: boolean;
     PatientDemographics: string;
}
