import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';
import { YEARS_IN_COLUMN } from '@nebular/theme';
@Component({
  selector: 'app-female-personal-details',
  templateUrl: './female-personal-details.component.html',
  styleUrls: ['./female-personal-details.component.css']
})
export class FemalePersonalDetailsComponent implements OnInit {

  periodsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedPeriods = false;
  pregnanciesOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  firstTrimesterAllergyOptions = [
    { value: true, name: 'Increase' },
    { value: false, name: 'Decrease' },
  ];

  secondTrimesterAllergyOptions = [
    { value: true, name: 'Increase' },
    { value: false, name: 'Decrease' },
  ];

  thirdTrimesterAllergyOptions = [
    { value: true, name: 'Increase' },
    { value: false, name: 'Decrease' },
  ];
  afterDeliveryAllergyOptions = [
    { value: true, name: 'Increase' },
    { value: false, name: 'Decrease' },
  ];
  // allergySymptomsIncreaseOptions = [
  //   { value: true, name: 'YES' },
  //   { value: false, name: 'NO' },
  // ];
  allergySymptomsIncreaseOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  allergySymptomsDecreaseOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  // public firstTrimesterAllergyOptions: Object[] = [
  //   { value: true, name: 'YES' },
  //   { value: false, name: 'NO' },
  // ];

  // public secondTrimesterAllergyOptions: Object[] = [
  //   { value: true, name: 'YES' },
  //   { value: false, name: 'NO' },
  // ];

  selectedPregnancies = false;
  // public selectedAllergySymptomsIncrease: Object[];
  femalePersonalDetailsForm: FormGroup;

  Menstruate: string;
  MenarcheAge: number;
  FemaleTypeofFlow: string;
  Menstruateoption: string;
  selectedAllergySymptomsIncrease: string;
  selectedAllergySymptomsDecrease: string;
  MarriedForYears: number;
  AllergySymptomsIncrease: string;
  AllergySymptomsDecrease: string;
  selectedFirstTrimesterAllergy: boolean;
  selectedSecondTrimesterAllergy: boolean;
  selectedThirdTrimesterAllergy: boolean;
  selectedAfterDeliveryAllergy: boolean;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;
  ngOnInit() {
    this.femalePersonalDetailsForm = this.FormBuilder.group({
      RegularPeriods: this.FormBuilder.control('', Validators.required),
      Pregnancies: this.FormBuilder.control('', Validators.required),
      Menstruate: this.FormBuilder.control('', Validators.required),
      MenarcheAge: this.FormBuilder.control('', Validators.required),
      FemaleTypeofFlow: this.FormBuilder.control('', Validators.required),
      AllergySymptomsIncrease: this.FormBuilder.control('', Validators.required),
      AllergySymptomsDecrease: this.FormBuilder.control('', Validators.required),
      MarriedForYears: this.FormBuilder.control('', Validators.required),
      FirstTrimesterAllergy: this.FormBuilder.control('', Validators.required),
      SecondTrimesterAllergy: this.FormBuilder.control('', Validators.required),
      ThirdTrimesterAllergy: this.FormBuilder.control('', Validators.required),
      AfterDeliveryAllergy: this.FormBuilder.control('', Validators.required),
      // AllergySymptomsIncrease1: this.FormBuilder.control('', Validators.required),
    });

    console.log(" this.patientService.patientFemaleInfoDetails==", this.patientService.patientFemaleInfoDetails);
    this.Menstruate = this.patientService.patientFemaleInfoDetails.Menstruate;
    console.log(" this.Menstruate==", this.Menstruate);
    this.MenarcheAge = this.patientService.patientFemaleInfoDetails.MenarcheAge;
    console.log(" this.MenarcheAge===", this.MenarcheAge);
    this.selectedPeriods = this.patientService.patientFemaleInfoDetails.RegularPeriods;
    console.log("this.selectedPeriods===", this.selectedPeriods);
    this.FemaleTypeofFlow = this.patientService.patientFemaleInfoDetails.FemaleTypeofFlow
    this.AllergySymptomsIncrease = this.patientService.patientFemaleInfoDetails.AllergySymptomsIncrease.toString();
    console.log(" get this.AllergySymptomsIncrease==", this.selectedAllergySymptomsIncrease);
    this.AllergySymptomsDecrease = this.patientService.patientFemaleInfoDetails.AllergySymptomsDecrease.toString();
    this.MarriedForYears = this.patientService.patientFemaleInfoDetails.MarriedForYears;
    this.selectedFirstTrimesterAllergy = this.patientService.patientFemaleInfoDetails.FirstTrimesterAllergy;
    this.selectedSecondTrimesterAllergy = this.patientService.patientFemaleInfoDetails.SecondTrimesterAllergy;
    this.selectedThirdTrimesterAllergy = this.patientService.patientFemaleInfoDetails.ThirdTrimesterAllergy;
    this.selectedAfterDeliveryAllergy = this.patientService.patientFemaleInfoDetails.AfterDeliveryAllergy;
  }



  submitFemalePersonalDetailsForm(form) {
    console.log("This form data;::::", form);
    this.loading = true;
    // this.patientService.patientFemaleInfoDetails['Menstruate'] = form.value['Menstruate'];
    // this.patientService.patientFemaleInfoDetails['MenarcheAge'] = form.value['MenarcheAge'];
    // this.patientService.patientFemaleInfoDetails['RegularPeriods'] = form.value['RegularPeriods'];
    // this.patientService.patientFemaleInfoDetails['Pregnancies'] = form.value['Pregnancies'];
    // if (form.value['RegularPeriods']) {
    //   this.patientService.patientFemaleInfoDetails['FemaleTypeofFlow'] = form.value['FemaleTypeofFlow'];
    //   this.patientService.patientFemaleInfoDetails['AllergySymptomsIncrease'] = form.value['AllergySymptomsIncrease'];
    //   console.log("this.patientService.patientFemaleInfoDetails['AllergySymptomsIncrease']====", this.patientService.patientFemaleInfoDetails['AllergySymptomsIncrease']);
    //   this.patientService.patientFemaleInfoDetails['AllergySymptomsDecrease'] = form.value['AllergySymptomsDecrease'];

    //   this.patientService.patientFemaleInfoDetails['MarriedForYears'] = form.value['MarriedForYears'];
    // }

    // if (form.value['Pregnancies']) {
    //   this.patientService.patientFemaleInfoDetails['FirstTrimesterAllergy'] = form.value['FirstTrimesterAllergy'];
    //   this.patientService.patientFemaleInfoDetails['SecondTrimesterAllergy'] = form.value['SecondTrimesterAllergy'];
    //   this.patientService.patientFemaleInfoDetails['ThirdTrimesterAllergy'] = form.value['ThirdTrimesterAllergy'];
    //   this.patientService.patientFemaleInfoDetails['AfterDeliveryAllergy'] = form.value['AfterDeliveryAllergy'];

    // }
    const patientFemaleInfoDetailsObj = {

      FemaleTypeofFlow: form.value['FemaleTypeofFlow'],
      AllergySymptomsIncrease: form.value['AllergySymptomsIncrease'] ? 1 : 0,
      AllergySymptomsDecrease: form.value['AllergySymptomsDecrease'] ? 1 : 0,
      MarriedForYears: form.value['MarriedForYears'],

      FirstTrimesterAllergy: form.value['FirstTrimesterAllergy'] ? 1 : 0,
      SecondTrimesterAllergy: form.value['SecondTrimesterAllergy'] ? 1 : 0,
      ThirdTrimesterAllergy: form.value['ThirdTrimesterAllergy'] ? 1 : 0,
      AfterDeliveryAllergy: form.value['AfterDeliveryAllergy'] ? 1 : 0,


      Menstruate: form.value['Menstruate'],
      MenarcheAge: form.value['MenarcheAge'],
      RegularPeriods: form.value['RegularPeriods'],
      Pregnancies: form.value['Pregnancies'],
      IsActive: true
    };
    // let patientFemaleInfoDetailsObj = Object.assign({}, this.patientService.patientFemaleInfoDetails);
    // delete patientFemaleInfoDetailsObj.FemaleInfoId;
    patientFemaleInfoDetailsObj.IsActive = true;
    console.log("patientFemaleInfoDetailsObj====", patientFemaleInfoDetailsObj);
    this.patientService.updatepatientFemaleInfoDetails(patientFemaleInfoDetailsObj).subscribe(response => {
      console.log("responce===", response);
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}


// Aboritions: false
// AfterDeliveryAllergy: false
// AllergySymptomsDecrease: true
// AllergySymptomsIncrease: true
// CreatedByUserId: 0
// CreatedDateTime: "0001-01-01T00:00:00"
// FemalDeliveryType: null
// FemaleInfoId: 30
// FemaleTypeofFlow: ""
// FirstTrimesterAllergy: false
// HaveChildren: false
// IsActive: true
// MarriedForYears: 10
// MenarcheAge: 10
// Menstruate: "YES"
// NoofAbortions: 0
// NoofChildren: 0
// PatientDemographics: null
// PatientDemographicsId: 404
// Pregnancies: false
// ReasonforAbortion: null
// RegularPeriods: true
// SecondTrimesterAllergy: false
// StillBirthProblems: null
// StillBirths: false
// ThirdTrimesterAllergy: false