import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

// import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-alcohol-habits-details',
  templateUrl: './alcohol-habits-details.component.html',
  styleUrls: ['./alcohol-habits-details.component.css']
})

export class AlcoholHabitsDetailsComponent implements OnInit {



  alcoholHabitsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  selectedAlcoholHabits = false;

  alcoholHabitsChangeOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ]
  selectedAlcoholHabitsChange = false;

  alcoholHabitsDetailsForm: FormGroup;
  alcoholName: string;
  alcoholQuantity: string;
  selectedAlcoholFrequency: string;

  alcoholHabitsEarlierOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedAlcoholHabitsEarlier = false;

  alcoholHabitsEarlierChangeOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedAlcoholHabitsEarlierChange = false;
  alcoholHabitsEarlierForm: FormGroup;
  historicalAlcoholName: string;
  historicalAlcoholQuantity: string;

  ChewingHabitsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ]
  selectedChewingHabits = false;

  ChewingHabitsEarlierOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedChewingEarlierHabits = false;

  chewingHabitsForm: FormGroup;
  chewingSubstance: string;
  // selectedHistoricalAlcoholFrequency: string;
  alcoholHabitsChange: string;
  alcoholHabitsEarlierChange: string;
  chewingChanges: boolean;
  alcoholHabitsAnyChange: string;
  historicalAlcoholQuitDate: any;
  alcoholHabitsEarlierAnyChange: string;
  chewingQuitDate: any;
  historicalAlcoholFrequency: string;
  HistoricalChewingSubstance: string;

  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  loading = false;

  ngOnInit() {
    this.alcoholHabitsDetailsForm = this.FormBuilder.group({

      // PatientPhysicalDetailsId: this.patientService.patientHabitDetails != null ? this.patientService.patientHabitDetails.PatientHabitId : 0,
      // PatientPhysicalDetailsId: this.patientService.patientPhysicalDetails.PatientPhysicalDetailsId,
      AlcoholName: this.FormBuilder.control('', Validators.required),
      AlcoholQuantity: this.FormBuilder.control('', Validators.required),
      AlcoholHabits: this.FormBuilder.control('', Validators.required),
      AlcoholHabitsChange: this.FormBuilder.control('', Validators.required),
      AlcoholFrequency: this.FormBuilder.control('', Validators.required),
      AlcoholHabitsAnyChange: this.FormBuilder.control('', Validators.required),

      HistoricalAlcoholName: this.FormBuilder.control('', Validators.required),
      HistoricalAlcoholQuantity: this.FormBuilder.control('', Validators.required),
      HistoricaAlcoholFrequency: this.FormBuilder.control('', Validators.required),
      HistoricalAlcoholQuitDate: this.FormBuilder.control('0001-01-01T00:00:00', Validators.required),


      AlcoholHabitsEarlier: this.FormBuilder.control('', Validators.required),
      AlcoholHabitsEarlierChange: this.FormBuilder.control('', Validators.required),
      AlcoholHabitsEarlierAnyChange: this.FormBuilder.control('', Validators.required),

      ChewingHabits: this.FormBuilder.control('', Validators.required),
      ChewingHabitsEarlier: this.FormBuilder.control('', Validators.required),
      ChewingSubstance: this.FormBuilder.control('', Validators.required),
      ChewingQuitDate: this.FormBuilder.control('0001-01-01T00:00:00', Validators.required),
      HistoricalChewingSubstance: this.FormBuilder.control('', Validators.required),
    })
    // this.selectedAlcoholHabits = this.patientService.patientHabitDetails.IsActive;
    console.log("this.patientService.patientHabitDetails")
    this.selectedAlcoholHabits = this.patientService.patientHabitDetails.AlcoholConsumption;
    this.selectedAlcoholHabitsChange = this.patientService.patientHabitDetails.AnyChangesAfterAlcoholConsumption;
    this.alcoholName = this.patientService.patientHabitDetails.AlcoholName;
    this.alcoholQuantity = this.patientService.patientHabitDetails.AlcoholQuantity;
    this.alcoholHabitsAnyChange = this.patientService.patientHabitDetails.ChangesAfterAlcoholConsumption;
    this.selectedAlcoholFrequency = this.patientService.patientHabitDetails.AlcoholFrequency;


    this.selectedAlcoholHabitsEarlier = this.patientService.patientHabitDetails.HistoricalAlcoholConsumption;
    this.selectedAlcoholHabitsEarlierChange = this.patientService.patientHabitDetails.HistoricalAnyChangesAfterAlcoholConsumption;
    this.historicalAlcoholName = this.patientService.patientHabitDetails.HistoricalAlcoholName;
    this.historicalAlcoholQuantity = this.patientService.patientHabitDetails.HistoricalAlcoholQuantity;
    this.historicalAlcoholFrequency = this.patientService.patientHabitDetails.HistoricalAlcoholFrequency;
    this.alcoholHabitsEarlierAnyChange = this.patientService.patientHabitDetails.HistoricalChangesAfterAlcoholConsumption;
    this.historicalAlcoholQuitDate = this.patientService.patientHabitDetails.HistoricalAlcoholQuitDate;
    this.historicalAlcoholQuitDate = this.historicalAlcoholQuitDate.toString().substr(0, 10);
    console.log(" this.historicalAlcoholFrequency==", this.historicalAlcoholFrequency);
    // console.log("this.historicalAlcoholQuitDate==", this.historicalAlcoholQuitDate);
    // let str = this.historicalAlcoholQuitDate.split("T");
    this.selectedChewingHabits = this.patientService.patientHabitDetails.Chewing;
    this.selectedChewingEarlierHabits = this.patientService.patientHabitDetails.HistoricalChewing;
    this.chewingSubstance = this.patientService.patientHabitDetails.ChewingSubstance;
    this.chewingChanges = this.patientService.patientHabitDetails.HistoricalChewing;
    this.chewingQuitDate = this.patientService.patientHabitDetails.ChewingQuitDate;
    this.chewingQuitDate = this.chewingQuitDate.toString().substr(0, 10);
    this.HistoricalChewingSubstance = this.patientService.patientHabitDetails.HistoricalChewingSubstance;
    // console.log(" this.chewingQuitDate==", this.chewingQuitDate);

  }

  submitAlcoholHabitsDetailsForm(form) {
    this.loading = true;

    let patientHabits = form.value;
    // console.log("date ===", form.value);

    // PatientPhysicalDetailsId: this.patientService.patientHabitDetails != null ? this.patientService.patientHabitDetails.PatientHabitId : 0,
    //  0001-01-01T00:00:00
    patientHabits.ChewingQuitDate = form.value.ChewingQuitDate === "" ? new Date('0001-01-01T00:00:00') : form.value.ChewingQuitDate;
    patientHabits.HistoricalAlcoholQuitDate = form.value.HistoricalAlcoholQuitDate === "" ? new Date('0001-01-01T00:00:00') : form.value.HistoricalAlcoholQuitDate;

    // patientHabits.ChewingQuitDate =  new Date(form.value.ChewingQuitDate).toLocaleString();
    patientHabits.AlcoholConsumption = form.value.AlcoholHabits;
    patientHabits.AnyChangesAfterAlcoholConsumption = form.value.AlcoholHabitsChange;

    patientHabits.HistoricalAlcoholConsumption = form.value.AlcoholHabitsEarlier;
    patientHabits.HistoricalAnyChangesAfterAlcoholConsumption = form.value.AlcoholHabitsEarlierChange;
    patientHabits.HistoricaAlcoholFrequency = form.value.HistoricaAlcoholFrequency;
    console.log(" patientHabits.HistoricaAlcoholFrequency==", patientHabits.HistoricaAlcoholFrequency);
    patientHabits.Chewing = form.value.ChewingHabits;
    patientHabits.HistoricalChewing = form.value.ChewingHabitsEarlier;
    patientHabits.HistoricalChewingSubstance = form.value.HistoricalChewingSubstance;
    patientHabits.ChangesAfterAlcoholConsumption = form.value.AlcoholHabitsAnyChange;
    patientHabits.HistoricalChangesAfterAlcoholConsumption = form.value.AlcoholHabitsEarlierAnyChange;

    patientHabits.IsActive = true;
    // delete patientHabits.AlcoholHabitsEarlierAnyChange;
    // delete patientHabits.HistoricalAlcoholQuitDate;
    // delete patientHabits.ChewingQuitDate;
    // delete patientHabits.AlcoholHabitsAnyChange;
    // delete patientHabits.HistoricaAlcoholFrequency;
    // delete patientHabits.HistoricalChangesAfterAlcoholConsumption;
    // delete patientHabits.ChangesAfterAlcoholConsumption;
    console.log("hbdhbfshdhj=====", patientHabits);
    this.patientService.updatePatientHabitDetails(patientHabits).subscribe(response => {
      // console.log("response====", response);
      this.patientService.patientHabitDetails = response;//response["patientDemographics"];

      this.loading = false;
      this.alertsService.showToast(true);
    })
  }
}
