import { AFBirds } from './afbirds';
import { AFContactFoodDrug } from './AllergyFactors/afcontact-food-drug';
import { AFExposure } from './AllergyFactors/afexposure';
import { AFFoodAllergy } from './AllergyFactors/affood-allergy';
import { AFFurniture } from './AllergyFactors/affurniture';
import { AFHouse } from './AllergyFactors/afhouse';
import { AFMattress } from './AllergyFactors/afmattress';
import { AFInsect } from './AllergyFactors/afinsect';
import { AFAnimal } from './afanimal';
import { AFPillow } from './AllergyFactors/afpillow';
import { AFBlanket } from './AllergyFactors/afblanket';
import { AFCarpet } from './AllergyFactors/afcarpet';

export class AllergyDetails {

     // screen-1
     AFBird:AFBirds[]=[];
     AFFoodAllergy:AFFoodAllergy[]=[];
     
     AFContactFoodDrug:AFContactFoodDrug;
     // FoodAllergySymptoms :string[];
 
     // screen-2
     AFExposure:AFExposure[]=[];
     //AFFoodAllergy:AFFoodAllergy[]=[];
     FoodTextArea:string;
     DrugReaction:boolean;
     DrugTextArea:string;
 
    // screen-3
    AFFurniture:AFFurniture[]=[];
    Carpets:boolean;
    CarpetsSelect:string[];
    AFCarpet:AFCarpet[]=[];
 
    // screen-4
    AFHouse:AFHouse[]=[];
    Neighbourhood:string[];

    // screen-5
    AFMattress :AFMattress []=[];
    Insects:boolean;
    AFInsect:AFInsect[]=[];

    // screen-6
    AFAnimal:AFAnimal[]=[];
    AFPillow :AFPillow []=[];

    // screen-7
    AFBlanket:AFBlanket[]=[];

    //screen-8
    InsectBite:string;
    LatexExposure:string;
    FoodConsumption:string;
    DrugIntake:string;
    DrugInjection:string;
}
