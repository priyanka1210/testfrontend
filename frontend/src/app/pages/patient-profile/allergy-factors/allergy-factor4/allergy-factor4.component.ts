import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFMattress} from './../../../../shared/AllergyFactors/afmattress';
import { AFInsect } from './../../../../shared/AllergyFactors/afinsect';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-allergy-factor4',
  templateUrl: './allergy-factor4.component.html',
  styleUrls: ['./allergy-factor4.component.css']
})
export class AllergyFactor4Component implements OnInit {

  insectAllergyOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedInsects = false;

  insects = [{'value':'Item1', 'name':'Item1'}, {'value':'Item2', 'name': 'Item2'}, {'value':'Item3', 'name': 'Item3'},{'value':'Item4', 'name': 'Item4'}, {'value':'Item5', 'name': 'Item5'}];
  mattresses = [{mattress : 'very old'}, {mattress: 'moderately old'},{mattress : 'new'}, {mattress : 'cotton'},{mattress : 'silk cotton'},{mattress: 'Foam'}, {mattress : 'Polyster'}, {mattress : 'rubberised coir'},{mattress : 'Jamkhana'}, {mattress : 'Mat'}];
  allergyFactor4Form: FormGroup;
  loading = false;

   // screen-5
   //AFMattress :AFMattress[]=[];
   //Insects:boolean;
   //AFInsect:AFInsect[]=[];
  selectedMatress : AFMattress[]=[];
  selectedInsectsAllergy : AFInsect[]=[];
  selectedInsect:string[] = [];

  Insects:boolean;
  selectedInsectOnSelect = "";


  public VeryOldCheckBox: boolean = false;
  public ModeratelyOldCheckbox : boolean  = false
  public NewCheckbox : boolean = false
  public ModeratelyNewCheckbox : boolean = false
  public CottonCheckbox : boolean = false;

  public SilkCottonCheckBox:boolean = false
  public RubberisedCoirCheckBox:boolean = false;
  public FoamCheckBox:boolean = false;
  public JhamkhanaCheckBox:boolean = false;
  public MatCheckBox:boolean = false;

  public MosquitoesSelected:boolean = false;
  public CockroachesSelected:boolean = false; 
  public HouseFliesSelected: boolean = false;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor4Form = this.formBuilder.group({
     // birdsAllergy: this.formBuilder.array([]),
     // foodAllergy: this.formBuilder.array([])
    // Mattress: this.formBuilder.control('', Validators.required),
      Mattress_VeryOld:this.formBuilder.control('', Validators.required),
      Mattress_ModeratelyOld:this.formBuilder.control('', Validators.required),
      Mattress_New:this.formBuilder.control('', Validators.required),
      Mattress_ModeratelyNew:this.formBuilder.control('', Validators.required),
      Mattress_Cotton:this.formBuilder.control('', Validators.required),
      Mattress_SilkCotton:this.formBuilder.control('', Validators.required),
      Mattress_RubberisedCoir:this.formBuilder.control('', Validators.required),
      Mattress_Foam:this.formBuilder.control('', Validators.required),
      Mattress:this.formBuilder.control('', Validators.required),
      Mattress_Mat:this.formBuilder.control('', Validators.required),
     Insect:this.formBuilder.control('', Validators.required),
     InsectsSelect: this.formBuilder.control('', Validators.required),
     
    

    });

    for (let matress of this.patientService.patientAllergyDetails.AFMattress) {
      switch(matress.AllergyItemDescription) {
        
          case "Very Old":
              this.VeryOldCheckBox=true;
              break;
          case "Moderately Old":
              this.ModeratelyOldCheckbox=true;
              break;
          case "New":
              this.NewCheckbox=true;
              break;
          case "Moderately New":
              this.ModeratelyNewCheckbox=true;
              break;
          case "Cotton":
              this.CottonCheckbox=true;
              break;
          case "Silk Cotton":
              this.SilkCottonCheckBox=true;
              break;
          case "Rubberised Coir":
              this.RubberisedCoirCheckBox=true;
              break;
          case "Foam":
              this.FoamCheckBox=true;
              break;
          case "Jamkhana":
              this.JhamkhanaCheckBox=true;
              break;
          case "Mat":
              this.MatCheckBox=true;
              break;
         

      }




    //set all insects
    let insectAllergies = this.patientService.patientAllergyDetails.AFInsect;
    this.selectedInsects = this.patientService.patientAllergyDetails.AFInsect.length > 0; 
    for (let insectAllergy of this.patientService.patientAllergyDetails.AFInsect) {
      let insectName = this.patientService.allergyDataIdToDataName.get(insectAllergy.MasterAllergyDataId);
      this.selectedInsect.push(insectName);
    }
  }
}

  submitAllergyFactor4Form() {
    console.log('submitted allergy factor for 4');
    this.loading = true;

    if (this.VeryOldCheckBox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Very Old";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Very Old");//48;
      afMattresses.AllergyStatus = this.VeryOldCheckBox;
      this.selectedMatress.push(afMattresses);
    }

    if (this.ModeratelyOldCheckbox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Moderately Old";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately Old");//48;
      afMattresses.AllergyStatus = this.ModeratelyOldCheckbox;
      this.selectedMatress.push(afMattresses);
    }

    if (this.NewCheckbox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "New";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("New");//48;
      afMattresses.AllergyStatus = this.NewCheckbox;
      this.selectedMatress.push(afMattresses);
    }

    if (this.ModeratelyNewCheckbox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Moderately New";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately New");//48;
      afMattresses.AllergyStatus = this.ModeratelyNewCheckbox;
      this.selectedMatress.push(afMattresses);
    }
    if (this.CottonCheckbox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Cotton";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cotton");//48;
      afMattresses.AllergyStatus = this.CottonCheckbox;
      this.selectedMatress.push(afMattresses);
    }



    if (this.SilkCottonCheckBox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Silk Cotton";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Silk Cotton");//48;
      afMattresses.AllergyStatus = this.SilkCottonCheckBox;
      this.selectedMatress.push(afMattresses);
    }
    if (this.RubberisedCoirCheckBox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Rubberised Coir";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Rubberised Coir");//49;
      afMattresses.AllergyStatus = this.RubberisedCoirCheckBox;
      this.selectedMatress.push(afMattresses);
    }
    if (this.FoamCheckBox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Foam";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Foam");//50;
      afMattresses.AllergyStatus = this.FoamCheckBox;
      this.selectedMatress.push(afMattresses);
    }
    // if (this.JhamkhanaCheckBox)
    // {
    //   let afMattresses = new AFMattress();
    //   afMattresses.AllergyItemDescription = "Jhamkhana";
    //   afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
    //   afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Jhamkhana");//51;
    //   afMattresses.AllergyStatus = this.JhamkhanaCheckBox;
    //   this.selectedMatress.push(afMattresses);
    // }
    if (this.MatCheckBox)
    {
      let afMattresses = new AFMattress();
      afMattresses.AllergyItemDescription = "Mat";
      afMattresses.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afMattresses.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Mat");//52;
      afMattresses.AllergyStatus = this.MatCheckBox;
      this.selectedMatress.push(afMattresses);
    }



    var allergyFactor4 = {};
    allergyFactor4["AFMattress"] = this.selectedMatress;
    console.log(JSON.stringify(allergyFactor4));

    // if (this.MosquitoesSelected)
    // {
    //   let afInsect = new AFInsect();
    //   afInsect.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
    //   afInsect.MasterAllergyDataId = 75;
    //   afInsect.AllergyStatus = true;
    //   afInsect.AllergyStatus = this.MosquitoesSelected;
    //   this.selectedInsectsAllergy.push(afInsect);
    // }
    // if (this.CockroachesSelected)
    // {
    //   let afInsect = new AFInsect();
    //   afInsect.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
    //   afInsect.MasterAllergyDataId = 76;
    //   afInsect.AllergyStatus = true;
    //   afInsect.AllergyStatus = this.CockroachesSelected;
    //   this.selectedInsectsAllergy.push(afInsect);
    // }
   
    // if (this.HouseFliesSelected)
    // {
    //   let afInsect = new AFInsect();
    //   afInsect.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
    //   afInsect.MasterAllergyDataId = 77;
    //   afInsect.AllergyStatus = true;
    //   afInsect.AllergyStatus = this.HouseFliesSelected;
    //   this.selectedInsectsAllergy.push(afInsect);
    // }

    for (var i in this.selectedInsect) {
   
      let afInsect = new AFInsect();
    

      // let insectMasterAllergyDataId = this.patientService.allergyDataNameToDataId.get('this.selectedInsect[i]');
      // console.log("Insect Master Allergy data id",insectMasterAllergyDataId);

      afInsect.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afInsect.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get(this.selectedInsect[i]);//75;
      afInsect.AllergyStatus = true;
      afInsect.AllergyItemDescription = this.selectedInsect[i];
      this.selectedInsectsAllergy.push(afInsect);
    }
    allergyFactor4["AFInsect"] = this.selectedInsectsAllergy;
    console.log(JSON.stringify(allergyFactor4));

    
    let allergyFactor3 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor3, allergyFactor4);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response); 
      this.loading = false; 
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);

    },
    error => {
     // alert(error);
    });

  }

}
