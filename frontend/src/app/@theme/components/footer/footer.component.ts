import { Component } from '@angular/core';

@Component({
  selector: 'ngx-footer',
  styleUrls: ['./footer.component.scss'],
  template: `
  
    <span class = 'footer-content' class="created-by"> Email :<b> idoctor.solutions@gmail.com</b></span>
  
    
  `,
})
export class FooterComponent {
}
