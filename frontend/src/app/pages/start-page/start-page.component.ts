import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../shared/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-start-page',
  templateUrl: './start-page.component.html',
  styleUrls: ['./start-page.component.css']
})
export class StartPageComponent implements OnInit {
  private loginError = false;

  constructor(private authService: AuthService,
    private router: Router) { }

  ngOnInit() {
    // if error, show error box
    // if no error ad
    if (localStorage.getItem("adal.error")) {
      this.loginError = true;
    }
    if (localStorage.getItem("adal.idtoken")) {
      this.router.navigate(['/pages/patient-profile/all-patients']);
    }
  }

  logout() {
    this.authService.logout();
  }

  login() {
    this.authService.getAccesstoken()
      .subscribe(token => {
        console.log("User logged in");
      }, error => {
        console.log("[Error] Access token get Error", error);
        this.authService.login();
      })
  }

  closeErrorBanner() {
    this.authService.closeErrorBanner();
    this.loginError = false;
  }

}
