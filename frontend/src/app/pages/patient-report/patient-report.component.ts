import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'app-patient-report',
  templateUrl: './patient-report.component.html',
  styleUrls: ['./patient-report.component.css']
})
export class PatientReportComponent  {
    registerForm: FormGroup;
  submitted = false;

    onSubmitClick:boolean = false;
    options = [
      { value: 'This is value 1', label: 'Option 1' },
      { value: 'This is value 2', label: 'Option 2'},
      { value: 'This is value 3', label: 'Option 3' },
      { value: 'This is value 4', label: 'Option 4'},
      { value: 'This is value 5', label: 'Option 5' },
    ];
    option = 'This is value 1';

    options1 = [
      { value: 'This is value 1', label: 'Option 1' },
      { value: 'This is value 2', label: 'Option 2'},
      { value: 'This is value 3', label: 'Option 3' },
      { value: 'This is value 4', label: 'Option 4'},
      { value: 'This is value 5', label: 'Option 5' },
    ];
    option1 = 'This is value 1';
  constructor( private formBuilder: FormBuilder ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
        firstName: ['', Validators.required],
        // lastName: ['', Validators.required],
        // email: ['', [Validators.required, Validators.email]],
        password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    alert('SUCCESS!! :-)')
}

}
