import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-prognosis-details',
  templateUrl: './prognosis-details.component.html',
  styleUrls: ['./prognosis-details.component.css']
})
export class PrognosisDetailsComponent implements OnInit {

  prognosisDetailsForm: FormGroup;
  PrognosisDetails: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  loading = false;

  ngOnInit() {
    this.prognosisDetailsForm = this.FormBuilder.group({
      PrognosisDetails: this.FormBuilder.control('', Validators.required),
    })
    this.PrognosisDetails = this.patientService.patientFanalInitialAdviseDetails['PrognosisDetails']
  }

  submitPrognosisDetailsForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['PrognosisDetails'] = form.value['PrognosisDetails'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });

  }

}
