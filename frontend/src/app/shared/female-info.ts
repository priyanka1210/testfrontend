export class FemaleInfo {
    /*[Key]*/
    public FemaleInfoId: number;
    public HaveChildren: boolean;
    public NoofChildren: number;
    public FemalDeliveryType: string;
    public Aboritions: boolean;
    public NoofAbortions: number;
    public ReasonforAbortion: string;
    public StillBirths: boolean;
    public StillBirthProblems: string;
    public Menstruate: string;
    public MenarcheAge: number;
    public RegularPeriods: boolean;
    public FemaleTypeofFlow: string;
    public AllergySymptomsIncrease: boolean;
    public AllergySymptomsDecrease: boolean;
    public MarriedForYears: number;
    public Pregnancies: boolean;
    public FirstTrimesterAllergy: boolean;
    public SecondTrimesterAllergy: boolean;
    public ThirdTrimesterAllergy: boolean;
    public AfterDeliveryAllergy: boolean;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: String;
}
