import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test8',
  templateUrl: './test8.component.html',
  styleUrls: ['./test8.component.css']
})
export class Test8Component implements OnInit {

  lungFunctionTestOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  lungFunctionTestForm: FormGroup;
  LungFunctionTest = false;
  loading = false;
  lungFunctionTest: boolean;
  file: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  ngOnInit() {
    this.lungFunctionTestForm = this.FormBuilder.group({
      LungFunctionTest: this.FormBuilder.control('', Validators.required),
      File: this.FormBuilder.control('', Validators.required),
    });
    this.lungFunctionTest = this.patientService.patientTestsDetails.LungFunctionalTest;
    this.file = this.patientService.patientTestsDetails.LungFunctionalTestDetails;
    console.log("this.file====", this.file);

  }
  submitLungFunctionTestForm(form) {
    this.loading = true;
    this.patientService.patientTestsDetails['LungFunctionalTest'] = form.value['LungFunctionTest'];
    this.patientService.patientTestsDetails['LungFunctionalTestDetails'] = form.value['File'];
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    console.log("lung test data====", apiData);
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
