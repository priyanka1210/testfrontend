import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";

import { PatientService } from './../../../../shared/patientService.service';
import { stringify } from '@angular/core/src/render3/util';
import { AFBirds } from './../../../../shared/afbirds';
import { AFFoodAllergy } from './../../../../shared/AllergyFactors/affood-allergy';
import { AFContactFoodDrug } from './../../../../shared/AllergyFactors/afcontact-food-drug';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  //changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-allergy-factor0',
  templateUrl: './allergy-factor0.component.html',
  styleUrls: ['./allergy-factor0.component.css']
})
export class AllergyFactor0Component implements OnInit {
  
  birds = [{bird : 'Pigeon'}, {bird : 'Parrot'}, {bird : 'Hen'}];
  foodAllergies = [{'value':'Giddiness', 'name':'Giddiness'}, {'value':'Vomiting', 'name': 'Vomiting'}, {'value':'Nausea', 'name': 'Nausea'},{'value':'Watering in Mouth', 'name': 'Watering in Mouth'}, {'value':'Rashes', 'name': 'Rashes'}, {'value':'Redness', 'name': 'Redness'}];
  contactAllergyOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  contactAllergen = false;
  loading : boolean = false;

  selectedBirds : AFBirds[] = [];
  selectedFoodAllergy : AFFoodAllergy[]=[];
  contactAllergenDescription;
  allergyFactor0Form: FormGroup;
  birdsList: string[];
  FoodAllergySymptoms :string[] = [];
  selectedFood :string[] = [];

  public PigeonCheckBox:boolean = false; 
  public ParrotCheckBox:boolean = false;
  public HenCheckBox:boolean = false;
  public FoodSelection: boolean;
  //PigeonCheckBox = new FormControl();


  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor0Form = this.formBuilder.group({
     
      ContactAllergens: this.formBuilder.control('', Validators.required),
      ContactAllegensDescription: this.formBuilder.control('', Validators.required),
      FoodSelect : this.formBuilder.control('', Validators.required),
      Birds_Pigeon : this.formBuilder.control('', Validators.required),
      Birds_Hen : this.formBuilder.control('', Validators.required),
      Birds_Parrot : this.formBuilder.control('', Validators.required),

    });

    let birds = this.patientService.patientAllergyDetails["AFBird"];
    setTimeout(() =>  {
      //initialize the contact allergen factor
    this.contactAllergen = this.patientService.patientAllergyDetails['AFContactFoodDrug']['ContactAllergens'];
    this. contactAllergenDescription = this.patientService.patientAllergyDetails['AFContactFoodDrug']['ContactAllegensDescription'];

    //initialize the bird selection checkboxes
    for (let  bird  of this.patientService.patientAllergyDetails.AFBird) {
      switch(bird.AllergyItemDescription) {
        case "Pigeon":
          this.PigeonCheckBox = true;
          break;
        case "Parrot":
          this.ParrotCheckBox = true;
          break;
        case "Chicken":
          this.HenCheckBox = true;
          break;
      }
    }}); 
    
  }


  
 

 
  submitAllergyFactor0Form(){

    

      this.loading = true;
      if (this.PigeonCheckBox){
        let afBirds = new AFBirds();
        afBirds.AllergyItemDescription = "Pigeon";
        afBirds.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
        afBirds.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Pigeon");//87;
        afBirds.AllergyStatus = this.PigeonCheckBox;
        this.selectedBirds.push(afBirds);
      }
   
      if(this.ParrotCheckBox){
        let afBirds = new AFBirds();
        afBirds.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
        afBirds.AllergyItemDescription = "Parrot";
        afBirds.MasterAllergyDataId= this.patientService.allergyDataNameToDataId.get("Parrot");//88;
        afBirds.AllergyStatus = this.ParrotCheckBox;
        this.selectedBirds.push(afBirds);
      }
   
      if (this.HenCheckBox){
        let afBirds = new AFBirds();
        afBirds.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
        afBirds.AllergyItemDescription = "Chicken";
        afBirds.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Chicken");//86;
        afBirds.AllergyStatus = this.HenCheckBox;
        this.selectedBirds.push(afBirds);
      }
     
       var data = {};
       data["AFBird"] = this.selectedBirds
       console.log(JSON.stringify(data))
   
       // Right now AFFoodAllergy does not work bcz there is no masterdataid from API.
      //  for (var i in this.selectedFood) {
      //      //for each food item create the object and push it to the array
      //      let foodAllMasterId = this.patientService.allergyDataNameToDataId.get(this.selectedFood[i]);
      //      //use the above foodAllMasterId instead of hardcoded value of 43. 
      //      let afFood = new AFFoodAllergy(foodAllMasterId, this.patientService.patientDemographicsDetails.PatientDemographicsId,this.patientService.allergyDataNameToDataId.get(this.selectedFood[i]),this.selectedFood[i]);
      //      this.selectedFoodAllergy.push(afFood);
   
      //  }
    //    for (var i in this.selectedFood) {
    //     //for each food item create the object and push it to the array
    //     let foodMasterAllergyDataId = this.patientService.allergyDataNameToDataId.get(this.selectedFood[i]);
    //     //use the above foodAllMasterId instead of hardcoded value of 43. 
    //     let afFood = new AFFoodAllergy(foodMasterAllergyDataId,this.FoodSelection,this.patientService.allergyDataNameToDataId.get(this.selectedFood[i]));
    //     this.selectedFoodAllergy.push(afFood);

    //  }

     for (var i in this.selectedFood) {
   
      let afFood = new AFFoodAllergy();
    

      // let insectMasterAllergyDataId = this.patientService.allergyDataNameToDataId.get('this.selectedInsect[i]');
      // console.log("Insect Master Allergy data id",insectMasterAllergyDataId);

      afFood.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afFood.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get(this.selectedFood[i]);//75;
      afFood.AllergyStatus = true;
      afFood.AllergyItemDescription = this.selectedFood[i];
      this.selectedFoodAllergy.push(afFood);
    }






       data["AFFoodAllergy"] = this.selectedFoodAllergy;
       console.log(JSON.stringify(data));

   
   
       let afContactFoodDrug = new AFContactFoodDrug();
       if (this.contactAllergen == true) {
        //  afContactFoodDrug.FoodorBevarageAllergens = true;
        //  afContactFoodDrug.DrugReaction = true;
        //  afContactFoodDrug.ContactAllegensDescription = this.allergyFactor0Form.get('ContactAllergyDesc').value;
        //  afContactFoodDrug.ContactAllergens = this.allergyFactor0Form.get('ContactAllergy').value;
        afContactFoodDrug.ContactAllergens = this.allergyFactor0Form.get('ContactAllergens').value;
        afContactFoodDrug.ContactAllegensDescription = this.allergyFactor0Form.get('ContactAllegensDescription').value;


       }
       data["AFContactFoodDrug"] = afContactFoodDrug;
       console.log(JSON.stringify(data));
       
       this.patientService.createAllergyFactors(data).subscribe(response => {
         console.log('Patient AllergyFactor 0 Details', response);
         this.loading = false;
         console.log(this.patientService.patientAllergyDetails);
         if(this.patientService.patientAllergyDetails.AFBird[0].AllergyItemDescription == 'Pigeon'){
           this.PigeonCheckBox = true;
         } else if(this.patientService.patientAllergyDetails.AFBird[1].AllergyItemDescription == 'Parrot'){
            this.ParrotCheckBox = true;
         } else {
           this.HenCheckBox = true;
         }
        this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      
         this.alertsService.showToast(true);
         console.log("Patient Allergy Details Merged : ", this.patientService.patientAllergyDetails);
         
       },
       error => {
         //alert(error);
       });

   // }
    
   
  
  } 


    

}
