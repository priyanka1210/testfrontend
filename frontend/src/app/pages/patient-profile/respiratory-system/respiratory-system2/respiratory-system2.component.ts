import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFExposure } from './../../../../shared/AllergyFactors/afexposure';
import { Chest } from '../../../../shared/chest';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-respiratory-system2',
  templateUrl: './respiratory-system2.component.html',
  styleUrls: ['./respiratory-system2.component.css']
})
export class RespiratorySystem2Component implements OnInit {

  phlegmThicknessoptions = [
    { value: true, name: 'thick' },
    { value: false, name: 'watery' }, 
    { value: true, name: 'both' }
  ];
  selectedPlegmThickness;

  phlegmBloodOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedPhlegmBlood = false;

  phlegmSmellOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedPhlegmSmell = false;
  

  respiratorySystem2Form : FormGroup;
  loading = false;


  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.respiratorySystem2Form = this.formBuilder.group({
     
      PhlegmThickness: this.formBuilder.control('', Validators.required),
      PhlegmwithBlood: this.formBuilder.control('', Validators.required),
      PhlegmBloodColor: this.formBuilder.control('', Validators.required),
      PhlegmSmell : this.formBuilder.control('', Validators.required),
      PhlegmTiming : this.formBuilder.control('', Validators.required),

    });


    this.selectedPlegmThickness = this.patientService.patientRespiratoryDetails['PhlegmThickness'];
    this.selectedPhlegmBlood = this.patientService.patientRespiratoryDetails['PhlegmwithBlood'];
    this.selectedPhlegmSmell = this.patientService.patientRespiratoryDetails['PhlegmSmell'];


  }

  submitRespiratoySystem2Form(){
    this.loading = true;

          let patientChestDetails = this.patientService.patientRespiratoryDetails;
          
          let respiratorySystem2 = new Chest();
          respiratorySystem2.PhlegmThickness = this.respiratorySystem2Form.get('PhlegmThickness').value;
          respiratorySystem2.PhlegmwithBlood = this.respiratorySystem2Form.get('PhlegmwithBlood').value;
          respiratorySystem2.PhlegmBloodColor = this.respiratorySystem2Form.get('PhlegmBloodColor').value;
          respiratorySystem2.PhlegmSmell = this.respiratorySystem2Form.get('PhlegmSmell').value;
          respiratorySystem2.PhlegmTiming = this.respiratorySystem2Form.get('PhlegmTiming').value;

          var respiratoryDetails = Object.assign(patientChestDetails, respiratorySystem2);
          delete respiratoryDetails["CreatedDateTime"];
          delete respiratoryDetails["RespiratoryId"];
          delete respiratoryDetails["CreatedByUserId"];

          this.patientService.createChestRespiratorySystem(respiratoryDetails).subscribe(response => {
            console.log('Patient Respiratory System 2 Form response', response); 
            this.loading = false;
            //this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"];
            this.patientService.patientRespiratoryDetails = {...patientChestDetails, ...response};
            console.log('Patient Respiratory System 2 Form response',this.patientService.patientRespiratoryDetails); 
            this.alertsService.showToast(true);

          },
          error => {
           // alert(error);
          });     
        }
}
