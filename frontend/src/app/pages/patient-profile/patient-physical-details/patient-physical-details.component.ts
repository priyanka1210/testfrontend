import { Component, OnInit } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PatientService } from './../../../shared/patientService.service';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';


@Component({
  selector: 'app-patient-physical-details',
  templateUrl: './patient-physical-details.component.html',
  styleUrls: ['./patient-physical-details.component.css']
})
export class PatientPhysicalDetailsComponent implements OnInit {

  patientPhysicalForm:  FormGroup;
  patientDemographicsDetails = new PatientDemographicsDetails();


  constructor(private formBuilder: FormBuilder, private patientService: PatientService) { }

  ngOnInit() {
   /* this.patientPhysicalForm = this.formBuilder.group({
      Gender: this.formBuilder.control('', Validators.required),
      MarriatalStatus: this.formBuilder.control('', Validators.required),
      Occupation: this.formBuilder.control('', Validators.required),
     // BodySize: this.formBuilder.control('', Validators.required),
    //  SkinColour: this.formBuilder.control('', Validators.required),
      Height: this.formBuilder.control('', Validators.required),
      Weight: this.formBuilder.control('', Validators.required),

    });*/
   // this.patientPhysicalForm.patchValue({'FirstName': "Chaitra Test Patch"});
  }

  handleSubmit(){
    console.log("patient-physical-details submitted");
  }

}
