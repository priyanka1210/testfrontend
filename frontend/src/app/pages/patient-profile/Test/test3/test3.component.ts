import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test3',
  templateUrl: './test3.component.html',
  styleUrls: ['./test3.component.css']
})
export class Test3Component implements OnInit {

  cytologyOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  cytologyTestForm: FormGroup;
  selectedCytology: boolean = false;
  cytologyTestDetails: string = '';
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;


  ngOnInit() {
    this.cytologyTestForm = this.FormBuilder.group({
      CytologyTest: this.FormBuilder.control('', Validators.required),
      CytologyTestDetails: this.FormBuilder.control('', Validators.required)
    });
    this.selectedCytology = this.patientService.patientTestsDetails['CytologyTest'];
    this.cytologyTestDetails = this.patientService.patientTestsDetails['CytologyTestDetails'];
  }

  submitCytologyTestForm(form) {
    this.patientService.patientTestsDetails['CytologyTest'] = form.value['CytologyTest'];
    this.patientService.patientTestsDetails['CytologyTestDetails'] = form.value['CytologyTestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}



