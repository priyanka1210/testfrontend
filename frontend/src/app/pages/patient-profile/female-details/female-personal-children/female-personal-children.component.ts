import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-female-personal-children',
  templateUrl: './female-personal-children.component.html',
  styleUrls: ['./female-personal-children.component.css']
})
export class FemalePersonalChildrenComponent implements OnInit {

  childrenOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  abortionsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  BirthsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedChildren = false;
  selectedAbortions = false;
  selectedBirths = false;
  noofChildren: number;
  femalDeliveryType: String;
  noofAbortions: number;
  reasonforAbortion: String;
  loading = false;
  femalePersonalChildrenDetailsForm: FormGroup;
  ProblemsAfterBirth: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.femalePersonalChildrenDetailsForm = this.FormBuilder.group({
      HaveChildren: this.FormBuilder.control('', Validators.required),
      NoofChildren: this.FormBuilder.control('', Validators.required),
      FemalDeliveryType: this.FormBuilder.control('', Validators.required),
      Aboritions: this.FormBuilder.control('', Validators.required),
      NoofAbortions: this.FormBuilder.control('', Validators.required),
      ReasonforAbortion: this.FormBuilder.control('', Validators.required),
      ProblemsAfterBirth: this.FormBuilder.control('', Validators.required),
      Births: this.FormBuilder.control('', Validators.required),
    });

    this.selectedChildren = this.patientService.patientFemaleInfoDetails['HaveChildren'];
    this.noofChildren = this.patientService.patientFemaleInfoDetails['NoofChildren'];
    this.femalDeliveryType = this.patientService.patientFemaleInfoDetails['FemalDeliveryType'];
    this.selectedAbortions = this.patientService.patientFemaleInfoDetails['Aboritions'];
    this.noofAbortions = this.patientService.patientFemaleInfoDetails['NoofAbortions'];
    this.reasonforAbortion = this.patientService.patientFemaleInfoDetails['ReasonforAbortion'];
    this.ProblemsAfterBirth = this.patientService.patientFemaleInfoDetails['StillBirthProblems'];
    this.selectedBirths = this.patientService.patientFemaleInfoDetails['StillBirths'];
    console.log(" this.selectedBirths==", this.selectedBirths);
  }

  submitFemalePersonalChildrenDetailsForm(form) {

    this.patientService.patientFemaleInfoDetails['HaveChildren'] = form.value['HaveChildren'];
    this.patientService.patientFemaleInfoDetails['NoofChildren'] = form.value['HaveChildren'] ? form.value['NoofChildren'] : this.patientService.patientFemaleInfoDetails['NoofChildren'];
    this.patientService.patientFemaleInfoDetails['FemalDeliveryType'] = form.value['HaveChildren'] ? form.value['FemalDeliveryType'] : this.patientService.patientFemaleInfoDetails['FemalDeliveryType'];
    this.patientService.patientFemaleInfoDetails['Aboritions'] = form.value['Aboritions'];
    this.patientService.patientFemaleInfoDetails['NoofAbortions'] = form.value['Aboritions'] ? form.value['NoofAbortions'] : this.patientService.patientFemaleInfoDetails['NoofAbortions'];
    this.patientService.patientFemaleInfoDetails['ReasonforAbortion'] = form.value['Aboritions'] ? form.value['ReasonforAbortion'] : this.patientService.patientFemaleInfoDetails['ReasonforAbortion'];
    this.patientService.patientFemaleInfoDetails['StillBirthProblems'] = form.value['ProblemsAfterBirth'];
    this.patientService.patientFemaleInfoDetails['StillBirths'] = form.value['Births'];
    let patientFemaleInfoDetailsObj = Object.assign({}, this.patientService.patientFemaleInfoDetails);
    patientFemaleInfoDetailsObj.IsActive = true;
    delete patientFemaleInfoDetailsObj.FemaleInfoId;
    this.loading = true;
    console.log("patientFemaleInfoDetailsObj")
    this.patientService.updatepatientFemaleInfoDetails(patientFemaleInfoDetailsObj).subscribe(response => {
      console.log("responce===", response);
      this.loading = false;
      this.alertsService.showToast(true);
    })
  }

}
