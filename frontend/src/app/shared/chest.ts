export class Chest {
    /*[Key]*/
    public RespiratoryId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[ForeignKey("PatientDemographicsId")]*/
    public Wheezing: boolean;
    public WheezingTime: string;
    public Breathlessness: boolean;
    public BreathlessnessTime: string;
    public ChestPain: boolean;
    public ChestPainArea: string;
    public ChestRhonchiType: string;
    public Cough: boolean;
    public CoughContinous: boolean;
    public CoughType: string;
    public PhelgmType: string;
    public PhlegmColor: string;
    public PhlegmStrength: string;
    public PhlegmThickness: boolean;
    public PhlegmwithBlood: boolean;
    public PhlegmBloodColor: string;
    public PhlegmSmell: boolean;
    public PhlegmTiming: string;
    public ChestShape: string;
    public ChestCrepitations: boolean;
    public ChestRales: boolean;
    public ChestRhonchi: boolean;
    public WheezingType: string;
    public WheezingTone: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    public IsActive: boolean;
    public PatientDemographics: string;
}
