import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AllergyHistory } from './../../../../shared/allergy-history';
import { AlertsService } from './../../../../shared/alerts.service';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';


@Component({
  selector: 'app-allergy-history0',
  templateUrl: './allergy-history0.component.html',
  styleUrls: ['./allergy-history0.component.css']
})
export class AllergyHistory0Component implements OnInit {


  placeAllergyHistoryOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedPlaceAllergyHistory = false;
  selectedPlace1;
  selectedPlace1Symptoms;
  selectedPlace2;
  selectedPlace2Symptoms;
  selectedPlace3;
  selectedPlace3Symptoms;
  selectedPlace4;
  selectedPlace4Symptoms;
  
  allergyHistory0Form : FormGroup;
 
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;
  
  ngOnInit() {
    
    this.allergyHistory0Form = this.formBuilder.group({
      PatientDemographicsId : (this.patientService.patientDemographicsDetails) ? this.patientService.patientDemographicsDetails.PatientDemographicsId : 0,
      DifferPerPlace: this.formBuilder.control('', Validators.required),
      Place1 : this.formBuilder.control('', Validators.required),
      Place1Symptom : this.formBuilder.control('', Validators.required),
      Place2 : this.formBuilder.control('', Validators.required),
      Place2Symptoms : this.formBuilder.control('', Validators.required),
      Place3 : this.formBuilder.control('', Validators.required),
      Place3Symptoms : this.formBuilder.control('', Validators.required),
      Place4 : this.formBuilder.control('', Validators.required),
      Place4Symptoms : this.formBuilder.control('', Validators.required),
     
    });

  ///  if (this.patientService.patientAllergyHistoryDetails != null) {

     /// this.allergyHistory0Form.patchValue({'DifferPerPlace': this.patientService.patientAllergyHistoryDetails['DifferPerPlace'],'Place1':this.patientService.patientAllergyHistoryDetails['Place1']});
      // this.selectedBodySize = this.patientService.patientAllergyHistoryDetails["Place1"];
      // this.selectedSkinColour = this.patientService.patientAllergyHistoryDetails["Place2"];
    /// }

    //  this.patientContactForm.patchValue({'FirstName':this.patientService.patientDemographicsDetails['FirstName'], 'MiddleName':this.patientService.patientDemographicsDetails['MiddleName'],'LastName':this.patientService.patientDemographicsDetails['LastName'],'Mobile':this.patientService.patientDemographicsDetails['Mobile'],'EmailID':this.patientService.patientDemographicsDetails['EmailID'],'DateofBirth':this.patientService.patientDemographicsDetails['DateofBirth'],
    //  'SecondaryContactNumber':this.patientService.patientDemographicsDetails['SecondaryContactNumber'],'ReferringDoctorName':this.patientService.patientDemographicsDetails['ReferringDoctorName'],'ReferringDoctortNumber':this.patientService.patientDemographicsDetails['ReferringDoctortNumber'],'Address1':this.patientService.patientDemographicsDetails['Address1'],'Address2':this.patientService.patientDemographicsDetails['Address2']});
    
    this.selectedPlaceAllergyHistory = this.patientService.patientAllergyHistoryDetails.DifferPerPlace;
    this.selectedPlace1 = this.patientService.patientAllergyHistoryDetails['Place1'];
    this.selectedPlace1Symptoms = this.patientService.patientAllergyHistoryDetails['Place1Symptom'];
    this.selectedPlace2 = this.patientService.patientAllergyHistoryDetails['Place2'];
    this.selectedPlace2Symptoms = this.patientService.patientAllergyHistoryDetails['Place2Symptoms'];
    this.selectedPlace3 = this.patientService.patientAllergyHistoryDetails['Place3'];
    this.selectedPlace3Symptoms = this.patientService.patientAllergyHistoryDetails['Place3Symptoms'];
    this.selectedPlace4 = this.patientService.patientAllergyHistoryDetails['Place4'];
    this.selectedPlace4Symptoms = this.patientService.patientAllergyHistoryDetails['Place4Symptoms'];


  }

  submitAllergyHistory0Form() {


    this.loading = true;

    // if (this.patientService.patientAllergyHistoryDetails === undefined) {
    //   console.log("Patient allergy history details are not present");
    //   this.loading = false;
    //   return;
    // }
    // if (this.patientService.patientAllergyHistoryDetails === null) {
    //   console.log("Patient allergy history details are not present");
    //   this.loading = false;
    //   return;
    // }
    // if (Object.entries(this.patientService.patientAllergyHistoryDetails).length === 0) {
    //   console.log("Patient allergy history details are not present");
    //   this.loading = false;
    //   return;
    // }
    

    let allergyHistory = new AllergyHistory();
    
    if (this.selectedPlaceAllergyHistory == true) {
     // allergyHistory.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      allergyHistory.DifferPerPlace = this.allergyHistory0Form.get('DifferPerPlace').value;//true;
      allergyHistory.Place1 = this.allergyHistory0Form.get('Place1').value;
      allergyHistory.Place1Symptom = this.allergyHistory0Form.get('Place1Symptom').value;
      allergyHistory.Place2 = this.allergyHistory0Form.get('Place2').value;
      allergyHistory.Place2Symptoms = this.allergyHistory0Form.get('Place2Symptoms').value;
      allergyHistory.Place3 = this.allergyHistory0Form.get('Place3').value;
      allergyHistory.Place3Symptoms = this.allergyHistory0Form.get('Place3Symptoms').value;
      allergyHistory.Place4 = this.allergyHistory0Form.get('Place4').value;
      allergyHistory.Place4Symptoms = this.allergyHistory0Form.get('Place4Symptoms').value;

     // let allergyHistoryDetailsOriginal = this.patientService.patientAllergyHistoryDetails;
     // allergyHistoryDetailsOriginal = Object.assign(allergyHistoryDetailsOriginal, allergyHistory);


      allergyHistory = new FieldCleanup().removeIdFields( allergyHistory);
      this.patientService.createAllergyHistory(allergyHistory).subscribe(response => {
        console.log('Patient Allergy History 0 Form response', response);
      
        this.patientService.patientAllergyHistoryDetails = response;

        console.log("Global Allergy History Details", this.patientService.patientAllergyHistoryDetails);
        console.log('Indivisual History Details DifferPerPlace', this.patientService.patientAllergyHistoryDetails['DifferPerPlace']);
        console.log('Indivisual History Details Place1', this.patientService.patientAllergyHistoryDetails['Place1']);
        console.log('Indivisual History Details Place2', this.patientService.patientAllergyHistoryDetails['Place2']);


        this.loading = false;
        this.alertsService.showToast(true);
       
      },
      error => {
        //alert(error);
      });
     
    }
  
      //alert(this.allergyHistory0Form.value);

  }

}
