import { Component, Input, OnInit } from '@angular/core';

import { NbSearchService, NbMenuService, NbSidebarService } from '@nebular/theme';
import { UserData } from '../../../@core/data/users';
import { AnalyticsService } from '../../../@core/utils';
import { LayoutService } from '../../../@core/utils';
import { PatientService } from './../../../shared/patientService.service';
import { Router } from "@angular/router";
import { AuthService } from '../../../shared/auth.service';

@Component({
  selector: 'ngx-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit {

  @Input() position = 'normal';

  user: any;
  value = '';
  PatientPhone: number;
  private logginInUserName: string = '';

  userMenu = [{ title: 'Profile' }, { title: 'Log out' }];

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private userService: UserData,
    private analyticsService: AnalyticsService,
    private layoutService: LayoutService,
    private searchService: NbSearchService,
    private patientService: PatientService,
    private router: Router,
    private authService: AuthService) {

    console.log("Searching Patient By Contact Number");
    this.searchService.onSearchSubmit()
      .subscribe(response => {
        console.log('Patient Details atter search', response);
        this.router.navigate(['/pages/patient-profile/update-patient']);
      })
  }

  ngOnInit() {
    // this.userService.getUsers()
    //   .subscribe((users: any) => this.user = users.nick);

    this.logginInUserName = this.getUserName();

    this.menuService.onItemClick()
      .subscribe(data => {
        if (data.item.title == 'Log out') {
          this.authService.logout();
        }
      });
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();

    return false;
  }

  goToHome() {
    this.menuService.navigateHome();
  }

  startSearch() {
    this.analyticsService.trackEvent('startSearch');
    /* console.log("Searching Patient By Contact Number");
     this.patientService.searchPatientDetails(this.PatientPhone).subscribe(response => {
       console.log('Patient Details atter search', response);
       this.patientService.patientDemographicsDetails = response["PatientDemographics"];
       this.patientService.patientPhysicalDetails = response["PatientPhysicalDetails"];
       this.router.navigate(['/pages/patient-profile/update-patient']);
     },
     error => {
       alert(error);
     });*/
  }

  getUserName(): string {
    return this.authService.getUserName();
  }





}
