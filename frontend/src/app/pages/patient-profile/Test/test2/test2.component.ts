import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {

  stoolOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  stoolTestForm: FormGroup;
  selectedStool: boolean = false;
  stoolTestDetails: string = '';
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;


  ngOnInit() {
    this.stoolTestForm = this.FormBuilder.group({
      StoolTest: this.FormBuilder.control('', Validators.required),
      StoolTestDetails: this.FormBuilder.control('', Validators.required)
    });

    this.selectedStool = this.patientService.patientTestsDetails['StoolTest'];
    this.stoolTestDetails = this.patientService.patientTestsDetails['StoolTestDetails'];
  }

  submitStoolTestForm(form) {
    this.patientService.patientTestsDetails['StoolTest'] = form.value['StoolTest'];
    this.patientService.patientTestsDetails['StoolTestDetails'] = form.value['StoolTestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}

