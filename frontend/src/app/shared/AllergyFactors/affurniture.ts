export class AFFurniture {
     /*[Key]*/
     public AfFurnitureId: number;
     /*[Required]*/
     public PatientDemographicsId: number;
     /*[Required]*/
     public MasterAllergyDataId: number;
     /*[Required]*/
     public AllergyItemDescription: string;
     /*[Required]*/
     public AllergyStatus: boolean;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;
     /*[ForeignKey("PatientDemographicsId")]*/
}
