export class PatientDemographics {
    PatientDemographicsId:number;
    FirstName:string;
    MiddleName:string;
    LastName:string;
    Mobile:number;
    
    SecondaryContactNumber:number;
    ReferringDoctorName:string;
    ReferringDoctortNumber:number;
    EmailID:string;
    DateofBirth:Date;
    Gender:string;
    MarriatalStatus:string;
    Address1:string;
    Address2:string;
    City:string;
    ZipCode:number;
    State:string;
    Occupation:string;
    CreatedDate:Date;
    UpdatedDate:Date;
}
