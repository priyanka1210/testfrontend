export class FamilyHistory {
    /*[Key]*/
    public FamilyHistoryId: number;
    public FamilyAllergy: boolean;
    public AllergyRelation: string;
    public AllergyDescription: string;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
}
