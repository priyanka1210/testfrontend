export class SkinDetails {
    /*[Key]*/
    public SkinId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public SkinItching: boolean;
    public SkinItchingArea: string;
    public SkinItchingAppearArea: string;
    public SkinSymptomsMaster: string;
    public SkinSymptomsArea: string;
    public SkinDryness: boolean;
    public SkinRedness: boolean;
    public SkinAppearandDisAppear: boolean;
    public SkinPatchColor: string;
    public PatchColorTiming: string;
    public PatchColorIncrease: boolean;
    public PatchColorAppear: string;
    public PatchColorDisappear: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: string;
}



// SkinAppearandDisAppear: true

