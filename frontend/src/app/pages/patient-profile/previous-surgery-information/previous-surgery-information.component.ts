import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-previous-surgery-information',
  templateUrl: './previous-surgery-information.component.html',
  styleUrls: ['./previous-surgery-information.component.css']
})
export class PreviousSurgeryInformationComponent implements OnInit {

  surgeryOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  previousSurgeryForm: FormGroup;
  selectedSurgery = false;
  whatSurgery: string;
  whenSurgery: string;
  effects: string;
  previousTreatmentDetails: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  loading = false;

  ngOnInit() {
    this.previousSurgeryForm = this.FormBuilder.group({
      Surgery: this.FormBuilder.control('', Validators.required),
      WhatSurgery: this.FormBuilder.control('', Validators.required),
      WhenSurgery: this.FormBuilder.control('', Validators.required),
      Effects: this.FormBuilder.control('', Validators.required),
      PreviousTreatmentDetails: this.FormBuilder.control('', Validators.required),
    })
    console.log("this.patientService.patientPastPresentHistoryDetails===", this.patientService.patientPastPresentHistoryDetails);
    this.selectedSurgery = this.patientService.patientPastPresentHistoryDetails.AnySurgery;
    this.whatSurgery = this.patientService.patientPastPresentHistoryDetails.WhatSurgery;
    this.whenSurgery = this.patientService.patientPastPresentHistoryDetails.whenSurgery;
    console.log(" this.whenSurgery ==", this.whenSurgery);
    this.effects = this.patientService.patientPastPresentHistoryDetails.EffectsSurgery;
    this.previousTreatmentDetails = this.patientService.patientPastPresentHistoryDetails.PreviousTreatmentData;

  }
  submitPreviousSurgeryForm(form) {
    console.log("jhjkhkjkjjkvdjk");
    console.log("form==========", form);
    // let apiData = this.patientService.patientPastPresentHistoryDetails;
    // console.log("vhhjhgh", this.patientService.patientPastPresentHistoryDetails.FamilyAllergyHistory);
    // console.log("apiData=====", apiData);
    this.patientService.patientPastPresentHistoryDetails['AnySurgery'] = form.value['Surgery'];
    console.log(" this.patientService.patientPastPresentHistoryDetails['AnySurgery']====", this.patientService.patientPastPresentHistoryDetails['AnySurgery']);
    this.patientService.patientPastPresentHistoryDetails['WhatSurgery'] = form.value['WhatSurgery'];
    this.patientService.patientPastPresentHistoryDetails['WhenSurgery'] = form.value['WhenSurgery'];
    this.patientService.patientPastPresentHistoryDetails['EffectsSurgery'] = form.value['Effects'];
    this.patientService.patientPastPresentHistoryDetails['PreviousTreatmentData'] = form.value['PreviousTreatmentDetails'];
    // this.patientService.patientPastPresentHistoryDetails['PatchColorDisappear'] = form.value['PatchColorDisappear'];
    this.patientService.patientPastPresentHistoryDetails.IsActive = true;
    this.loading = true;
    let apiData = this.patientService.patientPastPresentHistoryDetails;

    delete apiData.PastHistoryId;
    console.log("apiData=====", apiData);
    this.patientService.updatePatientPastPresentHistoryDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

  // submitPreviousSurgeryForm(form) {
  //   console.log("form======", form);
  //   this.patientService.patientTestsDetails['UrineTest'] = form.value['UrineTest'];
  //   console.log("this.patientService.patientTestsDetails['UrineTest']", this.patientService.patientTestsDetails['UrineTest']);
  //   this.patientService.patientTestsDetails['UrineTestDetails'] = form.value['UrineTestDetails'];
  //   this.loading = true;
  //   let apiData = this.patientService.patientTestsDetails;
  //   delete apiData.PatientTestId;
  //   console.log("apiData===", apiData);
  //   this.patientService.updatepatientTests(apiData).subscribe(response => {
  //     this.loading = false;
  //     this.alertsService.showToast(true);
  //   });
  // }
}
