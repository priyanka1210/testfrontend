export class PastPresentHistoryDetails {
     // screen-1
     MajorDiseases: string;
     // PreviousAllergySymptoms: string[];
     PreviousAllergy: string;
     //screen-2
     ProblemStartedTime: string;

     //screen-3
     AnySurgery: boolean;
     WhatSurgery: string;
     whenSurgery: string;
     EffectsSurgery: string;
     PreviousTreatmentDetails: string;
     public IsActive: boolean;
     AllergyType: string;
     CreatedByUserId: number;
     CreatedDateTime: Date;
     FamilyAllergyHistory: string;
     FeverSymptoms: string;
     FeverWhen: string;
     PastHistoryId: number;
     PatientDemographicsId: number;
     PreviousTreatmentData: string;
     Relatives: string;
}


// AllergyType: "normal"
// AnySurgery: true
// CreatedByUserId: 1
// CreatedDateTime: "2018-10-10T00:00:00"
// EffectsSurgery: "back pain"
// FamilyAllergyHistory: true
// FeverSymptoms: true
// FeverWhen: "10 days"
// IsActive: true
// MajorDiseases: "malariya"
// PastHistoryId: 2
// PatientDemographicsId: 314
// PreviousAllergy: "dust22"
// PreviousTreatmentData: "not present"
// Relatives: "mother"
// WhatSurgery: "leg"
