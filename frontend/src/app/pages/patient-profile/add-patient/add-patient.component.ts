import { Component, OnInit } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PatientService } from './../../../shared/patientService.service';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';
import { PhysicalDetails } from './../../../shared/physical-details';
import { NbToastrService } from '@nebular/theme';
import { AlertsService } from './../../../shared/alerts.service';
import { Router } from "@angular/router";
import { Chest } from '../../../shared/chest';
import { Tests } from '../../../shared/tests';
import { SkinDetails } from '../../../shared/skin-details';
import { HairDetails } from '../../../shared/hair-details';
import { Bowel } from '../../../shared/bowel';
import { PastPresentHistoryDetails } from '../../../shared/past-present-history-details';
import { NonAllergicGerdDetails } from '../../../shared/non-allergic-gerd-details';
import { FanalInitialAdviseDetails } from '../../../shared/fanal-initial-advise-details';

@Component({
  selector: 'app-add-patient',
  templateUrl: './add-patient.component.html',
  styleUrls: ['./add-patient.component.css']
})
export class AddPatientComponent implements OnInit {
  min: Date;
  max: Date;
  expanded: boolean = false;

  loading: boolean = false;
  patientContactDetails = ["FirstName", "MiddleName", "LastName", "Gender", "MaritalStatus", "Occupation", "EmailID", "DateofBirth", "Mobile", "SecondaryContactNumber", "ReferringDoctorName", "ReferringDoctorContactNumber", "Address1", "Address2"];
  patientPhysicalDetails = ["Size", "Color", "Height", "Weight", "SymptomsAtOccupation", "SymptomsSelectedAtOccupation", "ExposureToSun"];
  // , "SymptomsAtOccupation", "SymptomsSelectedAtOccupation"

  genders = [{ 'value': 'Male', 'name': 'Male' }, { 'value': 'Female', 'name': 'Female' }, { 'value': 'Transgender', 'name': 'Transgender' }];

  occupationSymptomsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedSymptomsAtOccupation = false;

  sunExposureOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedExposureToSun = false;


  MentalStatusObj: Object[] = [
    { value: 1, name: 'Normal' },
    { value: 2, name: 'Anxious' },
    { value: 3, name: 'Tense' },
    { value: 4, name: 'Depressed' },
  ];
  public selectedMentalStatus: Object[];
  // patientContactForm:  FormGroup;
  // patientPhysicalForm: FormGroup;
  patientContactAndPhysicalForm: FormGroup;
  submitted = false;

  selectedGender = "";
  selectedMaritalStatus = "";
  selectedOccupation = "";
  selectedBodySize = "";
  selectedSkinColour = "";
  MentalStatus: any;




  constructor(protected dateService: NbDateService<Date>, private formBuilder: FormBuilder, private patientService: PatientService, private alertsService: AlertsService, private router: Router) {
    this.min = this.dateService.addMonth(this.dateService.today(), -1200);
    this.max = this.dateService.addMonth(this.dateService.today(), 0);

  }


  ngOnInit() {
    this.patientContactAndPhysicalForm = this.formBuilder.group({
      FirstName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      MiddleName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      LastName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      Mobile: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      EmailID: ['', [Validators.required, Validators.email]],
      DateofBirth: ['', Validators.required],
      SecondaryContactNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      ReferringDoctorName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      ReferringDoctorContactNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      Address1: this.formBuilder.control('', Validators.required),
      Address2: this.formBuilder.control(''),
      Gender: this.formBuilder.control('', Validators.required),
      MaritalStatus: this.formBuilder.control('', Validators.required),

      Occupation: this.formBuilder.control('', Validators.required),

      SymptomsAtOccupation: this.formBuilder.control(''),
      SymptomsSelectedAtOccupation: this.formBuilder.control(''),
      ExposureToSun: this.formBuilder.control('', Validators.required),
      Size: this.formBuilder.control('', Validators.required),
      Color: this.formBuilder.control('', Validators.required),
      Height: ['', Validators.required],
      Weight: ['', Validators.required],
      MentalStatus: ['', Validators.required],

    });


  }

  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();

    }
  }
  submitPatientForm(form) {
    console.log("hjkjhusdfhcjksdjfkhjk", form);
    // alert('click submit');

    this.submitted = true;

    // stop here if form is invalid
    if (this.patientContactAndPhysicalForm.invalid) {
      return;
    }



    let patientDemographicsData = new PatientDemographicsDetails();
    for (var field in this.patientContactDetails) {
      if (this.patientContactAndPhysicalForm.get(this.patientContactDetails[field]) === null) {
        console.log("Field name :" + this.patientContactDetails[field] + " Value is null");
      } else {
        console.log("Field name :" + this.patientContactDetails[field] + " Value: " + this.patientContactAndPhysicalForm.get(this.patientContactDetails[field]).value);
        patientDemographicsData[this.patientContactDetails[field]] = this.patientContactAndPhysicalForm.get(this.patientContactDetails[field]).value;
      }


    }
    console.log("*****" + patientDemographicsData);

    let patientPhysialData = new PhysicalDetails();
    for (var field in this.patientPhysicalDetails) {
      if (this.patientContactAndPhysicalForm.get(this.patientPhysicalDetails[field]) === null) {
        console.log("Field name :" + this.patientPhysicalDetails[field] + " Value is null");
      } else {
        console.log("Field name :" + this.patientPhysicalDetails[field] + " Value: " + this.patientContactAndPhysicalForm.get(this.patientPhysicalDetails[field]).value);
      }
      patientPhysialData[this.patientPhysicalDetails[field]] = this.patientContactAndPhysicalForm.get(this.patientPhysicalDetails[field]).value;


    }
    console.log(" this.patientService.patientPhysicalDetails..............", this.patientService.patientPhysicalDetails);
    // this.patientService.patientPhysicalDetails['SymptomsAtOccupation'] = form.value['SymptomsAtOccupation'];
    // this.patientService.patientPhysicalDetails['SymptomesSelectedAtOccupation'] = form.value['SymptomesSelectedAtOccupation'];
    // patientPhysialData['SymptomsAtOccupation'] =  this.patientService.patientPhysicalDetails['SymptomsAtOccupation'];
    // patientPhysialData['SymptomesSelectedAtOccupation'] = this.patientService.patientPhysicalDetails['SymptomesSelectedAtOccupation'];
    this.patientService.patientPhysicalDetails.MentalStatus = form.value['MentalStatus'] ? JSON.stringify(form.value.MentalStatus) : JSON.stringify(this.selectedMentalStatus)

    patientPhysialData['MentalStatus'] = this.patientService.patientPhysicalDetails.MentalStatus;

    // this.patientService.patientPastPresentHistoryDetails['PreviousAllergy'] = form.value['PreviousAllergySymptoms'] ? JSON.stringify(form.value.PreviousAllergySymptoms) : JSON.stringify(this.selectedPreviousAllergySymptoms);

    console.log(" patientPhysialData[this.MentalStatus]===", patientPhysialData['MentalStatus']);
    console.log("*****" + patientPhysialData);

    console.log("Create Patient Form Submitted with FormBuilder", this.patientContactAndPhysicalForm.value);


    this.loading = true;
    console.log("Patient contact form :" + this.patientContactAndPhysicalForm.value);
    console.log("patientPhysialData...////////", patientPhysialData);
    this.patientService.createPatientDetails(patientDemographicsData, patientPhysialData)
      .subscribe(response => {
        console.log("Patient Personal Details", response);
        this.loading = false;
        this.patientService.patientDemographicsDetails = response["patientDemographics"];
        this.patientService.patientPhysicalDetails = response["patientPhysicalDetails"];
        this.alertsService.showToast(true);
        this.router.navigate(['/pages/patient-profile/search-patient']);
        this.patientService.patientRespiratoryDetails = new Chest();
        this.patientService.patientTestsDetails = new Tests();
        this.patientService.patientSkinDetails = new SkinDetails();
        this.patientService.patientHairDetails = new HairDetails();
        this.patientService.patientBowelDetails = new Bowel();
        this.patientService.patientPastPresentHistoryDetails = new PastPresentHistoryDetails();
        this.patientService.patientNonAllergicGerdDetails = new NonAllergicGerdDetails();
        this.patientService.patientFanalInitialAdviseDetails = new FanalInitialAdviseDetails();
      },
        error => {
          console.log('error occured', error);
          console.log("Error Value Displayed", error["error"]["value"]);
          this.loading = false;
          alert(error["error"]["value"]);
          //this.alertsService.showError(error);

          //alert(error["error"]["value"]);


        });
  }

  get f() {
    return this.patientContactAndPhysicalForm.controls;

  }

}
