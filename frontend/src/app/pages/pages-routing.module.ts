import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AddPatientComponent } from './patient-profile/add-patient/add-patient.component';
import { UpdatePatientComponent } from './patient-profile/update-patient/update-patient.component';
import { PatientReportComponent } from './patient-report/patient-report.component';
import { SearchPatientComponent } from './patient-profile/search-patient/search-patient.component';
import { AllPatientsComponent } from './patient-profile/all-patients/all-patients.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { StartPageComponent } from './start-page/start-page.component';
import { AuthenticationGuard } from 'microsoft-adal-angular6';

const routes: Routes = [
  { path: '', component: StartPageComponent },
  {
    path: '', component: PagesComponent,

    children: [
      // { path: 'dashboard', component: DashboardComponent},
      /*{
        path: 'iot-dashboard',
        component: DashboardComponent,
      },*/


      {
        path: 'patient-profile',
        children: [
          { path: 'all-patients', component: AllPatientsComponent },
          { path: 'add-patient', component: AddPatientComponent },
          { path: 'update-patient', component: UpdatePatientComponent },
          { path: 'search-patient', component: SearchPatientComponent },
          // { path: 'test-patient', component: TestComponent },
          { path: '**', component: NotFoundComponent }

        ],
        canActivate: [AuthenticationGuard]
      },
      { path: 'doctor-profile', component: DoctorProfileComponent, canActivate: [AuthenticationGuard] },
      { path: 'patient-report', component: PatientReportComponent, canActivate: [AuthenticationGuard] },
      { path: '**', component: NotFoundComponent }

    ]

  }];
/*
children: [{
  path: 'dashboard',
  component: ECommerceComponent,
}, {
  path: 'iot-dashboard',
  component: DashboardComponent,
}, {
  path: 'ui-features',
  loadChildren: './ui-features/ui-features.module#UiFeaturesModule',
}, {
  path: 'modal-overlays',
  loadChildren: './modal-overlays/modal-overlays.module#ModalOverlaysModule',
}, {
  path: 'extra-components',
  loadChildren: './extra-components/extra-components.module#ExtraComponentsModule',
}, {
  path: 'bootstrap',
  loadChildren: './bootstrap/bootstrap.module#BootstrapModule',
}, {
  path: 'maps',
  loadChildren: './maps/maps.module#MapsModule',
}, {
  path: 'charts',
  loadChildren: './charts/charts.module#ChartsModule',
}, {
  path: 'editors',
  loadChildren: './editors/editors.module#EditorsModule',
}, {
  path: 'forms',
  loadChildren: './forms/forms.module#FormsModule',
}, {
  path: 'tables',
  loadChildren: './tables/tables.module#TablesModule',
}, {
  path: 'miscellaneous',
  loadChildren: './miscellaneous/miscellaneous.module#MiscellaneousModule',
}, {
  path: '',
  redirectTo: 'dashboard',
  pathMatch: 'full',
}, {
  path: '**',
  component: NotFoundComponent,
}],*/


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers: [
  //   { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
  //   AuthService
  // ],
})
export class PagesRoutingModule {
}
