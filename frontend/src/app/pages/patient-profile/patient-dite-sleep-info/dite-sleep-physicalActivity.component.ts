import { OnInit, Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from '../../../shared/alerts.service';
import { DietDetails } from '../../../shared/diet-details'


@Component({
    selector: 'app-dite-sleep-physicalActivity',
    templateUrl: './dite-sleep-physicalActivity.component.html',
    styleUrls: ['./dite-sleep-physicalActivity.component.css']
})


export class DiteSleepPhysicalActivityComponent implements OnInit {
    public DietSleepObj: DietDetails[] = [
        new DietDetails(1, 43, "Vegetarian", " ", " ", " ", " ", " "),
        new DietDetails(2, 43, "Non-Vegetarian", " ", " ", " ", " ", " "),
        new DietDetails(3, 43, "Vegetarian + Egg", " ", " ", " ", " ", " "),
        new DietDetails(4, 43, "Vegan", " ", " ", " ", " ", " "),
        new DietDetails(5, 43, "Vegetarian + sea food", " ", " ", " ", " ", " "),
    ];

    public EatingObj: DietDetails[] = [
        new DietDetails(1, 43, " ", "Hastily", " ", " ", " ", " "),
        new DietDetails(2, 43, " ", "Chews Well", " ", " ", " ", " "),
        new DietDetails(3, 43, " ", "In a Hurry", " ", " ", " ", " "),
    ];

    public SnoringObj: DietDetails[] = [
        new DietDetails(1, 43, " ", " ", "Item1", " ", " ", " "),
        new DietDetails(2, 43, " ", " ", "Item2", " ", " ", " "),
        new DietDetails(3, 43, " ", " ", "Item3", " ", " ", " "),
    ];

    public IncreasedGasObj: DietDetails[] = [
        new DietDetails(1, 43, " ", " ", " ", "Item1", " ", " "),
        new DietDetails(2, 43, " ", " ", " ", "Item2", " ", " "),
        new DietDetails(3, 43, " ", " ", " ", "Item3", " ", " "),
    ];

    public AppetiteObj: DietDetails[] = [
        new DietDetails(1, 43, " ", " ", " ", " ", "Good", " "),
        new DietDetails(2, 43, " ", " ", " ", " ", "Bad", " "),
        new DietDetails(3, 43, " ", " ", " ", " ", "Voracious or Uncontrolled", " "),
    ];

    public PhysicalActivityObj: DietDetails[] = [
        new DietDetails(1, 43, " ", " ", " ", " ", " ", "Sedentary"),
        new DietDetails(2, 43, " ", " ", " ", " ", " ", "Active"),
        new DietDetails(3, 43, " ", " ", " ", " ", " ", "Very Active"),
        new DietDetails(3, 43, " ", " ", " ", " ", " ", "Active but intermittent Sedentary"),
    ];

    public SleepPatternObj: Object[] = [
        { value: 1, name: 'Sleep delayed' },
        { value: 2, name: 'Wake-up too early' },
        { value: 3, name: 'Too many dreams' },
        { value: 4, name: 'Do not feel rested well after waking up' },
        { value: 5, name: 'Symptoms delay Onset' },
        { value: 6, name: 'Symptoms wake me up' }
    ]


    public dietFollowed: String;
    public eating: String;
    public appetite: String;
    public physicalActivity: String;
    public snoring: String;
    public increasedGas: String;
    public selectedSleepPatterns: Object[];

    diteSleepPhysicalActivityForm: FormGroup;

    constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

    loading = false;
    ngOnInit() {
        this.diteSleepPhysicalActivityForm = this.FormBuilder.group({
            DietFollowed: this.FormBuilder.control('', Validators.required),
            Eating: this.FormBuilder.control('', Validators.required),
            Appetite: this.FormBuilder.control('', Validators.required),
            PhysicalActivity: this.FormBuilder.control('', Validators.required),
            Snoring: this.FormBuilder.control('', Validators.required),
            IncreasedGas: this.FormBuilder.control('', Validators.required),
            SleepPattern: this.FormBuilder.control('', Validators.required)
        });

        this.dietFollowed = this.patientService.patientDietDetails.DietFollowed;
        this.eating = this.patientService.patientDietDetails.Eating;
        this.appetite = this.patientService.patientDietDetails.Appetite;
        this.physicalActivity = this.patientService.patientDietDetails.PhysicalActivity;
        this.snoring = this.patientService.patientDietDetails.Snoring;
        this.increasedGas = this.patientService.patientDietDetails.IncreasedGas;
        let previousSleepPatterns = JSON.parse(this.patientService.patientDietDetails.SleepPattern);
        this.selectedSleepPatterns = this.SleepPatternObj.filter(asp => previousSleepPatterns.map(psp => psp['value']).indexOf(asp['value']) !== -1);
    }

    submitDiteSleepPhysicalActivityForm(form) {
        this.loading = true;
        this.patientService.patientDietDetails.SleepPattern = form.value.SleepPattern ? JSON.stringify(form.value.SleepPattern) : JSON.stringify(this.selectedSleepPatterns);
        let apiData = this.patientService.patientDietDetails;
        delete apiData.DietSleepId;
        this.patientService.updatePatientDietDetails(apiData).subscribe(response => {
            this.loading = false;
            this.alertsService.showToast(true);
        })
    }

}