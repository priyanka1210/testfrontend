import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-past-history',
  templateUrl: './past-history.component.html',
  styleUrls: ['./past-history.component.css']
})
export class PastHistoryComponent implements OnInit {
  PreviousAllergySymptomsObj: Object[] = [
    { value: 1, name: 'Nasal Allergy' },
    { value: 2, name: 'Asthma' },
    { value: 3, name: 'Allergy Dry Cough' },
    { value: 4, name: 'Allergy Conjuctivitis' },
    { value: 5, name: 'Allergy Pharyngitis' },
    { value: 6, name: 'Allergy Laryngitis' },
    { value: 7, name: 'Hives' },
    { value: 8, name: 'Dermatitis' },
    { value: 9, name: 'Eczema' },
    { value: 10, name: 'Migraine' }
  ];
  public selectedPreviousAllergySymptoms: Object[];
  // selectedPreviousAllergySymptoms = false;
  MajorDiseases: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private datePipe: DatePipe, private alertsService: AlertsService) { }
  pastHistoryForm: FormGroup;
  loading = false;
  ngOnInit() {
    this.pastHistoryForm = this.FormBuilder.group({
      MajorDiseases: this.FormBuilder.control('', Validators.required),
      PreviousAllergySymptoms: this.FormBuilder.control('', Validators.required),
    })

    this.MajorDiseases = this.patientService.patientPastPresentHistoryDetails.MajorDiseases;
    let previousAllergySymptoms = JSON.parse(this.patientService.patientPastPresentHistoryDetails['PreviousAllergy']);
    this.selectedPreviousAllergySymptoms = this.PreviousAllergySymptomsObj.filter(asp => previousAllergySymptoms.map(psp => psp['value']).indexOf(asp['value']) !== -1);

  }

  submitPastHistoryForm(form) {
    // console.log("form==========", form);
    this.loading = true;
    this.patientService.patientPastPresentHistoryDetails['MajorDiseases'] = form.value['MajorDiseases'];
    this.patientService.patientPastPresentHistoryDetails['PreviousAllergy'] = form.value['PreviousAllergySymptoms'] ? JSON.stringify(form.value.PreviousAllergySymptoms) : JSON.stringify(this.selectedPreviousAllergySymptoms);
    this.patientService.patientPastPresentHistoryDetails['IsActive'] = true;
    let apiData = this.patientService.patientPastPresentHistoryDetails;
    // console.log("api data===", apiData);
    delete apiData.PastHistoryId;

    this.patientService.updatePatientPastPresentHistoryDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
