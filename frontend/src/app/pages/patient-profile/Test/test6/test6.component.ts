import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test6',
  templateUrl: './test6.component.html',
  styleUrls: ['./test6.component.css']
})
export class Test6Component implements OnInit {
  MRITestOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  MRITestForm: FormGroup;
  mriTest: boolean = false;
  loading = false;
  MRITestDetails: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.MRITestForm = this.FormBuilder.group({
      MRITest: this.FormBuilder.control('', Validators.required),
      MRITestDetails: this.FormBuilder.control('', Validators.required)
    });

    this.mriTest = this.patientService.patientTestsDetails['MRITest'];
    this.MRITestDetails = this.patientService.patientTestsDetails['MRITestDetails'];
  }

  submitMRITestForm(form) {
    this.patientService.patientTestsDetails['MRITest'] = form.value['MRITest'];
    this.patientService.patientTestsDetails['MRITestDetails'] = form.value['MRITestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }


}
