import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFExposure } from './../../../../shared/AllergyFactors/afexposure';
import { Chest } from '../../../../shared/chest';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-respiratory-system1',
  templateUrl: './respiratory-system1.component.html',
  styleUrls: ['./respiratory-system1.component.css']
})
export class RespiratorySystem1Component implements OnInit {


  options = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedCoughOption = false;

  coughContinuousOptions = [
    { value: true, name: 'Continuous' },
    { value: false, name: 'Sometimes' },
  ];
  //selectedCoughContinuous = "Continuous";

  coughTypeOptions = [
    { value: 'Dry', name: 'Dry' },
    { value: 'with Phlegm', name: 'with Phlegm' },
  ];
  //selectedCoughType = "Dry";

  phlegmTypeptions = [
    { value: 'U', name: 'U' },
    { value: 'A', name: 'A' },
    { value: 'A.E or D', name: 'A.E or D' }
  ];
  // selectedPlegmType = "U";

  respiratorySystem1Form: FormGroup;
  loading = false;
  coughContinous: boolean;
  coughType: string;
  phelgmType: string;
  phlegmColor: string;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.respiratorySystem1Form = this.formBuilder.group({

      Cough: this.formBuilder.control('', Validators.required),
      CoughContinous: this.formBuilder.control('', Validators.required),
      CoughType: this.formBuilder.control('', Validators.required),
      PhelgmType: this.formBuilder.control('', Validators.required),
      PhlegmColor: this.formBuilder.control('', Validators.required),
    });

    this.selectedCoughOption = this.patientService.patientRespiratoryDetails['Cough'];
    this.coughContinous = this.patientService.patientRespiratoryDetails['CoughContinous'];
    this.coughType = this.patientService.patientRespiratoryDetails['CoughType'];
    this.phelgmType = this.patientService.patientRespiratoryDetails['PhelgmType'];
    this.phlegmColor = this.patientService.patientRespiratoryDetails['PhlegmColor'];
  }


  submitRespiratoySystem1Form() {
    this.loading = true;

    let patientChestDetails = this.patientService.patientRespiratoryDetails;


    let respiratorySystem1 = new Chest();

    if (this.selectedCoughOption == true) {
      respiratorySystem1.Cough = this.respiratorySystem1Form.get('Cough').value;
      respiratorySystem1.CoughContinous = this.respiratorySystem1Form.get('CoughContinous').value;
      respiratorySystem1.CoughType = this.respiratorySystem1Form.get('CoughType').value;
      respiratorySystem1.PhelgmType = this.respiratorySystem1Form.get('PhelgmType').value;
      respiratorySystem1.PhlegmColor = this.respiratorySystem1Form.get('PhlegmColor').value;


      var respiratoryDetails = Object.assign(patientChestDetails, respiratorySystem1)
      delete respiratoryDetails["CreatedDateTime"];
      delete respiratoryDetails["RespiratoryId"];
      delete respiratoryDetails["CreatedByUserId"];


      this.patientService.createChestRespiratorySystem(respiratoryDetails).subscribe(response => {
        console.log('Patient Respiratory System 1 Form response', response);
        this.loading = false;
        //this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"];
        this.patientService.patientRespiratoryDetails = { ...patientChestDetails, ...response };
        console.log('Patient Respiratory System 1 Form response', this.patientService.patientRespiratoryDetails);
        this.alertsService.showToast(true);

      },
        error => {
          //alert(error);
        });
    }

  }



}
