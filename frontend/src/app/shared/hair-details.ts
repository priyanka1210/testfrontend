export class HairDetails {
    /*[Key]*/
    public HairHeadacheId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public HairType: string;
    public Dandruff: boolean;
    public HairLoss: boolean;
    public HeadHeaviness: boolean;
    public Headache: boolean;
    public HeadacheArea: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
}
