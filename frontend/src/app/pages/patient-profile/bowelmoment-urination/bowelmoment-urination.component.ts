import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-bowelmoment-urination',
  templateUrl: './bowelmoment-urination.component.html',
  styleUrls: ['./bowelmoment-urination.component.css']
})
export class BowelmomentUrinationComponent implements OnInit {
  bowelmomentUrinationForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  public bowelHabitsObj: Object[] = [
    { value: 1, name: 'Regular' },
    { value: 2, name: 'Acidic stools' },
    { value: 3, name: 'Commonly Constipated' },
    { value: 4, name: 'Sometimes Constipated' },
    { value: 5, name: 'Freq-Loose Motions' },
    { value: 6, name: 'Infreq- Loose Motions' }
  ];

  public urinationObj: Object[] = [
    { value: 1, name: 'Normal' },
    { value: 2, name: 'Freequent' },
    { value: 3, name: 'Burning' },
    { value: 4, name: 'Hesitancy' },
    { value: 5, name: 'Retention' },
    { value: 6, name: 'Loss of Bladder Control' }
  ];

  public selectedBowelHabits: Object[];
  public selectedUrination: Object[];

  ngOnInit() {
    this.bowelmomentUrinationForm = this.formBuilder.group({
      BowelHabits: this.formBuilder.control('', Validators.required),
      Urination: this.formBuilder.control('', Validators.required)
    });

    let previousBowelHabits = JSON.parse(this.patientService.patientBowelDetails['BowelHabits']);
    this.selectedBowelHabits = this.bowelHabitsObj.filter(asp => previousBowelHabits.map(pbh => pbh['value']).indexOf(asp['value']) !== -1);

    let previousUrination = JSON.parse(this.patientService.patientBowelDetails['Urination']);
    this.selectedUrination = this.urinationObj.filter(asp => previousUrination.map(pu => pu['value']).indexOf(asp['value']) !== -1);
  }

  submitBowelmomentUrinationForm(form) {
    this.loading = true;
    console.log("form===", form);
    this.patientService.patientBowelDetails['BowelHabits'] = form.value.BowelHabits ? JSON.stringify(form.value.BowelHabits) : JSON.stringify(this.selectedBowelHabits);
    this.patientService.patientBowelDetails['Urination'] = form.value.Urination ? JSON.stringify(form.value.Urination) : JSON.stringify(this.selectedUrination);
    let apiData = this.patientService.patientBowelDetails;
    delete apiData.BowelId;
    console.log("api data bowel======", apiData);
    this.patientService.updatePatientBowelDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
