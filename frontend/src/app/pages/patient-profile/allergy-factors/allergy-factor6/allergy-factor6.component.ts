import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFBlanket } from './../../../../shared/AllergyFactors/afblanket';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-allergy-factor6',
  templateUrl: './allergy-factor6.component.html',
  styleUrls: ['./allergy-factor6.component.css']
})
export class AllergyFactor6Component implements OnInit {

  blankets = [{blanket : 'Woolen'}, {blanket : 'Cotton'}, {blanket : 'Silk'}, {blanket : 'Synthetic'}, {blanket : 'Polyster'}];
  allergyFactor6Form: FormGroup;
 // AFBlanket:AFBlanket[]=[];
  selectedBlankets : AFBlanket[]=[];
  loading = false;

  public WoollenCheckBox = true;
  public CottonCheckBox:boolean = false;
  public SilkCheckBox:boolean = false;
  public SyntheticCheckBox:boolean = false;
  public PolysterCheckBox:boolean = false;
 

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor6Form = this.formBuilder.group({
     Blankets : this.formBuilder.control('', Validators.required),
    });
  }

  submitAllergyFactor6Form() {
    this.loading = true;

    if (this.WoollenCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Woollen";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = 55;
      afBlankets.AllergyStatus = this.WoollenCheckBox
      this.selectedBlankets.push(afBlankets);
    }
    if (this.CottonCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Cotton";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = 56;
      afBlankets.AllergyStatus = this.CottonCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.SilkCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Silk";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = 57;
      afBlankets.AllergyStatus = this.SilkCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.SyntheticCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Synthetic";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = 58;
      afBlankets.AllergyStatus = this.SyntheticCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.PolysterCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Polyster";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = 59;
      afBlankets.AllergyStatus = this.PolysterCheckBox;
      this.selectedBlankets.push(afBlankets);
    }

    var allergyFactor6 = {};
    allergyFactor6["AFBlanket"] = this.selectedBlankets
    console.log(JSON.stringify(allergyFactor6))

    let allergyFactor5 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor5, allergyFactor6);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response);  
      this.loading = false;
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);

    },
    error => {
      //alert(error);
    });

  }

}
