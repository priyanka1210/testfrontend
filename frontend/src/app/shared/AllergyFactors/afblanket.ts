export class AFBlanket {
    /*[Key]*/
    public AfBlanketId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[Required]*/
    public MasterAllergyDataId: number;
    /*[Required]*/
    public AllergyItemDescription: string;
    /*[Required]*/
    public AllergyStatus: boolean;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
}
