import { Component, OnInit } from '@angular/core';
import { PatientService } from './../../../shared/patientService.service';
import { stringify } from '@angular/core/src/render3/util';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';
import { PatientDetails } from './../../../shared/patient-details';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-all-patients',
  templateUrl: './all-patients.component.html',
  styleUrls: ['./all-patients.component.css']
})
export class AllPatientsComponent implements OnInit {

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private router: Router) { }
  loading = false;
  patient_list: any = [];
  // { "firstName": "3e32q", "lastName": "rtwt", "emailId": null, "city": null, "mobileNo": 9475932366, "patientDemographicId": 316, "total": 0, "dob": "0001-01-01T00:00:00", "age": 1 }
  ngOnInit() {
    /*
    this.userService.getUsers()
      .subscribe( data => {
        this.users = data;
      });
    */


    this.patientService.getAllPatientDetails().subscribe(response => {
      console.log('Patient List details', response);
      this.patient_list = response;
      //  this.router.navigate(['/all-patients])
    },
      error => {
        //alert(error);
      });
  }
  UpdateList(mobile_number) {
    console.log("Inside all patient update");
    console.log("mobile_number=======", mobile_number);
    // this.router.navigate(['/pages/patient-profile/update-patient']);
    // this.router.navigate["/#/pages/patient-profile/update-patien"];
    this.patientService.searchPatientDetails(mobile_number).subscribe(response => {
      console.log('Patient Details after search', response);
      this.loading = false;
      this.patientService.patientDemographicsDetails = response["PatientDemographics"];
      this.patientService.patientPhysicalDetails = response["PatientPhysicalDetails"];
      this.patientService.patientAllergyDetails = response["AllergyFactors"];
      this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"]["AllergyHistory"];
      this.patientService.patientRespiratoryDetails = response["ChestDetails"] ? response["ChestDetails"] : this.patientService.patientRespiratoryDetails;
      this.patientService.patientHabitDetails = response["PatientHabits"];
      this.patientService.patientBloodDetails = response["BloodDetails"];
      this.patientService.patientNativityDetails = response["Nativity"];
      this.patientService.patientDietDetails = response["DietDetails"];
      this.patientService.patientGastroIntestinalDetails = response["GastroIntestinal"];
      this.patientService.patientFemaleInfoDetails = response["FemaleInfoDetails"];
      this.patientService.patientENTDetails = response["ENTDetails"];
      this.patientService.patientTestsDetails = response["PatientTests"] ? response["PatientTests"] : this.patientService.patientTestsDetails;
      this.patientService.patientSkinDetails = response['SkinDetails'] ? response['SkinDetails'] : this.patientService.patientSkinDetails;
      this.patientService.patientBowelDetails = response['BowelDetails'] ? response["BowelDetails"] : this.patientService.patientBowelDetails;
      this.patientService.patientHairDetails = response['HairHeadacheDetails'] ? response['HairHeadacheDetails'] : this.patientService.patientHairDetails;
      this.patientService.patientPastPresentHistoryDetails = response['PastHistoryDetails'] ? response['PastHistoryDetails'] : this.patientService.patientPastPresentHistoryDetails;
      this.patientService.patientNonAllergicGerdDetails = response['PresentNonAllergics'] ? response['PresentNonAllergics'] : this.patientService.patientNonAllergicGerdDetails;
      this.patientService.patientFanalInitialAdviseDetails = response['PatientDiagnosticsDetails'] ? response['PatientDiagnosticsDetails'] : this.patientService.patientFanalInitialAdviseDetails;
      this.router.navigate(['/pages/patient-profile/update-patient']);
    },
      error => {
        console.log('error occured', error);
        console.log("Error Value Displayed", error["error"]["value"]);
        this.loading = false;
        alert(error["error"]["value"]);
        //this.alertsService.showError(error);

        //alert(error["error"]["value"]);
      });


  }

}
