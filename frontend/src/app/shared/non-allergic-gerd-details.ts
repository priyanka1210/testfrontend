export class NonAllergicGerdDetails {
    // screen-1
    GERD: boolean;
    GERDHowLong: string;
    GERDMedication: string;
    GERDUnderControl: boolean;
    // GERD: true
    // GERDHowLong: "2 YEARS"
    // GERDMedication: "Yes"
    // GERDUnderControl: true
    //screen-2
    Diabetic: boolean;
    DiabeticHowLong: string;
    DiabeticMedication: string;
    DiabeticUnderControl: boolean;
    Hypertensive: boolean;
    HypertensiveHowLong: string;
    HypertensiveMedication: string;
    HypertensiveUnderControl: boolean;

    CreatedByUserId: number;
    CreatedDateTime: Date;
    IsActive: boolean;
    PatientDemographicsId: number;
    PatientDemographics: string;
    PresentNonAllergicId: number;
}

// CreatedByUserId: 1234
// CreatedDateTime: "2017-01-01T00:00:00"
// Diabetic: true
// DiabeticHowLong: "2 YEAR"
// DiabeticMedication: "YES"
// DiabeticUnderControl: false
// GERD: true
// GERDHowLong: "2 YEARS"
// GERDMedication: "Yes"
// GERDUnderControl: true
// Hypertensive: true
// HypertensiveHowLong: "1 YEAR"
// HypertensiveMedication: "No"
// HypertensiveUnderControl: true
// IsActive: true
// PatientDemographics: null
// PatientDemographicsId: 314
// PresentNonAllergicId: 1

// ,[Diabetic]
// ,[DiabeticHowLong]
// ,[DiabeticMedication]
// ,[DiabeticUnderControl]
// ,[Hypertensive]
// ,[HypertensiveHowLong]
// ,[HypertensiveMedication]
// ,[HypertensiveUnderControl]
// ,[GERD]
// ,[GERDHowLong]
// ,[GERDMedication]
// ,[GERDUnderControl]
// ,[CreatedByUserId]
// ,[CreatedDateTime]
// ,[IsActive]
// ,[PatientDemographicsId]
// FROM [dbo].[PresentNonAllergics]