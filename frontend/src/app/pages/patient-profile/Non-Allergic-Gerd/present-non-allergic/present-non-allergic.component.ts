import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-present-non-allergic',
  templateUrl: './present-non-allergic.component.html',
  styleUrls: ['./present-non-allergic.component.css']
})
export class PresentNonAllergicComponent implements OnInit {
  diabeticOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedDiabetic = false;
  controlOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedControl = false;
  hypertensiveOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedHypertensive = false;

  hypertensiveControlOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedHypertensiveControl = false;
  presentNonAllergicForm: FormGroup;
  DiabeticHowLong: string;
  DiabeticMedication: string;
  DiabeticUnderControl: boolean;
  Hypertensive: boolean;
  HypertensiveHowLong: string;
  HypertensiveMedication: string;
  HypertensiveControl: boolean;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.presentNonAllergicForm = this.formBuilder.group({
      Diabetic: this.formBuilder.control('', Validators.required),
      // Control: this.formBuilder.control('', Validators.required),
      DiabeticHowLong: this.formBuilder.control('', Validators.required),
      DiabeticMedication: this.formBuilder.control('', Validators.required),
      DiabeticUnderControl: this.formBuilder.control('', Validators.required),
      Hypertensive: this.formBuilder.control('', Validators.required),
      HypertensiveHowLong: this.formBuilder.control('', Validators.required),
      HypertensiveMedication: this.formBuilder.control('', Validators.required),
      HypertensiveControl: this.formBuilder.control('', Validators.required),
    })

    // console.log(" this.patientService.patientNonAllergicGerdDetails=====", this.patientService.patientNonAllergicGerdDetails);
    this.selectedDiabetic = this.patientService.patientNonAllergicGerdDetails.Diabetic;
    this.DiabeticHowLong = this.patientService.patientNonAllergicGerdDetails.DiabeticHowLong;
    this.DiabeticMedication = this.patientService.patientNonAllergicGerdDetails.DiabeticMedication;
    this.selectedControl = this.patientService.patientNonAllergicGerdDetails.DiabeticUnderControl;
    this.selectedHypertensive = this.patientService.patientNonAllergicGerdDetails.Hypertensive;
    this.HypertensiveHowLong = this.patientService.patientNonAllergicGerdDetails.HypertensiveHowLong;
    this.HypertensiveMedication = this.patientService.patientNonAllergicGerdDetails.HypertensiveMedication;
    this.selectedHypertensiveControl = this.patientService.patientNonAllergicGerdDetails.HypertensiveUnderControl;
    console.log("selectedHypertensiveControl=====", this.patientService.patientNonAllergicGerdDetails.HypertensiveUnderControl);
  }

  submitPresentNonAllergic(form) {
    console.log("form.value['HypertensiveUnderControl']==", form.value['HypertensiveUnderControl']);
    this.loading = true;
    this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientNonAllergicGerdDetails['DiabeticHowLong'] = form.value['DiabeticHowLong'];
    this.patientService.patientNonAllergicGerdDetails['DiabeticMedication'] = form.value['DiabeticMedication'];
    this.patientService.patientNonAllergicGerdDetails['DiabeticUnderControl'] = form.value['DiabeticUnderControl'];
    this.patientService.patientNonAllergicGerdDetails['Hypertensive'] = form.value['Hypertensive'];
    this.patientService.patientNonAllergicGerdDetails['HypertensiveHowLong'] = form.value['HypertensiveHowLong'];
    this.patientService.patientNonAllergicGerdDetails['HypertensiveMedication'] = form.value['HypertensiveMedication'];
    this.patientService.patientNonAllergicGerdDetails['HypertensiveUnderControl'] = form.value['HypertensiveControl'];
    this.patientService.patientNonAllergicGerdDetails['IsActive'] = true;

    let apiData = this.patientService.patientNonAllergicGerdDetails;
    delete apiData.PresentNonAllergicId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientNonAllergicGerdDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });

  }
}
