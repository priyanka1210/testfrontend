import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-skin1',
  templateUrl: './skin1.component.html',
  styleUrls: ['./skin1.component.css']
})
export class Skin1Component implements OnInit {

  SkinSymptomsAreaObj: Object[] = [
    { value: 1, name: 'Urticaria' },
    { value: 2, name: 'Dermatitis' },
    { value: 3, name: 'Eczema' },
    { value: 4, name: 'Angio Oedema' },
  ]
  itchingOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  dryordischargeOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  rednessOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedItching = false;
  skin1Form: FormGroup;
  loading = false;
  skinItchingArea: string;
  skinAppearandDisAppear: string;
  skinSymptomsArea: string;
  selectedDryordischarge: boolean;
  selectedRedness: boolean;
  skinSymptomsMaster: string;
  selectedSkinSymptomsArea: Object[];
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }


  ngOnInit() {
    this.skin1Form = this.FormBuilder.group({
      Itching: this.FormBuilder.control('', Validators.required),
      SkinItchingArea: this.FormBuilder.control('', Validators.required),
      SkinSymptomsArea: this.FormBuilder.control('', Validators.required),
      SkinAppearandDisAppear: this.FormBuilder.control('', Validators.required),
      Dryordischarge: this.FormBuilder.control('', Validators.required),
      Redness: this.FormBuilder.control('', Validators.required),
      SkinSymptomsMaster: this.FormBuilder.control('', Validators.required),
    });
    // console.log("this.patientService.patientSkinDetails=====", this.patientService.patientSkinDetails);
    this.selectedItching = this.patientService.patientSkinDetails.SkinItching;
    this.skinItchingArea = this.patientService.patientSkinDetails.SkinItchingArea;
    // this.skinSymptomsArea = this.patientService.patientSkinDetails.SkinSymptomsArea;
    this.skinAppearandDisAppear = this.patientService.patientSkinDetails.SkinItchingAppearArea;
    this.selectedDryordischarge = this.patientService.patientSkinDetails.SkinDryness;
    this.selectedRedness = this.patientService.patientSkinDetails.SkinRedness;
    this.skinSymptomsMaster = this.patientService.patientSkinDetails.SkinSymptomsMaster;

    let SkinSymptomsArea = JSON.parse(this.patientService.patientSkinDetails['SkinSymptomsArea']);
    this.selectedSkinSymptomsArea = this.SkinSymptomsAreaObj.filter(asp => SkinSymptomsArea.map(psp => psp['value']).indexOf(asp['value']) !== -1);


  }
  submitSkin1Form(form) {
    // console.log("form values====", form);
    this.loading = true;
    this.patientService.patientSkinDetails['SkinSymptomsArea'] = form.value['SkinSymptomsArea'] ? JSON.stringify(form.value.SkinSymptomsArea) : JSON.stringify(this.selectedSkinSymptomsArea);
    this.patientService.patientSkinDetails['SkinItching'] = form.value['Itching'];
    this.patientService.patientSkinDetails['SkinItchingArea'] = form.value['SkinItchingArea'];
    // this.patientService.patientSkinDetails['SkinSymptomsArea'] = form.value['SkinSymptomsArea'];
    this.patientService.patientSkinDetails['SkinItchingAppearArea'] = form.value['SkinAppearandDisAppear'];
    this.patientService.patientSkinDetails['SkinDryness'] = form.value['Dryordischarge'];
    this.patientService.patientSkinDetails['SkinRedness'] = form.value['Redness'];
    this.patientService.patientSkinDetails['SkinSymptomsMaster'] = form.value['SkinSymptomsMaster'];
    let apiData = this.patientService.patientSkinDetails;
    delete apiData.SkinId;

    this.patientService.updatepatientSkinDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });

  }
}

