import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';
@Component({
  selector: 'app-eye-ear-info',
  templateUrl: './eye-ear-info.component.html',
  styleUrls: ['./eye-ear-info.component.css']
})
export class EyeEarInfoComponent implements OnInit {

  giddinessOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  selectedGiddiness = false;
  eyeEarInfoForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  public eyeComplaintsObj: Object[] = [
    { value: 1, name: 'Sleep delayed' },
    { value: 2, name: 'Redness' },
    { value: 3, name: 'Itching' },
    { value: 4, name: 'Discharge / Exudate' }
  ];

  public earComplaintsObj: Object[] = [
    { value: 1, name: 'Discharge' },
    { value: 2, name: 'Pain' },
    { value: 3, name: 'Itching' },
    { value: 4, name: 'Ringing Sound' }
  ];

  public earDischargeObj: Object[] = [
    { value: 1, name: 'Watery' },
    { value: 2, name: 'Nucoid' },
    { value: 3, name: 'Pus Like' },
    { value: 4, name: 'Bloody' }
  ];

  public selectedEyeComplaints: Object[];
  public selectedEarComplaints: Object[];
  public selectedEarDischarge: Object[];

  public eyeDetails: String;
  public earDetails: String;
  public giddiness: String;

  ngOnInit() {

    this.eyeEarInfoForm = this.formBuilder.group({
      EyeComplaints: this.formBuilder.control('', Validators.required),
      EyeDetails: this.formBuilder.control('', Validators.required),
      EarComplaints: this.formBuilder.control('', Validators.required),
      EarDischarge: this.formBuilder.control('', Validators.required),
      EarDetails: this.formBuilder.control('', Validators.required),
      Giddiness: this.formBuilder.control('', Validators.required)
    });

    this.eyeDetails = this.patientService.patientENTDetails['EyeDetails'];
    this.earDetails = this.patientService.patientENTDetails['EarDetails'];
    if (this.patientService.patientENTDetails['Giddiness'] == "true") {
      this.selectedGiddiness = true;
    } else {
      this.selectedGiddiness = false;
    }

    let previousEyeComplaints = JSON.parse(this.patientService.patientENTDetails['EyeComplaints']);
    this.selectedEyeComplaints = this.eyeComplaintsObj.filter(asp => previousEyeComplaints.map(psp => psp['value']).indexOf(asp['value']) !== -1);

    let previousEarComplaints = JSON.parse(this.patientService.patientENTDetails['EarComplaints']);
    this.selectedEarComplaints = this.earComplaintsObj.filter(asp => previousEarComplaints.map(psp => psp['value']).indexOf(asp['value']) !== -1);

    let previousEarDischarge = JSON.parse(this.patientService.patientENTDetails['EarDischarge']);
    this.selectedEarDischarge = this.earDischargeObj.filter(asp => previousEarDischarge.map(psp => psp['value']).indexOf(asp['value']) !== -1);
  }

  submiteyeEarInfoForm(eyeEarInfoForm) {
    this.loading = true;
    this.patientService.patientENTDetails['EyeComplaints'] = eyeEarInfoForm.value.EyeComplaints ? JSON.stringify(eyeEarInfoForm.value.EyeComplaints) : JSON.stringify(this.selectedEyeComplaints);
    this.patientService.patientENTDetails['EyeDetails'] = eyeEarInfoForm.value['EyeDetails'];
    this.patientService.patientENTDetails['EarComplaints'] = eyeEarInfoForm.value.EarComplaints ? JSON.stringify(eyeEarInfoForm.value.EarComplaints) : JSON.stringify(this.selectedEarComplaints);
    this.patientService.patientENTDetails['EarDischarge'] = eyeEarInfoForm.value.EarDischarge ? JSON.stringify(eyeEarInfoForm.value.EarDischarge) : JSON.stringify(this.selectedEarDischarge);
    this.patientService.patientENTDetails['EarDetails'] = eyeEarInfoForm.value['EarDetails'];
    this.patientService.patientENTDetails['Giddiness'] = eyeEarInfoForm.value['Giddiness'];

    let patientpatientENTDetailsObj = Object.assign({}, this.patientService.patientENTDetails);
    patientpatientENTDetailsObj.IsActive = true;
    delete patientpatientENTDetailsObj.ENTId;

    this.patientService.updatepatientENTDetails(patientpatientENTDetailsObj).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });

  }

}
