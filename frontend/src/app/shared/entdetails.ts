export class ENTDetails {
    /*[Key]*/
    public ENTId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public EarDetails: string;
    public EyeDetails: string;
    public EyeComplaints: string;
    public EarComplaints: string;
    public EarDischarge: string;
    public Giddiness: string;
    public NoseComplaint: string;
    public NoseTroubling: string;
    public AssociatedItching: boolean;
    public ItchingPlace: string;
    public ThroatComplaint: string;
    public ThroatChoking: boolean;
    public VoiceBoxComplaints: string;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    IsActive: boolean;
    PatientDemographics: String;
}
