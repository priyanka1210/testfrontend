import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-headache-info',
  templateUrl: './headache-info.component.html',
  styleUrls: ['./headache-info.component.css']
})
export class HeadacheInfoComponent implements OnInit {
  heavinessOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedHeaviness = false;
  headacheOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedHeadache = false;
  heachacheArea = '';
  headacheDetailsForm: FormGroup;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.headacheDetailsForm = this.formBuilder.group({
      HeadHeaviness: this.formBuilder.control('', Validators.required),
      Headache: this.formBuilder.control('', Validators.required),
      HeadacheArea: this.formBuilder.control('', Validators.required)
    });

    this.selectedHeaviness = this.patientService.patientHairDetails.HeadHeaviness;
    this.selectedHeadache = this.patientService.patientHairDetails.Headache;
    this.heachacheArea = this.patientService.patientHairDetails.HeadacheArea;
  }
  submitHeadacheDetailsForm(form) {
    this.loading = true;
    this.patientService.patientHairDetails.HeadHeaviness = form.value.HeadHeaviness;
    this.patientService.patientHairDetails.Headache = form.value.Headache;
    this.patientService.patientHairDetails.HeadacheArea = form.value.HeadacheArea;

    let apiData = this.patientService.patientHairDetails;
    delete apiData.HairHeadacheId;
    this.patientService.updatePatientHairDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    })
  }
}
