import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFHouse } from './../../../../shared/AllergyFactors/afhouse';
import { AFNeighbourhood } from './../../../../shared/AllergyFactors/afneighbourhood';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AlertsService } from './../../../../shared/alerts.service';



@Component({
  selector: 'app-allergy-factor3',
  templateUrl: './allergy-factor3.component.html',
  styleUrls: ['./allergy-factor3.component.css']
})
export class AllergyFactor3Component implements OnInit {

  items = [{'value':'Clean', 'name':'Clean'}, {'value':'Construction going on', 'name': 'Construction going on'}, {'value':'Dumping of solid waste', 'name': 'Dumping of solid waste'},{'value':'Weeds and Grasses', 'name': 'Weeds and Grasses'},{'value':'Green and clean with health-friendly trees', 'name': 'Green and clean with health-friendly trees'},{'value':'Riverbed', 'name': 'Riverbed'}, {'value':'Seashore', 'name': 'Seashore'}];
  houses = [{house : 'Woolen'}, {house : 'Woolen'}, {house : 'Woolen'}, {house : 'Woolen'}, {house : 'Woolen'}, {house : 'Woolen'}];
  
  allergyFactor3Form: FormGroup;
  selectedNeighbourhood = "";

  selectedHouses : AFHouse[]=[];
  selectedNeghbourhoods : string[] = [];
  selectedNeighbourhoodAllergy :  AFNeighbourhood[] = [];
  loading:boolean = false;

  public VeryOldCheckBox:boolean = false;
  public ModeratelyOldCheckBox:boolean = false;
  public NewCheckBox:boolean = false;
  public WellVentilatedCheckBox:boolean = false;
  public EnoughLightCheckBox:boolean = false;
  public CleanRoofCheckBox:boolean = false;
  public DampWallsCheckBox:boolean = false;
  public DarkInteriorsCheckBox:boolean = false;
  public ApartmentCheckBox:boolean = false;
  public IndependentCheckBox:boolean = false;
  public ModeratelyNewCheckBox: boolean = false;
  public TwinCheckBox: boolean = false;


  
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor3Form = this.formBuilder.group({
     
    House_VeryOld :this.formBuilder.control('', Validators.required),
    House_ModeratelyOld :this.formBuilder.control('', Validators.required),
    House_New:this.formBuilder.control('', Validators.required),
    House_WellVentilated:this.formBuilder.control('', Validators.required),
    House_EnoughLight:this.formBuilder.control('', Validators.required),
    House_CleanRoof:this.formBuilder.control('', Validators.required),
    House_DampWalls:this.formBuilder.control('', Validators.required),
    House_DarkInteriors:this.formBuilder.control('', Validators.required),
    House_Apartment :this.formBuilder.control('', Validators.required),
    House_Independent :this.formBuilder.control('', Validators.required),
    House_ModeratelyNew:this.formBuilder.control('', Validators.required),
    House_Twin :this.formBuilder.control('', Validators.required),                 
     NeighbourhoodSelect : this.formBuilder.control('', Validators.required),
    

    });

    for (let house of this.patientService.patientAllergyDetails.AFHouse) {
      switch(house.AllergyItemDescription) {
        
          case "Very Old":
              this.VeryOldCheckBox=true;
              break;
          case "Moderately Old":
              this.ModeratelyOldCheckBox=true;
              break;
          case "New":
              this.NewCheckBox=true;
              break;
          case "Well Ventilated":
              this.WellVentilatedCheckBox=true;
              break;
          case "Enough Light":
              this.EnoughLightCheckBox=true;
              break;
          case "Clean Roof":
              this.CleanRoofCheckBox=true;
              break;
          case "Damp Walls":
              this.DampWallsCheckBox=true;
              break;
          case "Dark Interiors":
              this.DarkInteriorsCheckBox=true;
              break;
          case "Apartment":
              this.ApartmentCheckBox=true;
              break;
          case "Independent":
              this.IndependentCheckBox=true;
              break;
          case "Moderately New":
              this.ModeratelyNewCheckBox=true;
              break;
          case "Twin":
              this.TwinCheckBox=true;
              break;

      }
    }
    // selectedNeghbourhoods
  }

  submitAllergyFactor3Form() {
    this.loading = true;
    if (this.VeryOldCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Very Old";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Very Old");//35;
      afHouse.AllergyStatus = this.VeryOldCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.ModeratelyOldCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Moderately Old";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately Old")//36;
      afHouse.AllergyStatus = this.ModeratelyOldCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.NewCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "New";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("New")//37;
      afHouse.AllergyStatus = this.NewCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.WellVentilatedCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Well Ventilated";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Well Ventilated");//38;
      afHouse.AllergyStatus = this.WellVentilatedCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.EnoughLightCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Enough Light";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Enough Light");//40;
      afHouse.AllergyStatus = this.EnoughLightCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.CleanRoofCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Clean Roof";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Clean Roof");//41;
      afHouse.AllergyStatus = this.CleanRoofCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.DampWallsCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Damp Walls";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Damp Walls");//42;
      afHouse.AllergyStatus = this.DampWallsCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.DarkInteriorsCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Dark interiors";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Dark interiors");//43;
      afHouse.AllergyStatus = this.DarkInteriorsCheckBox;
    }
    if (this.ApartmentCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Apartment";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Apartment");//44;
      afHouse.AllergyStatus = this.ApartmentCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.IndependentCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Independent";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Independent");//45;
      afHouse.AllergyStatus = this.IndependentCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.ModeratelyNewCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Moderately New";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately New");//46;
      afHouse.AllergyStatus = this.ModeratelyNewCheckBox;
      this.selectedHouses.push(afHouse);
    }
    if (this.TwinCheckBox)
    {
      let afHouse = new AFHouse();
      afHouse.AllergyItemDescription = "Twin";
      afHouse.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afHouse.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Twin");//46;
      afHouse.AllergyStatus = this.TwinCheckBox;
      this.selectedHouses.push(afHouse);
    }
    

    //Neighbourhood

    for (var j in this.selectedNeghbourhoods) {
   
      let afNeighbourhood = new AFNeighbourhood();
    

      // let insectMasterAllergyDataId = this.patientService.allergyDataNameToDataId.get('this.selectedInsect[i]');
      // console.log("Insect Master Allergy data id",insectMasterAllergyDataId);

      afNeighbourhood.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afNeighbourhood.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get(this.selectedNeghbourhoods[j]);//75;
      afNeighbourhood.AllergyStatus = true;
      afNeighbourhood.AllergyItemDescription = this.selectedNeghbourhoods[j];
      this.selectedNeighbourhoodAllergy.push(afNeighbourhood);
    }
   
    
    var allergyFactor3 = {};
    allergyFactor3["AFHouse"] = this.selectedHouses;
    allergyFactor3["AFNeighbourhood"] = this.selectedNeighbourhoodAllergy;
    console.log(JSON.stringify(allergyFactor3));
    console.log(JSON.stringify(allergyFactor3))
    //alert('before api call');

    let allergyFactor2 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor2, allergyFactor3);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response);  
      this.loading = false;
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);

    },
    error => {
      //alert(error);
    });




  }
  



}
