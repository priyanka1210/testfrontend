export class PatientDemographicsDetails {
    PatientDemographicsId:number;
    FirstName : string ;
    MiddleName : string;
    LastName : string;
    Mobile : number;
    SecondaryContactNumber : number;
    EmailID : string;
    DateofBirth? : string;
    Gender : string;
    MarriatalStatus: string;
    Address1 : string;
    Address2 : string;
    City : string;
    ZipCode  : number;
    State : string;
    ReferringDoctorName : string;
    ReferringDoctortNumber : number;
    PatientLocationId : number;
    Occupation : string
   // CreatedByUserId : number;
   // CreatedDateTime : string;
}

