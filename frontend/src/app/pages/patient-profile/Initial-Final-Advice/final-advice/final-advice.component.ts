import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-final-advice',
  templateUrl: './final-advice.component.html',
  styleUrls: ['./final-advice.component.css']
})
export class FinalAdviceComponent implements OnInit {
  Immunotheraphy: string;
  General: string;
  Medication: string;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;
  finalAdviseForm: FormGroup;
  ngOnInit() {
    this.finalAdviseForm = this.formBuilder.group({
      Immunotheraphy: this.formBuilder.control('', Validators.required),
      Medication: this.formBuilder.control('', Validators.required),

      General: this.formBuilder.control('', Validators.required),

    })
    this.Immunotheraphy = this.patientService.patientFanalInitialAdviseDetails.Immunotheraphy;
    console.log("this.InitialAdvise=====", this.Immunotheraphy);
    this.Medication = this.patientService.patientFanalInitialAdviseDetails.Medication;
    this.General = this.patientService.patientFanalInitialAdviseDetails.General;

  }

  submitFinalAdviseForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['Immunotheraphy'] = form.value['Immunotheraphy'];
    this.patientService.patientFanalInitialAdviseDetails['Medication'] = form.value['Medication'];
    this.patientService.patientFanalInitialAdviseDetails['General'] = form.value['General'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}

// PatientDiagnosticsDetailId : number;
// SpecialPointsDetails : string;
// InitialAdviceDetails : string;
// FollowupDetails : string;
// Immunotheraphy: string;
// Medication: string;
// General : string;
// PrognosisDetails : string;
// ProvisionalDiagnosisDetails: string;
// CreatedByUserId:number;
// CreatedDateTime: Date;
// IsActive : boolean;
// PatientDemographicsId : number;
// PatientDemographics : string;