import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test5',
  templateUrl: './test5.component.html',
  styleUrls: ['./test5.component.css']
})
export class Test5Component implements OnInit {
  CTscanOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  CTScanTestForm: FormGroup;
  selectedCTscan: boolean = false;
  ctScanDetails: string = '';
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;


  ngOnInit() {
    this.CTScanTestForm = this.FormBuilder.group({
      CTScan: this.FormBuilder.control('', Validators.required),
      CTScanDetails: this.FormBuilder.control('', Validators.required)
    });
    this.selectedCTscan = this.patientService.patientTestsDetails['CTScan'];
    this.ctScanDetails = this.patientService.patientTestsDetails['CTScanDetails'];
  }

  submitCTScanTestForm(form) {
    this.patientService.patientTestsDetails['CTScan'] = form.value['CTScan'];
    this.patientService.patientTestsDetails['CTScanDetails'] = form.value['CTScanDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

  
}






