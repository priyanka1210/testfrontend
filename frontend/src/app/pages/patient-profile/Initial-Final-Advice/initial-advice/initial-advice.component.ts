import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-initial-advice',
  templateUrl: './initial-advice.component.html',
  styleUrls: ['./initial-advice.component.css']
})
export class InitialAdviceComponent implements OnInit {

  initialAdviseForm: FormGroup;
  InitialAdvise: string;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.initialAdviseForm = this.formBuilder.group({
      InitialAdvise: this.formBuilder.control('', Validators.required),
    })
    console.log("this.patientService.patientFanalInitialAdviseDetails===",this.patientService.patientFanalInitialAdviseDetails);
    this.InitialAdvise = this.patientService.patientFanalInitialAdviseDetails.InitialAdviceDetails;
    console.log("this.InitialAdvise=====", this.InitialAdvise);
  }

  submitInitialAdviseForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['InitialAdviceDetails'] = form.value['InitialAdvise'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
