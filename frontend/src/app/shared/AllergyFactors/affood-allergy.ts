export class AFFoodAllergy {
     /*[Key]*/
     public AfFoodAllergyId: number;
     /*[Required]*/
     public PatientDemographicsId: number;
     /*[Required]*/
     public MasterAllergyDataId: number;
     /*[Required]*/
     public AllergyItemDescription: string;
     /*[Required]*/
     public AllergyStatus: boolean;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;

     public IsActive: boolean;
     /*[ForeignKey("PatientDemographicsId")]*/

//  constructor(FoodAlID:number,PatientID:number,MstAllID:number,ItemDisc:string){
//      this.AfFoodAllergyId = FoodAlID;
//      this.PatientDemographicsId = PatientID;
//      this.MasterAllergyDataId = MstAllID;
//      this.AllergyItemDescription = ItemDisc;
//  }

//  constructor(MasterAllergyDataId: number,AllergyStatus: boolean, AllergyItemDescription: string ){
//     this.MasterAllergyDataId = MasterAllergyDataId;
//     this.AllergyStatus = AllergyStatus;
//     this.AllergyItemDescription = AllergyItemDescription;
//     //this.IsActive = IsActive;
//  }

}
