import { Component, OnInit } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { PatientDemographicsDetails } from './../../../../shared/patient-demographics-details';
import { Chest } from '../../../../shared/chest';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-respiratory-system3',
  templateUrl: './respiratory-system3.component.html',
  styleUrls: ['./respiratory-system3.component.css']
})
export class RespiratorySystem3Component implements OnInit {

  options = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedOption1 = false;
  selectedOption2 = false;
  selectedOption3 = false;

  inspiratoryoptions = [
    { value: 'Inspiratory', name: 'Inspiratory' },
    { value: 'Expiratory', name: 'Expiratory' },
    { value: 'Both', name: 'Both' },
  ];
  selectedinspiratory = '';

  musicaloptions = [
    { value: 'Musical', name: 'Musical' },
    { value: 'Rough', name: 'Rough' },
    { value: 'Both', name: 'Both' },
  ];
  selectedmusical = '';


  respiratorySystem3Form: FormGroup;
  selectedChestShape;
  loading = false;
  breathlessnessTime: string;

  constructor(public patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.respiratorySystem3Form = this.formBuilder.group({

      ChestShape: this.formBuilder.control('', Validators.required),
      ChestCrepitations: this.formBuilder.control('', Validators.required),
      ChestRales: this.formBuilder.control('', Validators.required),
      ChestRhonchi: this.formBuilder.control('', Validators.required),
      ChestRhonchiType: this.formBuilder.control('', Validators.required),
      WheezingTone: this.formBuilder.control('', Validators.required),


    });

    this.selectedOption1 = this.patientService.patientRespiratoryDetails['ChestCrepitations'];
    this.selectedOption2 = this.patientService.patientRespiratoryDetails['ChestRales'];
    this.selectedOption3 = this.patientService.patientRespiratoryDetails['ChestRhonchi'];
    this.selectedinspiratory = this.patientService.patientRespiratoryDetails['ChestRhonchiType'];
    this.selectedmusical = this.patientService.patientRespiratoryDetails['WheezingTone'];
    this.selectedChestShape = this.patientService.patientRespiratoryDetails['ChestShape'];
    this.breathlessnessTime = this.patientService.patientRespiratoryDetails['BreathlessnessTime'];
  }

  submitRespiratoySystem3Form(form) {
    console.log("form===", form);
    this.loading = true;

    let patientChestDetails = this.patientService.patientRespiratoryDetails;

    let respiratorySystem3 = new Chest();

    respiratorySystem3.ChestShape = this.respiratorySystem3Form.get('ChestShape').value;
    respiratorySystem3.ChestCrepitations = this.respiratorySystem3Form.get('ChestCrepitations').value;
    respiratorySystem3.ChestRales = this.respiratorySystem3Form.get('ChestRales').value;
    respiratorySystem3.ChestRhonchi = this.respiratorySystem3Form.get('ChestRhonchi').value;
    respiratorySystem3.ChestRhonchiType = this.respiratorySystem3Form.get('ChestRhonchiType').value;
    respiratorySystem3.WheezingTone = this.respiratorySystem3Form.get('WheezingTone').value;

    var respiratoryDetails = Object.assign(patientChestDetails, respiratorySystem3);
    delete respiratoryDetails["CreatedDateTime"];
    delete respiratoryDetails["RespiratoryId"];
    delete respiratoryDetails["CreatedByUserId"];

    this.patientService.createChestRespiratorySystem(respiratoryDetails).subscribe(response => {
      console.log('Patient Respiratory System 3 Form response', response);
      this.loading = false;
      //this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"];
      this.patientService.patientRespiratoryDetails = { ...patientChestDetails, ...response };
      console.log('Patient Respiratory System 3 Form response', this.patientService.patientRespiratoryDetails);
      this.alertsService.showToast(true);
    },
      error => {
        //alert(error);
      });
  }

}

