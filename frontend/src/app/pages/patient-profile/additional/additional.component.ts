import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';

@Component({
  selector: 'app-additional',
  templateUrl: './additional.component.html',
  styleUrls: ['./additional.component.css']
})
export class AdditionalComponent implements OnInit {

  additionalInfoForm: FormGroup;
  SpecialPointsDetails: string;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.additionalInfoForm = this.formBuilder.group({
      SpecialPointsDetails: this.formBuilder.control('', Validators.required),
    })
    this.SpecialPointsDetails = this.patientService.patientFanalInitialAdviseDetails.SpecialPointsDetails;
    console.log("this.SpecialPointsDetails", this.SpecialPointsDetails);
  }

  submitAdditionalInfoForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['SpecialPointsDetails'] = form.value['SpecialPointsDetails'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
