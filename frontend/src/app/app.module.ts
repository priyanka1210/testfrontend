/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { APP_BASE_HREF } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from './@core/core.module';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NbThemeModule, NbRadioModule } from '@nebular/theme';
import { NbToastrModule } from '@nebular/theme';
import { HttpModule } from '@angular/http';
import { MsAdalAngular6Module } from 'microsoft-adal-angular6';


@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    NbRadioModule,
    // NbThemeModule.forRoot({ name: 'default' }),
    // NgbModule.forRoot(),
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbToastrModule.forRoot(),
    MsAdalAngular6Module.forRoot({
      tenant: 'f44e469e-373c-4e5f-982c-1ee646c52c25',
      clientId: '2764513c-9a24-4b1c-bc8a-35d74db9c67a',
      redirectUri: "http://localhost:4200" || window.location.origin,
      endpoints: {
        api: '2764513c-9a24-4b1c-bc8a-35d74db9c67a',
      },
      navigateToLoginRequestUrl: false,
      cacheLocation: 'localStorage',
      postLogoutRedirectUri: 'http://localhost:4200'
    }),
  ],
  //exports: [AllergyFactorsComponent],
  bootstrap: [AppComponent],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/' },
  ],
})
export class AppModule {
}
