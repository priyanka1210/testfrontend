import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MsAdalAngular6Service } from 'microsoft-adal-angular6';
import * as jwt_decode from "jwt-decode";


@Injectable()
export class AuthService {
    constructor(
        private adalService: MsAdalAngular6Service,
        private http: HttpClient) {
        this.adalService.handleCallback();
    }

    login() {
        this.adalService.login();
    }

    getAccesstoken() {
        return this.adalService.acquireToken('2764513c-9a24-4b1c-bc8a-35d74db9c67a');
    }

    logout() {
        localStorage.removeItem('adal.auth.token.key2764513c-9a24-4b1c-bc8a-35d74db9c67a');
        this.adalService.logout();
    }

    getAuthenticationToken(accessToken) {
        return this.http.post('https://functionapp20190515071947.azurewebsites.net/.auth/login/aad',
            { "access_token": accessToken },
            { headers: { 'Content-Type': 'application/json' } });
    }

    getClinicId(accessToken) {
        try {
            let decodedToken = jwt_decode(accessToken);
            return decodedToken.groups[0];
        }
        catch (Error) {
            return null;
        }
    }

    getUserName() {
        const idToken = localStorage.getItem('adal.idtoken');
        const decodedIdToken = jwt_decode(idToken);
        return decodedIdToken.name || '';
    }

    closeErrorBanner() {
        localStorage.setItem('adal.error', '');
    }
}
