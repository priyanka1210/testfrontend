import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-provisional-details',
  templateUrl: './provisional-details.component.html',
  styleUrls: ['./provisional-details.component.css']
})
export class ProvisionalDetailsComponent implements OnInit {

  provisionalDiagnosisForm: FormGroup;
  ProvisionalDetails: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  loading = false;

  ngOnInit() {
    this.provisionalDiagnosisForm = this.FormBuilder.group({
      ProvisionalDetails: this.FormBuilder.control('', Validators.required),

    })
    this.ProvisionalDetails = this.patientService.patientFanalInitialAdviseDetails['ProvisionalDiagnosisDetails']
  }
  submitProvisionalDiagnosisForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['ProvisionalDiagnosisDetails'] = form.value['ProvisionalDetails'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });

  }


}
