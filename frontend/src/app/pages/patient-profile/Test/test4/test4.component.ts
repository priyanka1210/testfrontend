import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test4',
  templateUrl: './test4.component.html',
  styleUrls: ['./test4.component.css']
})
export class Test4Component implements OnInit {
  XRayOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  XRayTestForm: FormGroup;
  selectedXRay: boolean = false;
  xRayTestDetails: string = '';
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;


  ngOnInit() {
    this.XRayTestForm = this.FormBuilder.group({
      XRayTest: this.FormBuilder.control('', Validators.required),
      XRayTestDetails: this.FormBuilder.control('', Validators.required)
    });
    this.selectedXRay = this.patientService.patientTestsDetails['XRayTest'];
    this.xRayTestDetails = this.patientService.patientTestsDetails['XRayTestDetails'];
  }

  submitXRayTestForm(form) {
    this.patientService.patientTestsDetails['XRayTest'] = form.value['XRayTest'];
    this.patientService.patientTestsDetails['XRayTestDetails'] = form.value['XRayTestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}




