import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-present-non-allergic-gerd',
  templateUrl: './present-non-allergic-gerd.component.html',
  styleUrls: ['./present-non-allergic-gerd.component.css']
})
export class PresentNonAllergicGerdComponent implements OnInit {
  gerdOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedGerd = false;
  gerdControlOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  selectedGerdControl = false;
  presentNonAllergicGerdForm: FormGroup;
  Gerd: boolean;
  GerdMedication: string;
  GerdHowLong: string;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.presentNonAllergicGerdForm = this.formBuilder.group({
      Gerd: this.formBuilder.control('', Validators.required),
      // GerdControl: this.formBuilder.control('', Validators.required),
      GerdHowLong: this.formBuilder.control('', Validators.required),
      GerdMedication: this.formBuilder.control('', Validators.required),
      GerdUnderControl: this.formBuilder.control('', Validators.required),
    })
    // console.log("this.patientService.patientNonAllergicGerdDetails======", this.patientService.patientNonAllergicGerdDetails);
    this.selectedGerd = this.patientService.patientNonAllergicGerdDetails.GERD;
    // console.log("this.selectedGerd====", this.patientService.patientNonAllergicGerdDetails.GERD);
    this.GerdHowLong = this.patientService.patientNonAllergicGerdDetails.GERDHowLong;
    this.GerdMedication = this.patientService.patientNonAllergicGerdDetails.GERDMedication;
    this.selectedGerdControl = this.patientService.patientNonAllergicGerdDetails.GERDUnderControl;
  }

  submitPresentNonAllergicGERD(form) {
    this.loading = true;
    this.patientService.patientNonAllergicGerdDetails['GERD'] = form.value['Gerd'];
    this.patientService.patientNonAllergicGerdDetails['GERDHowLong'] = form.value['GerdHowLong'];
    this.patientService.patientNonAllergicGerdDetails['GERDMedication'] = form.value['GerdMedication'];
    this.patientService.patientNonAllergicGerdDetails['GERDUnderControl'] = form.value['GerdUnderControl'];
    this.patientService.patientNonAllergicGerdDetails['IsActive'] = true;
    let apiData = this.patientService.patientNonAllergicGerdDetails;
    delete apiData.PresentNonAllergicId;
    console.log("apiData======", apiData);

    this.patientService.updatePatientNonAllergicGerdDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}
