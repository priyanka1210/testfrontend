export class Nativity {
     /*[Key]*/
     public NativityDomilcileId: number;
     /*[Required]*/
     public PatientDemographicsId: number;
     /*[Required]*/
     public BirthPlace: string;
     public CurrentPlace: string;
     /*[Required]*/
     public NoofYearsinCurrentPlace: number;
     public BroughtupPlace: string;
     public ParentsNativity: string;
     public ChangeinStay: boolean;
     public Stay1Place: string;
     public Stay1Years: number;
     public Stay2Place: string;
     public Stay2Years: number;
     public Stay3Place: string;
     public Stay3Years: number;
     public Stay4Place: string;
     public Stay4Years: number;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;
     /*[ForeignKey("PatientDemographicsId")]*/
     IsActive: true;
     // PatientDemographics: string;
}




