export class PrescriptionImmunoTherapy {
     //Immunotherapy-1
     Immunotherapy1Allergens:string[];
     Immunotherapy1Group:string[];
     Immunotherapy1Reactivity:string[];	
     Immunotherapy1Quantity:string[];
     Immunotherapy1Total:string;
 
     //Immunotherapy-2
     Immunotherapy2Allergens:string[];
     Immunotherapy2Group:string[];
     Immunotherapy2Reactivity:string[];	
     Immunotherapy2Quantity:string[];
     Immunotherapy2Total:string;
     ConcentrationVial5:string;
     ConcentrationVial6:string;
     ConcentrationVial7:string;
}
