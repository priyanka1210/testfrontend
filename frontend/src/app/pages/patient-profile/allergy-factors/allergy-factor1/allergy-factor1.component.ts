import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AFExposure } from './../../../../shared/AllergyFactors/afexposure';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AFContactFoodDrug } from './../../../../shared/AllergyFactors/afcontact-food-drug';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-allergy-factor1',
  templateUrl: './allergy-factor1.component.html',
  styleUrls: ['./allergy-factor1.component.css']
})
export class AllergyFactor1Component implements OnInit {

  foodAllergyOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedFoodAllergy = false;
  selectedFoodAllergyText = "";

  drugReactionOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' }, 
  ];
  selectedDrugReaction = false;
  selectedDrugReactionText = "";
  

  allergyFactor1Form: FormGroup;
  loading : boolean  = false;


  //  screen-2
  // AFExposure:AFExposure[]=[];
  // AFFoodAllergy:AFFoodAllergy[]=[];
  // FoodTextArea:string;
  // DrugReaction:boolean;
  // DrugTextArea:string;

  selectedExposureToDust : AFExposure[]=[];
  FoodReaction : boolean;
  FoodTextArea:string;
  DrugReaction:boolean;
  DrugTextArea:string;

  public HouseDustCheckBox:boolean = false;
  public PaperDustCheckBox:boolean = false;
  public WheatDustCheckBox:boolean = false;
  public RagiDustCheckBox:boolean = false;
  public WollenDustCheckBox:boolean = false;
  public OldCupBoardDustCheckBox:boolean = false;
  public HayDustCheckBox:boolean = false;
  public RiceDustCheckBox:boolean = false;
  public ChilliDustCheckBox:boolean = false;
  public BakeryDustCheckBox:boolean = false;
  public CottonStrawCheckBox:boolean = false;
  public StrawDustCheckBox:boolean = false;
  public JowarDustCheckBox:boolean = false;
  public SawRoadCheckBox:boolean = false;
  public RoadDustCheckBox:boolean = false;


  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor1Form = this.formBuilder.group({
     
      FoodAllergySelect: this.formBuilder.control('', Validators.required),
      FoodAllergyText: this.formBuilder.control('', Validators.required),
      DrugReactionSelect : this.formBuilder.control('', Validators.required),
      DrugReactionText : this.formBuilder.control('', Validators.required),
      Exposureto_HouseDust     :this.formBuilder.control('',Validators.required),
      Exposureto_PaperDust:this.formBuilder.control('',Validators.required),
      Exposureto_Wheatdust:this.formBuilder.control('',Validators.required),
      Exposureto_RagiDust :this.formBuilder.control('',Validators.required),
      Exposureto_WollenDust :this.formBuilder.control('',Validators.required),
      Exposureto_OldCupboardDust:this.formBuilder.control('',Validators.required),
      Exposureto_HayDust:this.formBuilder.control('',Validators.required),
      Exposureto_RiceDust:this.formBuilder.control('',Validators.required),
      Exposureto_ChilliDust :this.formBuilder.control('',Validators.required),
      Exposureto_BakeryDust :this.formBuilder.control('',Validators.required),
      Exposureto_CottonStraw:this.formBuilder.control('',Validators.required),
      Exposureto_StrawDust:this.formBuilder.control('',Validators.required),
      Exposureto_JowarDust:this.formBuilder.control('',Validators.required),
      Exposureto_SawRoad:this.formBuilder.control('',Validators.required),
      Exposureto_RoadDust :this.formBuilder.control('',Validators.required)

    });

    //loop through the exposures and set the appropriate check boxes
    for (let afExposure of this.patientService.patientAllergyDetails.AFExposure) {
      switch(afExposure.AllergyItemDescription) {
      case "House Dust":
          this.HouseDustCheckBox=true;
          break;
      case "Paper Dust":
          this.PaperDustCheckBox=true;
          break;
      case "Wheat Dust":
          this.WheatDustCheckBox=true;
          break;
      case "Ragi Dust":
          this.RagiDustCheckBox=true;
          break;
      case "Wollen Dust":
          this.WollenDustCheckBox=true;
          break;
      case "Old CupBoard Dust":
          this.OldCupBoardDustCheckBox=true;
          break;
      case "Hay Dust":
          this.HayDustCheckBox=true;
          break;
      case "Rice Dust":
          this.RiceDustCheckBox=true;
          break;
      case "Chilli Dust":
          this.ChilliDustCheckBox=true;
          break;
      case "Bakery Dust":
          this.BakeryDustCheckBox=true;
          break;
      case "Cotton Dust":
          this.CottonStrawCheckBox=true;
          break;
      case "Straw Dust":
          this.StrawDustCheckBox=true;
          break;
      case "Jowar Dust":
          this.JowarDustCheckBox=true;
          break;
      case "Saw Road":
          this.SawRoadCheckBox=true;
          break;
      case "Road Dust":
          this.RoadDustCheckBox=true;
          break;
      }
      this.selectedFoodAllergy = this.patientService.patientAllergyDetails.AFContactFoodDrug.FoodorBevarageAllergens;
      this.selectedFoodAllergyText = this.patientService.patientAllergyDetails.AFContactFoodDrug.FoodorBevarageAllergensDesc;
      this.selectedDrugReaction = this.patientService.patientAllergyDetails.AFContactFoodDrug.DrugReaction;
      this.selectedDrugReactionText = this.patientService.patientAllergyDetails.AFContactFoodDrug.DrugReactionDetails;
    }

  }



  submitAllergyFactor1Form(){
    this.loading = true;
    //alert('before if');
    if (this.HouseDustCheckBox)
    {
      let  AFExposureObj = new AFExposure();
      AFExposureObj.AllergyItemDescription = "House Dust";
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("House Dust");
      AFExposureObj.AllergyStatus = this.HouseDustCheckBox;

      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.PaperDustCheckBox)
    {
      let  AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Paper Dust";
      AFExposureObj.MasterAllergyDataId= this.patientService.allergyDataNameToDataId.get("Paper Dust");
      AFExposureObj.AllergyStatus = this.PaperDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.WheatDustCheckBox)
    {
      let AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Wheat Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Wheat Dust");
      AFExposureObj.AllergyStatus = this.WheatDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.RagiDustCheckBox)
    {
      let AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Ragi Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Ragi Dust");
      AFExposureObj.AllergyStatus = this.RagiDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.WollenDustCheckBox)
    {
      var AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Wollen Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Wollen Dust");
      AFExposureObj.AllergyStatus = this.WollenDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.OldCupBoardDustCheckBox)
    {
      let AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Old CupBoard Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Old CupBoard Dust");
      AFExposureObj.AllergyStatus = this.OldCupBoardDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.HayDustCheckBox)
    {
      let  AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Hay Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Hay Dust");
      AFExposureObj.AllergyStatus = this.HayDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.RiceDustCheckBox)
    {
      let  AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Rice Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Rice Dust");
      AFExposureObj.AllergyStatus = this.RiceDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.ChilliDustCheckBox)
    {
      let AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Chilli Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Chilli Dust");
      AFExposureObj.AllergyStatus = this.ChilliDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.BakeryDustCheckBox)
    {
      let  AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Bakery Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Bakery Dust");
      AFExposureObj.AllergyStatus = this.BakeryDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.CottonStrawCheckBox)
    {
      let  AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Cotton Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cotton Dust");
      AFExposureObj.AllergyStatus = this.CottonStrawCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.StrawDustCheckBox)
    {
      let AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Straw Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Straw Dust");
      AFExposureObj.AllergyStatus = this.StrawDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.JowarDustCheckBox)
    {
      let AFExposureObj= new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Jowar Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Jowar Dust");
      AFExposureObj.AllergyStatus = this.JowarDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.SawRoadCheckBox)
    {
      let AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Road Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Road Dust");
      AFExposureObj.AllergyStatus = this.SawRoadCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }

    if (this.RoadDustCheckBox)
    {
      let AFExposureObj = new AFExposure();
      AFExposureObj.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      AFExposureObj.AllergyItemDescription = "Straw Dust";
      AFExposureObj.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Saw Dust");
      AFExposureObj.AllergyStatus = this.RoadDustCheckBox;
      this.selectedExposureToDust.push(AFExposureObj);
    }
    var allergyFactor1 = {};
    allergyFactor1["AFExposure"] = this.selectedExposureToDust;
    console.log(JSON.stringify(allergyFactor1))

    let afContactFoodDrug = new AFContactFoodDrug();
    if (this.selectedFoodAllergy == true) {
     afContactFoodDrug.FoodorBevarageAllergens = true;
     afContactFoodDrug.FoodorBevarageAllergensDesc = this.allergyFactor1Form.get('FoodAllergyText').value;
     afContactFoodDrug.DrugReaction = true;
     afContactFoodDrug.DrugReactionDetails = this.allergyFactor1Form.get('DrugReactionText').value;
    }
    allergyFactor1["AFContactFoodDrug"] = afContactFoodDrug;
    console.log(JSON.stringify(allergyFactor1));



    //alert('before api call');

    let allergyFactor0 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor0, allergyFactor1);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);


    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response); 
      this.loading = false;
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);
    },
    error => {
      //alert(error);
    });

  }

  }


