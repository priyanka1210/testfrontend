import { Component, OnInit, ChangeDetectionStrategy} from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  //changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-life-threaterning-allergy',
  templateUrl: './life-threaterning-allergy.component.html',
  styleUrls: ['./life-threaterning-allergy.component.css']
})
export class LifeThreaterningAllergyComponent implements OnInit {

  lifeThreaterningAllergyForm: FormGroup;
  
  selectedInsectBite = "";
  selectedLatexExposure = "";
  selectedFoodConsumption = "";
  selectedDrugIntake = "";
  selectedDrugInjection1 = "";
  selectedDrugInjection2 = "";
  loading = false;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.lifeThreaterningAllergyForm = this.formBuilder.group({

     MarriatalStatusSelect: this.formBuilder.control('', Validators.required),
     LatexExposureSelect: this.formBuilder.control('', Validators.required),
     FoodConsumptionSelect : this.formBuilder.control('', Validators.required),
     DrugIntakeSelect : this.formBuilder.control('', Validators.required),
     DrugInjectionSelect1 : this.formBuilder.control('', Validators.required),
     DrugInjectionSelect2 : this.formBuilder.control('', Validators.required),

    });

  }

  submitLifeThreaterningAllergyForm() {
    this.alertsService.showToast(true);
  }

}
