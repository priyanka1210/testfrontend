import { Component, OnInit } from '@angular/core';
import { PatientService } from './../../../shared/patientService.service';
import { stringify } from '@angular/core/src/render3/util';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';
import { PatientDetails } from './../../../shared/patient-details';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.scss']
})
export class SearchPatientComponent implements OnInit {


  searchPatientForm: FormGroup;
  //PatientPhone:number;
  form: FormGroup;
  expanded: boolean = false;
  loading: boolean = false;
  submitted = false;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.searchPatientForm = this.formBuilder.group({
      'Mobile': ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],

    });
    //this.isOpen = true;


    this.patientService.getAllMasterAllergyDataGroupNames().subscribe(response => {
      //console.log('Patient MasterAllergy Group Names', response); 
      for (var grp in response) {
        //console.log(response[grp]["masterAllergyDataGroupsId"] + " = " + response[grp]["masterAllergyDataGroupName"]);
        this.patientService.allergyGroupIdToGroupName.set(response[grp]["masterAllergyDataGroupsId"], response[grp]["masterAllergyDataGroupName"])
      }
    },
      error => {
        //alert(error);
      });

    // Indivisual Data Items

    this.patientService.getAllMasterAllergyDataItems().subscribe(response => {
      //console.log('Patient MasterAllergy DataItems Details', response); 
      for (var allergyDataItem in response) {
        //console.log(response[allergyDataItem]["masterAllergyDataId"] + " = " + response[allergyDataItem]["masterAllergyDataName"]);
        this.patientService.allergyDataIdToDataName.set(response[allergyDataItem]["masterAllergyDataId"], response[allergyDataItem]["masterAllergyDataName"]);
        this.patientService.allergyDataNameToDataId.set(response[allergyDataItem]["masterAllergyDataName"], response[allergyDataItem]["masterAllergyDataId"]);
        this.patientService.allergyDataNameToGroupId.set(response[allergyDataItem]["masterAllergyDataName"], response[allergyDataItem]["masterAllergyDataGroupsId"]);

      }
    },
      error => {
        //alert(error);
      });
  }

  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();

    }
  }
  //loading = false;

  // toggleLoadingAnimation() {
  //   this.loading = true;
  //   setTimeout(() => this.loading = false, 3000);
  // }
  get f() { return this.searchPatientForm.controls; }



  searchPatientByContactNumber() {

    this.submitted = true;

    // stop here if form is invalid
    if (this.searchPatientForm.invalid) {
      return;
    }

    // alert('SUCCESS!! :-)')
    console.log("Searching Patient By Contact Number");
    this.loading = true;
    //setTimeout(() => this.loading = false, 3000);
    //this.toggleLoadingAnimation();


    let PatientPhone = this.searchPatientForm.get('Mobile').value;
    //let PatientPhone = this.searchPatientForm.value;

    console.log("******" + PatientPhone);
    this.patientService.searchPatientDetails(PatientPhone).subscribe(response => {
      console.log('Patient Details after search', response);
      // console.log("response['PrickTests'],===", response['PrickTests']["PrickTestResults"]);
      this.loading = false;
      this.patientService.patientDemographicsDetails = response["PatientDemographics"];
      this.patientService.patientPhysicalDetails = response["PatientPhysicalDetails"];
      this.patientService.patientAllergyDetails = response["AllergyFactors"];
      this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"]["AllergyHistory"];
      this.patientService.patientRespiratoryDetails = response["ChestDetails"] ? response["ChestDetails"] : this.patientService.patientRespiratoryDetails;
      this.patientService.patientHabitDetails = response["PatientHabits"];
      this.patientService.patientBloodDetails = response["BloodDetails"] ? response["BloodDetails"] : this.patientService.patientBloodDetails;
      this.patientService.patientNativityDetails = response["Nativity"];
      this.patientService.patientDietDetails = response["DietDetails"] ? response["DietDetails"] : this.patientService.patientDietDetails;
      this.patientService.patientGastroIntestinalDetails = response["GastroIntestinal"];
      this.patientService.patientFemaleInfoDetails = response["FemaleInfoDetails"] ? response["FemaleInfoDetails"] : this.patientService.patientFemaleInfoDetails;
      this.patientService.patientENTDetails = response["ENTDetails"] ? response["ENTDetails"] : this.patientService.patientENTDetails;;
      this.patientService.patientTestsDetails = response["PatientTests"] ? response["PatientTests"] : this.patientService.patientTestsDetails;
      this.patientService.patientSkinDetails = response['SkinDetails'] ? response['SkinDetails'] : this.patientService.patientSkinDetails;
      this.patientService.patientBowelDetails = response['BowelDetails'] ? response["BowelDetails"] : this.patientService.patientBowelDetails;
      this.patientService.patientHairDetails = response['HairHeadacheDetails'] ? response['HairHeadacheDetails'] : this.patientService.patientHairDetails;
      this.patientService.patientPastPresentHistoryDetails = response['PastHistoryDetails'] ? response['PastHistoryDetails'] : this.patientService.patientPastPresentHistoryDetails;
      this.patientService.patientNonAllergicGerdDetails = response['PresentNonAllergics'] ? response['PresentNonAllergics'] : this.patientService.patientNonAllergicGerdDetails;
      this.patientService.patientFanalInitialAdviseDetails = response['PatientDiagnosticsDetails'] ? response['PatientDiagnosticsDetails'] : this.patientService.patientFanalInitialAdviseDetails;
      this.patientService.patientPrickTest = response['PrickTests'] ? response['PrickTests'] : this.patientService.patientPrickTest;


      this.router.navigate(['/pages/patient-profile/update-patient']);
    },
      error => {
        console.log('error occured', error);
        console.log("Error Value Displayed", error["error"]["value"]);
        this.loading = false;
        alert(error["error"]["value"]);
        //this.alertsService.showError(error);

        //alert(error["error"]["value"]);
      });







  }

}
