import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test7',
  templateUrl: './test7.component.html',
  styleUrls: ['./test7.component.css']
})
export class Test7Component implements OnInit {

  higherTestOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  higherTestForm: FormGroup;
  higherTest: boolean = false;
  loading = false;
  higherTestDetails: string;
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.higherTestForm = this.FormBuilder.group({
      HigherTest: this.FormBuilder.control('', Validators.required),
      HigherTestDetails: this.FormBuilder.control('', Validators.required)
    });
    this.higherTest = this.patientService.patientTestsDetails['HigherTest'];
    this.higherTestDetails = this.patientService.patientTestsDetails['HigherTestDetails'];
  }

  submitHigherTestForm(form) {
    this.patientService.patientTestsDetails['HigherTest'] = form.value['HigherTest'];
    this.patientService.patientTestsDetails['HigherTestDetails'] = form.value['HigherTestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    console.log("highrt test api data===", apiData);
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }

}
