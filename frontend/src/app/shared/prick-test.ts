export class PrickTest {
    /*[Key]*/
    public PrickTestId: number;
    /*[Required]*/
    public PatientDemographicsId: number;
    public PrickTestResults: string;
      /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: string;
}

// [Key]
// public int PrickTestId { get; set; }
// public string PrickTestResults { get; set; }
// public int CreatedByUserId { get; set; }
// public DateTime CreatedDateTime { get; set; }
// public bool IsActive { get; set; }
// [Required]
// public int PatientDemographicsId { get; set; }
// [ForeignKey("PatientDemographicsId")]
// public PatientDemographics PatientDemographics { get; set; }