import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ThemeModule } from '../@theme/theme.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PatientProfileComponent } from './patient-profile/patient-profile.component';
import { AddPatientComponent } from './patient-profile/add-patient/add-patient.component';
import { UpdatePatientComponent } from './patient-profile/update-patient/update-patient.component';
import { PatientReportComponent } from './patient-report/patient-report.component';
import { SearchPatientComponent } from './patient-profile/search-patient/search-patient.component';
import { AllPatientsComponent } from './patient-profile/all-patients/all-patients.component';
import { DoctorProfileComponent } from './doctor-profile/doctor-profile.component';
import { PatientService } from '../shared/patientService.service';
import { ReactiveFormsModule } from '@angular/forms';
import { AllergyFactor0Component } from './patient-profile/allergy-factors/allergy-factor0/allergy-factor0.component';
import { AllergyFactor1Component } from './patient-profile/allergy-factors/allergy-factor1/allergy-factor1.component';
import { AllergyFactor2Component } from './patient-profile/allergy-factors/allergy-factor2/allergy-factor2.component';
import { AllergyFactor3Component } from './patient-profile/allergy-factors/allergy-factor3/allergy-factor3.component';
import { AllergyFactor4Component } from './patient-profile/allergy-factors/allergy-factor4/allergy-factor4.component';
import { AllergyFactor5Component } from './patient-profile/allergy-factors/allergy-factor5/allergy-factor5.component';
import { AllergyFactor6Component } from './patient-profile/allergy-factors/allergy-factor6/allergy-factor6.component';
import { PatientContactDetailsComponent } from './patient-profile/patient-contact-details/patient-contact-details.component';
import { PatientPhysicalDetailsComponent } from './patient-profile/patient-physical-details/patient-physical-details.component';
import { LifeThreaterningAllergyComponent } from './patient-profile/allergy-factors/life-threaterning-allergy/life-threaterning-allergy.component';
import { AlcoholHabitsDetailsComponent } from './patient-profile/patient-habits/alcohol-habit/alcohol-habits-details.component';
// import { AlcoholHabitsEarlierComponent } from './patient-profile/patient-habits/alcohol-habits-earlier/alcohol-habits-earlier.component';
// import { ChewingHabitsDetailsComponent } from './patient-profile/patient-habits/chewing-habit/chewing-habits-details.component';
import { BloodDonationDetailsComponent } from './patient-profile/patient-blood-info/blood-donation/blood-donation-details.component';
// import { BloodReceivingDetailsComponent } from './patient-profile/patient-blood-info/blood-receiving/blood-receiving-details.component';
import { NativityAndDomicileComponent } from './patient-profile/nativity-and-domicile/nativity-and-domicile.component';
import { DiteSleepPhysicalActivityComponent } from './patient-profile/patient-dite-sleep-info/dite-sleep-physicalActivity.component';

import { RespiratorySystem1Component } from './patient-profile/respiratory-system/respiratory-system1/respiratory-system1.component';
import { RespiratorySystem2Component } from './patient-profile/respiratory-system/respiratory-system2/respiratory-system2.component';
import { RespiratorySystem3Component } from './patient-profile/respiratory-system/respiratory-system3/respiratory-system3.component';
import { PatientChestDetailsComponent } from './patient-profile/patient-chest-details/patient-chest-details.component';
import { AllergyHistory0Component } from './patient-profile/allergy-history/allergy-history0/allergy-history0.component';
import { AllergyHistory1Component } from './patient-profile/allergy-history/allergy-history1/allergy-history1.component';
import { StartPageComponent } from './start-page/start-page.component';
import { HttpClientModule, HttpErrorResponse } from '@angular/common/http';
import { AlertsService } from '../shared/alerts.service';
import { HttpModule } from '@angular/http';
import { EyeEarInfoComponent } from './patient-profile/eye-ear-info/eye-ear-info.component';
import { ThroatInfoComponent } from './patient-profile/throat-info/throat-info.component';
import { GastroIntestinalComponent } from './patient-profile/gastro-intestinal/gastro-intestinal.component';
import { FemalePersonalDetailsComponent } from './patient-profile/female-details/female-personal-details/female-personal-details.component';
import { FemalePersonalChildrenComponent } from './patient-profile/female-details/female-personal-children/female-personal-children.component';
import { Test1Component } from './patient-profile/Test/test1/test1.component';
import { Test2Component } from './patient-profile/Test/test2/test2.component';
import { Test3Component } from './patient-profile/Test/test3/test3.component';
import { Test4Component } from './patient-profile/Test/test4/test4.component';
import { Test5Component } from './patient-profile/Test/test5/test5.component';
import { Test6Component } from './patient-profile/Test/test6/test6.component';
import { Test7Component } from './patient-profile/Test/test7/test7.component';
import { Test8Component } from './patient-profile/Test/test8/test8.component';
import { Skin1Component } from './patient-profile/skin/skin1/skin1.component';
import { Skin2Component } from './patient-profile/skin/skin2/skin2.component';
import { BowelmomentUrinationComponent } from './patient-profile/bowelmoment-urination/bowelmoment-urination.component';
import { HairDetailsComponent } from './patient-profile/hair-details/hair-details.component';
import { PreviousSurgeryInformationComponent } from './patient-profile/previous-surgery-information/previous-surgery-information.component';
import { PastHistoryComponent } from './patient-profile/past-history/past-history.component';
import { PresentNonAllergicComponent } from './patient-profile/Non-Allergic-Gerd/present-non-allergic/present-non-allergic.component';
import { HeadacheInfoComponent } from './patient-profile/headache-info/headache-info.component';
import { PresentNonAllergicGerdComponent } from './patient-profile/Non-Allergic-Gerd/present-non-allergic-gerd/present-non-allergic-gerd.component';
// import { NoseComponent } from './patient-profile/nose/nose.component';
// import { InfectionPrescenceComponent } from './patient-profile/infection-prescence/infection-prescence.component';
import { InitialAdviceComponent } from './patient-profile/Initial-Final-Advice/initial-advice/initial-advice.component';
import { FinalAdviceComponent } from './patient-profile/Initial-Final-Advice/final-advice/final-advice.component';
import { FollowupDetailsComponent } from './patient-profile/Initial-Final-Advice/followup-details/followup-details.component';
import { PrognosisDetailsComponent } from './patient-profile/Prognosis-Provisional-Details/prognosis-details/prognosis-details.component';
import { ProvisionalDetailsComponent } from './patient-profile/Prognosis-Provisional-Details/provisional-details/provisional-details.component';
// import { NoseComponent } from './patient-profile/nose/nose.component';
// import { InfectionPrescenceComponent } from './patient-profile/infection-prescence/infection-prescence.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AuthInterceptor } from '../auth.interceptor';
import { AuthService } from '../shared/auth.service';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { PrickTestComponent } from './patient-profile/Test/prick-test/prick-test.component';
import { AdditionalComponent } from './patient-profile/additional/additional.component';
import { BloodReceivingDetailsComponent } from './patient-profile/patient-blood-info/blood-receiving-details/blood-receiving-details.component';
// import { FamilyHistoryComponent } from './patient-profile/family-history/family-history.component';
const PAGES_COMPONENTS = [
  PagesComponent,
];

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpModule,
  ],
  declarations: [
    PAGES_COMPONENTS,
    PatientProfileComponent,
    AddPatientComponent,
    UpdatePatientComponent,
    PatientReportComponent,
    SearchPatientComponent,
    AllPatientsComponent,
    DoctorProfileComponent,
    AllergyFactor0Component,
    AllergyFactor1Component,
    AllergyFactor2Component,
    AllergyFactor3Component,
    AllergyFactor4Component,
    AllergyFactor5Component,
    AllergyFactor6Component,
    PatientContactDetailsComponent,
    PatientPhysicalDetailsComponent,
    LifeThreaterningAllergyComponent,
    AllergyHistory0Component,
    AllergyHistory1Component,
    AlcoholHabitsDetailsComponent,
    // AlcoholHabitsEarlierComponent,
    // ChewingHabitsDetailsComponent,
    BloodDonationDetailsComponent,
    // BloodReceivingDetailsComponent,
    NativityAndDomicileComponent,
    DiteSleepPhysicalActivityComponent,
    RespiratorySystem1Component,
    RespiratorySystem2Component,
    RespiratorySystem3Component,
    PatientChestDetailsComponent,
    StartPageComponent,
    EyeEarInfoComponent,
    ThroatInfoComponent,
    GastroIntestinalComponent,
    FemalePersonalDetailsComponent,
    FemalePersonalChildrenComponent,
    Test1Component,
    Test2Component,
    Test3Component,
    Test4Component,
    Test5Component,
    Test6Component,
    Test7Component,
    Test8Component,
    Skin1Component,
    Skin2Component,
    BowelmomentUrinationComponent,
    HairDetailsComponent,
    PreviousSurgeryInformationComponent,
    PastHistoryComponent,
    PresentNonAllergicComponent,
    HeadacheInfoComponent,
    PresentNonAllergicGerdComponent,
    // NoseComponent,
    // InfectionPrescenceComponent,
    InitialAdviceComponent,
    FinalAdviceComponent,
    FollowupDetailsComponent,
    PrognosisDetailsComponent,
    ProvisionalDetailsComponent,
    PrickTestComponent,
    AdditionalComponent,
    BloodReceivingDetailsComponent,
    // FamilyHistoryComponent,



  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [PatientService,
    AlertsService,
    AuthenticationGuard,
    AuthService,
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }]
})
export class PagesModule {
}
