import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { PatientService } from './../../../../shared/patientService.service';
import { AllergyHistory } from './../../../../shared/allergy-history';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-allergy-history1',
  templateUrl: './allergy-history1.component.html',
  styleUrls: ['./allergy-history1.component.css']
})
export class AllergyHistory1Component implements OnInit {

  seasonOptions = [
    { value: 'true', name: 'YES' },
    { value: 'false', name: 'NO' }, 
  ];
  selectedSeason = 'false';

  summerOptions = [
    { value: 'true', name: 'YES' },
    { value: 'false', name: 'NO' }, 
  ];
  selectedSummerSeason = 'false';
  selectedSummer = '';

  winterOptions = [
    { value: 'true', name: 'YES' },
    { value: 'false', name: 'NO' }, 
  ];
  selectedWinterSeason = 'false';

  monsoonOptions = [
    { value: 'true', name: 'YES' },
    { value: 'false', name: 'NO' }, 
  ];
  selectedMonsoonSeason = 'false';
  loading = false;
  selectedSummerSymptoms;
  selectedWinterSymptoms;
  selectedMonsoonSymptoms;
 

  allergyHistory1Form : FormGroup;

  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    
    this.allergyHistory1Form = this.formBuilder.group({
      Season: this.formBuilder.control('', Validators.required),
      Summer : this.formBuilder.control('', Validators.required),
      SummerSymptoms : this.formBuilder.control('', Validators.required),
      Winter : this.formBuilder.control('', Validators.required),
      WinterSymptoms : this.formBuilder.control('', Validators.required),
      Monsoon : this.formBuilder.control('', Validators.required),
      MonsoonSymptoms : this.formBuilder.control('', Validators.required),
    });
   
    
     this.selectedSeason = this.patientService.patientAllergyHistoryDetails.Season;
     this.selectedSummerSeason =  this.patientService.patientAllergyHistoryDetails.Summer;
     this.selectedMonsoonSeason = this.patientService.patientAllergyHistoryDetails.Monsoon;
     this.selectedWinterSeason = this.patientService.patientAllergyHistoryDetails.Winter;
     this.selectedWinterSymptoms = this.patientService.patientAllergyHistoryDetails.WinterSymptoms;
     this.selectedMonsoonSymptoms = this.patientService.patientAllergyHistoryDetails.MonsoonSymptoms;
     this.selectedSummerSymptoms = this.patientService.patientAllergyHistoryDetails.SummerSymptoms;
    
  }

  submitAllergyHistory1Form() {
    this.loading = true;

    if (this.patientService.patientAllergyHistoryDetails === undefined) {
      console.log("Patient allergy history details not found");
      this.loading = false;
      return;
    }
    if (this.patientService.patientAllergyHistoryDetails === null) {
      console.log("Patient allergy history details are not present");
      this.loading = false;
      return;
    }
    if (Object.entries(this.patientService.patientAllergyHistoryDetails).length === 0) {
      console.log("Patient allergy history details are not present");
      this.loading = false;
      return;
    }

    let allergyHistory0 = this.patientService.patientAllergyHistoryDetails;

    let allergyHistory1 = new AllergyHistory();
    if (this.selectedSeason == 'true') {
      allergyHistory1.Season = this.allergyHistory1Form.get('Season').value;
        if(this.selectedSummerSeason == 'true'){
          allergyHistory1.Summer = this.allergyHistory1Form.get('Summer').value;
          allergyHistory1.SummerSymptoms = this.allergyHistory1Form.get('SummerSymptoms').value;
        }
        if(this.selectedWinterSeason == 'true'){
          allergyHistory1.Winter =  this.allergyHistory1Form.get('Winter').value;
          allergyHistory1.WinterSymptoms = this.allergyHistory1Form.get('WinterSymptoms').value;
        }
        if(this.selectedMonsoonSeason == 'true'){
          allergyHistory1.Monsoon =  this.allergyHistory1Form.get('Monsoon').value;
          allergyHistory1.MonsoonSymptoms = this.allergyHistory1Form.get('MonsoonSymptoms').value;
        }

        var allergyHistoryDetails = Object.assign(allergyHistory0, allergyHistory1);
        delete allergyHistoryDetails["CreatedDateTime"];
        delete allergyHistoryDetails["AllergyHistoryId"];
        delete allergyHistoryDetails["CreatedByUserId"];
        //console.log('Allergy History Details', allergyHistoryDetails);

        this.patientService.createAllergyHistory(allergyHistoryDetails).subscribe(response => {
          console.log('Patient Allergy History 1 Form response', response); 
          this.loading = false;
          //this.patientService.patientAllergyHistoryDetails = response["AllergyHistory"];
          this.patientService.patientAllergyHistoryDetails = {...allergyHistory0, ...response};
          console.log('Patient Allergy History 1 Form response',this.patientService.patientAllergyHistoryDetails); 
          this.alertsService.showToast(true);

        },
        error => {
         // alert(error);
        });
    }
    
    

  }





}
