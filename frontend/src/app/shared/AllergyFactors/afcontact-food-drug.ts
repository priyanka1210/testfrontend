export class AFContactFoodDrug {
     /*[Key]*/
     public AFContactFoodDrugId: number;
     /*[Required]*/
     public PatientDemographicsId: number;
     public ContactAllergens: boolean;
     public ContactAllegensDescription: string;
     public FoodorBevarageAllergens: boolean;
     public FoodorBevarageAllergensDesc: string;
     public DrugReaction: boolean;
     public DrugReactionDetails: string;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;
     /*[ForeignKey("PatientDemographicsId")]*/
}
