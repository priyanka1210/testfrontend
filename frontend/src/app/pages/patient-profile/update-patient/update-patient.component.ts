import { Component, Input, OnInit } from '@angular/core';
import { NbDateService } from '@nebular/theme';
import { PatientDemographicsDetails } from './../../../shared/patient-demographics-details';

import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from './../../../shared/patientService.service';
import { AlertsService } from './../../../shared/alerts.service';
import { Router } from "@angular/router";
import { PhysicalDetails } from './../../../shared/physical-details';

@Component({
  selector: 'app-update-patient',
  templateUrl: './update-patient.component.html',
  styleUrls: ['./update-patient.component.css']
})
export class UpdatePatientComponent implements OnInit {

  patientDemogrpahicDetails = new PatientDemographicsDetails();
  patientContactPhysicalForm: FormGroup;
  // patientPhysicalForm :  FormGroup;
  expanded: boolean = false;
  loading: boolean = false;
  //genders = [{'value':'Male', 'name':'Male'}, {'value':'Female', 'name': 'Female'}, {'value':'Transgender', 'name': 'Transgender'}];

  occupationSymptomsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedSymptomsAtOccupation = this.patientService.patientPhysicalDetails['SymptomsAtOccupation'];

  sunExposureOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedExposureToSun = this.patientService.patientPhysicalDetails['ExposureToSun'];
  SymptomsSelectedAtOccupation = this.patientService.patientPhysicalDetails['SymptomsSelectedAtOccupation'];

  alcoholHabitsOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];

  alcoholHabitsEarlierOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  selectedAlcoholHabits = false;
  selectedAlcoholHabitsEarlier = false;
  min: Date;
  max: Date;

  selectedGender = "";
  selectedMaritalStatus = "";
  selectedOccupation = "";
  selectedBodySize = "";
  selectedSkinColour = "";
  OccupationSymptomOption: boolean;
  SunExposureOption: boolean;
  MentalStatusObj: Object[] = [
    { value: 1, name: 'Normal' },
    { value: 2, name: 'Anxious' },
    { value: 3, name: 'Tense' },
    { value: 4, name: 'Depressed' },
  ];
  public selectedMentalStatus: Object[];
  submitted = false;
  patientContactDetails = ["FirstName", "MiddleName", "LastName", "Gender", "MaritalStatus", "Occupation", "EmailId", "DateofBirth", "Mobile", "SecondaryContactNumber", "ReferringDoctorName", "ReferringDoctorContactNumber", "Address1", "Address2"];
  patientPhysicalDetails = ["Size", "Color", "Height", "Weight", "SymptomsAtOccupation", "SymptomsSelectedAtOccupation", "ExposureToSun"];

  updatedFirstName = this.patientService.patientDemographicsDetails['FirstName'];
  MentalStatus: any;

  constructor(protected dateService: NbDateService<Date>, private formBuilder: FormBuilder, public patientService: PatientService, private alertsService: AlertsService, private router: Router) {
    this.min = this.dateService.addMonth(this.dateService.today(), -1200);
    this.max = this.dateService.addMonth(this.dateService.today(), 0);

  }

  @Input() ReadOnly: boolean = true;
  Drinking: string;
  Changes: string;

  ngOnInit() {


    if (this.selectedAlcoholHabits == true) {
      alert(this.selectedAlcoholHabits);
    }
    this.patientContactPhysicalForm = this.formBuilder.group({
      PatientDemographicsId: (this.patientService.patientDemographicsDetails) ? this.patientService.patientDemographicsDetails.PatientDemographicsId : 0,
      FirstName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      MiddleName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      LastName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      Mobile: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      EmailId: ['', [Validators.required, Validators.email]],
      // DateOfBirth:new FormControl(new Date(), Validators.required),
      DateOfBirth: ['', Validators.required],
      SecondaryContactNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      ReferringDoctorName: ['', Validators.compose([Validators.required, Validators.maxLength(50), Validators.pattern('^[a-zA-Z0-9 ]*$')])],
      ReferringDoctorContactNumber: ['', Validators.compose([Validators.required, Validators.maxLength(10), Validators.pattern('^[7-9][0-9]{9}$')])],
      Address1: this.formBuilder.control('', Validators.required),
      Address2: this.formBuilder.control('', Validators.required),

      // PatientDemographicsId : (this.patientService.patientDemographicsDetails) ? this.patientService.patientDemographicsDetails.PatientDemographicsId : 0,
      PatientPhysicalDetailsId: this.patientService.patientPhysicalDetails != null ? this.patientService.patientPhysicalDetails.PatientPhysicalDetailsId : 0,
      Gender: this.formBuilder.control('', Validators.required),
      MaritalStatus: this.formBuilder.control('', Validators.required),
      Occupation: this.formBuilder.control('', Validators.required),
      Size: this.formBuilder.control('', Validators.required),
      Color: this.formBuilder.control('', Validators.required),
      Height: ['', Validators.required],
      Weight: ['', Validators.required],
      SymptomsAtOccupation: this.formBuilder.control(''),
      SymptomsSelectedAtOccupation: this.formBuilder.control(''),
      ExposureToSun: this.formBuilder.control('', Validators.required),
      MentalStatus: this.formBuilder.control('', Validators.required),

    });



    //this.patientContactPhysicalDetailsForm.reset();
    /*
    console.log("Received patient demographic details:" + JSON.stringify(this.patientService.patientDemographicsDetails));
    //console.log("Patients Demographics Key", JSON.stringify(this.patientService.patientPhysicalDetails['PatientDemographics']));
    console.log("Patients Demographics Key with FirstName", this.patientService.patientDemographicsDetails['FirstName']);
    //console.log("Patients Demographics Key with Gender", this.patientService.patientDemographicsDetails['Gender']);
    console.log("Patients Demographics Key with Height", this.patientService.patientPhysicalDetails['Height']);
    console.log("Patients Demographics Key with Gender", this.patientService.patientDemographicsDetails['Gender']);
    */


    //   console.log("PatientPhysicalForm does not exist");
    //   return;
    // }

    //**June27 */
    // if (this.patientService.patientDemographicsDetails != null) {
    //     this.patientContactForm.patchValue({'FirstName':this.patientService.patientDemographicsDetails['FirstName'], 'MiddleName':this.patientService.patientDemographicsDetails['MiddleName'],'LastName':this.patientService.patientDemographicsDetails['LastName'],'Mobile':this.patientService.patientDemographicsDetails['Mobile'],'EmailID':this.patientService.patientDemographicsDetails['EmailID'],'DateofBirth':this.patientService.patientDemographicsDetails['DateofBirth'],
    //     'SecondaryContactNumber':this.patientService.patientDemographicsDetails['SecondaryContactNumber'],'ReferringDoctorName':this.patientService.patientDemographicsDetails['ReferringDoctorName'],'ReferringDoctortNumber':this.patientService.patientDemographicsDetails['ReferringDoctortNumber'],'Address1':this.patientService.patientDemographicsDetails['Address1'],'Address2':this.patientService.patientDemographicsDetails['Address2']});
    //     //this.disableForm()
    //     //this.patientDemogrpahicDetails.Gender = this.patientPhysicalForm.get('Gender').value;
    //     //let exposureToSunValue = this.patientContactForm.value.ExposureToSun;
    //     //console.log("Gender Value",this.patientDemogrpahicDetails.Gender);
    //     //this.patientDemogrpahicDetails['Gender'] = this.patientPhysicalForm.get('Gender').value;
    //     //console.log('Gender Value', this.patientDemogrpahicDetails.Gender);
    //     if (this.patientService.patientPhysicalDetails != null) {
    //       this.patientPhysicalForm.patchValue({'Height': this.patientService.patientPhysicalDetails['Height'], 'Weight': this.patientService.patientPhysicalDetails['Weight']});
    //     }
    //     this.selectedGender = this.patientService.patientDemographicsDetails['Gender'];
    //     this.selectedMaritalStatus = this.patientService.patientDemographicsDetails['MarriatalStatus'];
    //     this.selectedOccupation = this.patientService.patientDemographicsDetails['Occupation'];
    // } **June 27

    // if(this.patientService.patientPhysicalDetails['BodySize'] === undefined){
    //   this.selectedBodySize = '';
    // }
    //  if (this.patientService.patientPhysicalDetails != null) {
    //   this.selectedBodySize = this.patientService.patientPhysicalDetails['Size'];
    //   this.selectedSkinColour = this.patientService.patientPhysicalDetails['Color'];
    //  }
    //this.symptomsAtOccupation = this.patientService.patientPhysicalDetails['SymptomsAtOccupation'];

    let mentalStatus = JSON.parse(this.patientService.patientPhysicalDetails['MentalStatus']);
    this.selectedMentalStatus = this.MentalStatusObj.filter(asp => mentalStatus.map(psp => psp['value']).indexOf(asp['value']) !== -1);

    console.log("Patient details set in UI successfully.")

    this.patientService.getAllMasterAllergyDataItems()
      .subscribe(response => {
        console.log('Patient MasterAllergy DataId Details', response);
      },
        error => {
          alert(error);
        });

  }
  change() {
    alert(this.selectedAlcoholHabits);
  }
  _keyPress(event: any) {
    const pattern = /[0-9]/;
    let inputChar = String.fromCharCode(event.charCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();

    }
  }

  get f() {
    return this.patientContactPhysicalForm.controls;

  }

  // get Pf() 
  // { 
  //   return this.patientPhysicalForm.controls; 

  // }


  // submitPatientContactDetails(){

  //   this.submitted = true;

  //       // stop here if form is invalid
  //       if (this.patientContactPhysicalForm.invalid) {
  //           return;
  //       }
  //       this.loading = true;
  //       console.log('Submit Patient Contact Details');
  //       console.log('Patient Demographic Id',this.patientService.patientDemographicsDetails.PatientDemographicsId);
  //       this.patientService.updatePatientContactDetails(this.patientContactPhysicalForm.value)
  //         .subscribe(data => {
  //           console.log("Patient Contact  Details",data);
  //           this.loading = false;
  //           this.alertsService.showToast(true);
  //           //this.patientPhysicalDetails = data;
  //         // this.patientService.patientDemographicsDetails = data;
  //         // console.log("Add Patient Deatails Response",this.patientService.patientDemographicsDetails);
  //         },
  //         error => {
  //          // alert(error);
  //         });
  // }


  submitPatientContactPhysicalDetails(form) {
    console.log("submit clicked==========");
    this.submitted = true;

    // stop here if form is invalid
    // if (this.patientContactPhysicalForm.invalid) {
    //   return;
    // }

    let patientDemographicsData = new PatientDemographicsDetails();
    for (var field in this.patientContactDetails) {
      if (this.patientContactPhysicalForm.get(this.patientContactDetails[field]) === null) {
        console.log("Field name :" + this.patientContactDetails[field] + " Value is null");
      } else {
        console.log("Field name :" + this.patientContactDetails[field] + " Value: " + this.patientContactPhysicalForm.get(this.patientContactDetails[field]).value);
        patientDemographicsData[this.patientContactDetails[field]] = this.patientContactPhysicalForm.get(this.patientContactDetails[field]).value;
      }


    }
    console.log("*****" + patientDemographicsData);

    let patientPhysialData = new PhysicalDetails();
    for (var field in this.patientPhysicalDetails) {
      if (this.patientContactPhysicalForm.get(this.patientPhysicalDetails[field]) === null) {
        console.log("Field name :" + this.patientPhysicalDetails[field] + " Value is null");
      } else {
        console.log("Field name :" + this.patientPhysicalDetails[field] + " Value: " + this.patientContactPhysicalForm.get(this.patientPhysicalDetails[field]).value);
      }
      patientPhysialData[this.patientPhysicalDetails[field]] = this.patientContactPhysicalForm.get(this.patientPhysicalDetails[field]).value;
    }

    // this.patientService.patientPhysicalDetails.MentalStatus = form.value['MentalStatus'] ? JSON.stringify(form.value.MentalStatus) : JSON.stringify(this.selectedMentalStatus)
    // patientPhysialData[this.MentalStatus] = this.patientService.patientPhysicalDetails.MentalStatus;

    console.log(" this.patientService.patientPhysicalDetails..............", this.patientService.patientPhysicalDetails);
    this.patientService.patientPhysicalDetails.MentalStatus = form.value['MentalStatus'] ? JSON.stringify(form.value.MentalStatus) : JSON.stringify(this.selectedMentalStatus)

    patientPhysialData['MentalStatus'] = this.patientService.patientPhysicalDetails.MentalStatus;

    console.log("*****" + patientPhysialData);


    console.log("Create Patient Form Submitted with FormBuilder", this.patientContactPhysicalForm.value);




    // this.loading = true;
    console.log("Patient P/C :" + this.patientContactPhysicalForm.value);
    this.loading = true;
    console.log('Submit Patient Physical Details');
    this.patientService.updatePatientPhysicalDetails(patientDemographicsData, patientPhysialData)
      .subscribe(response => {
        console.log("Patient Contact Details", response);
        this.loading = false;
        this.patientService.patientDemographicsDetails = response["PatientDemographics"];//response["patientDemographics"];
        this.patientService.patientPhysicalDetails = response["PatientPhysicalDetails"];//response["patientPhysicalDetails"];
        this.alertsService.showToast(true);
      },
        error => {
          // alert('Server Error');
        });
  }




  clickOnBack() {
    this.router.navigate(['/pages/patient-profile/search-patient']);
  }



}
