import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-test1',
  templateUrl: './test1.component.html',
  styleUrls: ['./test1.component.css']
})
export class Test1Component implements OnInit {

  urineOptions = [
    { value: true, name: 'YES' },
    { value: false, name: 'NO' },
  ];
  urineTestForm: FormGroup;
  selectedUrine: boolean = false;
  urineDetails: string = '';
  constructor(private patientService: PatientService, private FormBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;
  ngOnInit() {
    this.urineTestForm = this.FormBuilder.group({
      UrineTest: this.FormBuilder.control('', Validators.required),
      UrineTestDetails: this.FormBuilder.control('', Validators.required)
    });

    this.selectedUrine = this.patientService.patientTestsDetails['UrineTest'];
    this.urineDetails = this.patientService.patientTestsDetails['UrineTestDetails'];
  }

  submitUrineTestForm(form) {
    console.log("form======", form);
    this.patientService.patientTestsDetails['UrineTest'] = form.value['UrineTest'];
    console.log("this.patientService.patientTestsDetails['UrineTest']", this.patientService.patientTestsDetails['UrineTest']);
    this.patientService.patientTestsDetails['UrineTestDetails'] = form.value['UrineTestDetails'];
    this.loading = true;
    let apiData = this.patientService.patientTestsDetails;
    delete apiData.PatientTestId;
    console.log("apiData===", apiData);
    this.patientService.updatepatientTests(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
