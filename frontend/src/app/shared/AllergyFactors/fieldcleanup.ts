export class FieldCleanup {

    //removes id fields from the object
    
    fieldsToRemove = new Set(["AfBirdId", "CreatedDateTime", "CreatedByUserId", "AFAnimalId", "AFContactFoodDrugId","AfFoodAllergyId","AfFurnitureId","AfHouseId","AFInsectId", "AFMattressId","AFPillowId", "AfBlanketId", "AfExposureId", "AllergyHistoryId", "RespiratoryId"]);

    removeIdFields(obj: any):any {
        for (var key in obj) {
            if (this.fieldsToRemove.has(key)) {
                delete obj[key];
                continue;
            }
            if (typeof(obj[key]) === "object") {
                this.removeIdFields(obj[key]);
            }
        }
        return obj;
    }

}