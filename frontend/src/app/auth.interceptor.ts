import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor, HttpHeaders, HttpEvent } from '@angular/common/http';
import { AuthService } from './shared/auth.service';
import { Observable } from 'rxjs';
@Injectable({
    providedIn: 'root'
})

export class AuthInterceptor implements HttpInterceptor {

    constructor(private authService: AuthService) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // TODO: make this interceptor work only on IDOCTOR API calls
        // TODO: Logout the user incase the token is expired

        console.log('[INFO] Intercepted request: ', req);
        if (req.url.includes('functionapp20190515071947') && req.url.indexOf('.auth/login/aad') == -1) {
            const accessToken = localStorage.getItem('adal.access.token.key2764513c-9a24-4b1c-bc8a-35d74db9c67a');
            const authToken = localStorage.getItem('adal.auth.token.key2764513c-9a24-4b1c-bc8a-35d74db9c67a');
            if (authToken) {
                const authReq = req.clone({
                    headers: new HttpHeaders({
                        'X-ZUMO-AUTH': authToken,
                        'Clinic-Id': this.authService.getClinicId(accessToken)
                    })
                });
                return next.handle(authReq);
            } else {
                this.authService.getAuthenticationToken(accessToken)
                    .subscribe((response: any) => {
                        localStorage.setItem('adal.auth.token.key2764513c-9a24-4b1c-bc8a-35d74db9c67a', response.authenticationToken)
                        const authReq = req.clone({
                            headers: new HttpHeaders({
                                'X-ZUMO-AUTH': response.authenticationToken,
                                'Clinic-Id': this.authService.getClinicId(accessToken)
                            })
                        });
                        return next.handle(authReq);
                    }, error => {
                        console.log('[ERROR] Interceptor Error', error);
                    });
            }
        }
        else {
            return next.handle(req);
        }
    }
}
