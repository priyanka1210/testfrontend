import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { PatientService } from '../../../../shared/patientService.service';
import { AlertsService } from './../../../../shared/alerts.service';

@Component({
  selector: 'app-followup-details',
  templateUrl: './followup-details.component.html',
  styleUrls: ['./followup-details.component.css']
})
export class FollowupDetailsComponent implements OnInit {

  followupDetailsForm: FormGroup;
  FollowupDetails: string;
  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }
  loading = false;

  ngOnInit() {
    this.followupDetailsForm = this.formBuilder.group({
      FollowupDetails: this.formBuilder.control('', Validators.required),
    })
    this.FollowupDetails = this.patientService.patientFanalInitialAdviseDetails.FollowupDetails;
    console.log("this.InitialAdvise=====", this.FollowupDetails);

  }
  submitFollowupDetailsForm(form) {
    this.loading = true;
    // this.patientService.patientNonAllergicGerdDetails['Diabetic'] = form.value['Diabetic'];
    this.patientService.patientFanalInitialAdviseDetails['FollowupDetails'] = form.value['FollowupDetails'];
    this.patientService.patientFanalInitialAdviseDetails['IsActive'] = true;
    let apiData = this.patientService.patientFanalInitialAdviseDetails;
    delete apiData.PatientDiagnosticsDetailId;
    console.log("apiData=========", apiData);
    this.patientService.updatePatientFanalInitialAdviseDetails(apiData).subscribe(response => {
      this.loading = false;
      this.alertsService.showToast(true);
    });
  }
}
