export class GastroIntestine {
    /*[Key]*/
    public GastroIntestinalId: number;
    public Nausea: boolean;
    public Vomiting: boolean;
    public DoesVomitingGiveRelief: boolean;
    public AbdominalPain: boolean;
    public AbdominalPainArea: string;
    public AbdominalPainType: string;
    /*[Required]*/
    public PatientDemographicsId: number;
    /*[Required]*/
    public CreatedByUserId: number;
    /*[Required]*/
    public CreatedDateTime: Date;
    /*[ForeignKey("PatientDemographicsId")]*/
    public IsActive: boolean;
    public PatientDemographics: String;
}
