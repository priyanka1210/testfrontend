import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl, Validators } from "@angular/forms";
import { AFBlanket } from './../../../../shared/AllergyFactors/afblanket';

import { PatientService } from './../../../../shared/patientService.service';
import { AFAnimal } from './../../../../shared/afanimal';
import { AFPillow } from './../../../../shared/AllergyFactors/afpillow';
import { FieldCleanup } from './../../../../shared/AllergyFactors/fieldcleanup';
import { AlertsService } from './../../../../shared/alerts.service';


@Component({
  selector: 'app-allergy-factor5',
  templateUrl: './allergy-factor5.component.html',
  styleUrls: ['./allergy-factor5.component.css']
})
export class AllergyFactor5Component implements OnInit {

  animals = [{animal : 'Cow'}, {animal : 'Buffalo'}, {animal : 'Cat'}, {animal : 'Dog'}, {animal : 'Donkey'},{animal : 'Rabbit'}, {animal : 'Buffalo'}, {animal : 'Horse'}, {animal : 'Sheep'}, {animal : 'Rat'}];
  pillows = [{pillow : 'very old'}, {pillow : 'moderately old'},{pillow : 'new'}, {pillow : 'cotton'},{pillow : 'silk cotton'},{pillow : 'Foam'}, {pillow : 'Polyster'}, {pillow : 'rubberised coir'},{pillow : 'polyfill'}, {pillow : 'dacron'}];

  allergyFactor5Form: FormGroup;
  selectedAnimals : AFAnimal[]=[];
  selectedPillows : AFPillow[]=[];
  loading = false;

  //Animals
  public CowCheckBox:boolean = false;
  public BuffaloCheckBox:boolean = false;
  public CatCheckBox:boolean = false;
  public DogCheckBox:boolean = false;
  public DonkeyCheckBox:boolean = false;
  public RabbitCheckBox:boolean = false;
  public HorseCheckBox:boolean = false;
  public SheepCheckBox:boolean = false;
  public RatCheckBox:boolean = false;

 //Pillow
  public VeryOldCheckBox = false;
  public ModeratelyOldCheckBox:boolean = false;
  public NewCheckBox:boolean = false;
  public PillowCottonCheckBox:boolean = false;
  public SilkCottonCheckBox:boolean = false;
  public FoamCheckBox:boolean = false;
  public PillowPolysterCheckBox:boolean = false;
  public RubberisedCoirCheckBox:boolean = false;
  public PolyfilCheckBox:boolean = false;
  public DacronCheckBox:boolean = false;

  // Blankets

  blankets = [{blanket : 'Woolen'}, {blanket : 'Cotton'}, {blanket : 'Silk'}, {blanket : 'Synthetic'}, {blanket : 'Polyster'}];
  allergyFactor6Form: FormGroup;
 // AFBlanket:AFBlanket[]=[];
  selectedBlankets : AFBlanket[]=[];
  //loading = false;

  public WoollenCheckBox = false;
  public BlanketCottonCheckBox:boolean = false;
  public SilkCheckBox:boolean = false;
  public SyntheticCheckBox:boolean = false;
  public BlanketPolysterCheckBox:boolean = false;




  constructor(private patientService: PatientService, private formBuilder: FormBuilder, private alertsService: AlertsService) { }

  ngOnInit() {
    this.allergyFactor5Form = this.formBuilder.group({
    
    Animals_Cow: this.formBuilder.control('', Validators.required),
    Animals_Buffalo:this.formBuilder.control('', Validators.required),         
    Animals_Dog:this.formBuilder.control('', Validators.required),
    Animals_Donkey:this.formBuilder.control('', Validators.required),
    Animals_Rabbit:this.formBuilder.control('', Validators.required),
    Animals_Horse:this.formBuilder.control('', Validators.required),
    Animals_Sheep:this.formBuilder.control('', Validators.required),
    Animals_Rat:this.formBuilder.control('', Validators.required),
    // Pillow: this.formBuilder.control('', Validators.required),
    Pillow_VeryOld:this.formBuilder.control('', Validators.required),
    Pillow_ModeratelyOld:this.formBuilder.control('', Validators.required),
    Pillow_New:this.formBuilder.control('', Validators.required),
    Pillow_Cotton:this.formBuilder.control('', Validators.required),
    Pillow_SilkCotton:this.formBuilder.control('', Validators.required),
    Pillow_Foam:this.formBuilder.control('', Validators.required),
    Pillow_Polyester:this.formBuilder.control('', Validators.required),
    Pillow_RubberisedCoir:this.formBuilder.control('', Validators.required),
    Pillow_PolyFill:this.formBuilder.control('', Validators.required),
    Pillow_Dacron:this.formBuilder.control('', Validators.required),

   //  Blankets : this.formBuilder.control('', Validators.required),

   Blankets_Woollen:this.formBuilder.control('', Validators.required),
   Blankets_Cotton:this.formBuilder.control('', Validators.required),
   Blankets_Silk:this.formBuilder.control('', Validators.required),
   Blankets_Synthetic:this.formBuilder.control('', Validators.required),
   Blankets_Polyester:this.formBuilder.control('', Validators.required),
});
for (let animal of this.patientService.patientAllergyDetails.AFAnimal) {
  switch(animal.AllergyItemDescription) {
    
      case "Cow":
          this.CowCheckBox=true;
          break;
      case "Buffallow":
          this.BuffaloCheckBox=true;
          break;
      case "Dog":
          this.DogCheckBox=true;
          break;
      case "Donkey":
          this.DonkeyCheckBox=true;
          break;
      case "Rabbit":
          this.RabbitCheckBox=true;
          break;
      case "Horse":
          this.HorseCheckBox=true;
          break;
      case "Sheep":
          this.SheepCheckBox=true;
          break;
      case "Rat":
          this.RatCheckBox=true;
          break;
  }

  for (let pillow of this.patientService.patientAllergyDetails.AFPillow) {
    switch(pillow.AllergyItemDescription) {
      
        case "Very Old":
            this.VeryOldCheckBox=true;
            break;
        case "Moderately Old":
            this.ModeratelyOldCheckBox=true;
            break;
        case "New":
            this.NewCheckBox=true;
            break;
        case "Cotton":
            this.PillowCottonCheckBox=true;
            break;
        case "Silk Cotton":
            this.SilkCottonCheckBox=true;
            break;
        case "Foam":
            this.FoamCheckBox=true;
            break;
        case "Polyester":
            this.PillowPolysterCheckBox=true;
            break;
        case "Rubberised Coir":
            this.RubberisedCoirCheckBox=true;
            break;
        case "PollyFill":
            this.PolyfilCheckBox=true;
            break;
        case "Dacron":
            this.DacronCheckBox=true;
            break;           
    }

    for (let blanket of this.patientService.patientAllergyDetails.AFBlanket) {
      switch(blanket.AllergyItemDescription) {
        
          case "Woollen":
              this.WoollenCheckBox=true;
              break;
          case "Cotton":
              this.BlanketCottonCheckBox=true;
              break;
          case "Silk":
              this.SilkCheckBox=true;
              break;
          case "Synthetic":
              this.SyntheticCheckBox=true;
              break;
          case "Polyester":
              this.BlanketPolysterCheckBox=true;
              break;
           
        }
      }
    }
  }
}



  submitAllergyFactor5Form() {
    this.loading = true;
    // Animals
    if (this.CowCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Cow";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cow");//67;
      afAnimals.AllergyStatus = this.CowCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.BuffaloCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Buffallow";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Buffallow");//68;
      afAnimals.AllergyStatus = this.BuffaloCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.DogCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Dog";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Dog");//69;
      afAnimals.AllergyStatus = this.DogCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.DonkeyCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Donkey";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Donkey");//70;
      afAnimals.AllergyStatus = this.DonkeyCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.RabbitCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Rabbit";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Rabbit");//71;
      afAnimals.AllergyStatus = this.RabbitCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.HorseCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Horse";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Horse");//72;
      afAnimals.AllergyStatus = this.HorseCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.SheepCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Sheep";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Sheep");//73;
      afAnimals.AllergyStatus = this.SheepCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    if (this.RatCheckBox)
    {
      let afAnimals = new AFAnimal();
      afAnimals.AllergyItemDescription = "Rat";
      afAnimals.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afAnimals.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Rat");//74;
      afAnimals.AllergyStatus = this.RatCheckBox;
      this.selectedAnimals.push(afAnimals);
    }
    

    //Pillows
    if (this.VeryOldCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Very Old";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Very Old");//197;
      afPillows.AllergyStatus = this.VeryOldCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.ModeratelyOldCheckBox)
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Moderately Old";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Moderately Old");//198;
      afPillows.AllergyStatus = this.ModeratelyOldCheckBox;
      this.selectedPillows.push(afPillows);
    }
    if (this.NewCheckBox)
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "New";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("New");//199;
      afPillows.AllergyStatus = this.NewCheckBox;
      this.selectedPillows.push(afPillows);
    }

    if (this.PillowCottonCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Cotton";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cotton");//200;
      afPillows.AllergyStatus = this.PillowCottonCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.SilkCottonCheckBox)
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Silk Cotton";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Silk Cotton");//201;
      afPillows.AllergyStatus = this.SilkCottonCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.FoamCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Foam";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Foam");//202;
      afPillows.AllergyStatus = this.FoamCheckBox;
      this.selectedPillows.push(afPillows);
    }
    if (this.PillowPolysterCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Polyester";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Polyester");//203;
      afPillows.AllergyStatus = this.PillowPolysterCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.RubberisedCoirCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Rubberised Coir";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Rubberised Coir");//204;
      afPillows.AllergyStatus = this.RubberisedCoirCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.PolyfilCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "PollyFill";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("PollyFill");//205;
      afPillows.AllergyStatus = this.PolyfilCheckBox ;
      this.selectedPillows.push(afPillows);
    }
    if (this.DacronCheckBox )
    {
      let afPillows = new AFPillow();
      afPillows.AllergyItemDescription = "Dacron";
      afPillows.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afPillows.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Dacron");//206;
      afPillows.AllergyStatus = this.DacronCheckBox ;
      this.selectedPillows.push(afPillows);
    }

    // Blankents  (Here need to include code from allergy factor 6)

   

    if (this.WoollenCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Woollen";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Woollen");//55;
      afBlankets.AllergyStatus = this.WoollenCheckBox
      this.selectedBlankets.push(afBlankets);
    }
    if (this.BlanketCottonCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Cotton";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Cotton");//56;
      afBlankets.AllergyStatus = this.BlanketCottonCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.SilkCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Silk";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Silk");//57;
      afBlankets.AllergyStatus = this.SilkCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.SyntheticCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Synthetic";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Synthetic");//58;
      afBlankets.AllergyStatus = this.SyntheticCheckBox;
      this.selectedBlankets.push(afBlankets);
    }
    if (this.BlanketPolysterCheckBox)
    {
      let afBlankets = new AFBlanket();
      afBlankets.AllergyItemDescription = "Polyester";
      afBlankets.PatientDemographicsId = this.patientService.patientDemographicsDetails.PatientDemographicsId;
      afBlankets.MasterAllergyDataId = this.patientService.allergyDataNameToDataId.get("Polyester");//59;
      afBlankets.AllergyStatus = this.BlanketPolysterCheckBox;
      this.selectedBlankets.push(afBlankets);
    }

  //   //var allergyFactor6 = {};
  //   allergyFactor5["AFBlanket"] = this.selectedBlankets
  //   console.log(JSON.stringify(allergyFactor5));

  //   //let allergyFactor5 = this.patientService.patientAllergyDetails;// need to include
  //  // var allergyfactorDetails = Object.assign(allergyFactor5, allergyFactor6);// need to include
  //   allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    var allergyFactor5 = {};
    allergyFactor5["AFAnimal"] = this.selectedAnimals;
    allergyFactor5["AFPillow"] = this.selectedPillows;
    allergyFactor5["AFBlanket"] = this.selectedBlankets;
    console.log(JSON.stringify(allergyFactor5))

    let allergyFactor4 = this.patientService.patientAllergyDetails;
    var allergyfactorDetails = Object.assign(allergyFactor4, allergyFactor5);
    allergyfactorDetails = new FieldCleanup().removeIdFields(allergyfactorDetails);

    this.patientService.createAllergyFactors(allergyfactorDetails).subscribe(response => {
      console.log('Patient Allergy Details', response); 
      this.loading = false; 
      this.patientService.patientAllergyDetails = {...this.patientService.patientAllergyDetails, ...response};
      this.alertsService.showToast(true);
    },
    error => {
      //alert(error);
    });
  }

}
