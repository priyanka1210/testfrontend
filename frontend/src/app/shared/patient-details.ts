import { PatientDemographicsDetails } from './patient-demographics-details';
import { PhysicalDetails } from './physical-details';
import { DietDetails } from './diet-details';
import { MainComplaints } from './main-complaints';
import { ENTDetails } from './entdetails';
import { Habits } from './habits';
import { AllergyHistory } from './allergy-history';
import { Blood } from './blood';
import { Bowel } from './bowel';
import { Chest } from './chest';
import { Tests } from './tests';
import { Nativity } from './nativity';
import { FamilyHistory } from './family-history';
import { FemaleInfo } from './female-info';
import { NoseDetails } from './nose-details';
import { GastroIntestine } from './gastro-intestine';
import { PastPresentHistoryDetails } from './past-present-history-details';
import { SkinDetails } from './skin-details';
import { NonAllergicGerdDetails } from './non-allergic-gerd-details';
import { PrognosisDetails } from './prognosis-details';
import { PrescriptionImmunoTherapy } from './prescription-immuno-therapy';
import { FanalInitialAdviseDetails } from './fanal-initial-advise-details';
import { ThroatDetails } from './throat-details';
import { HairDetails } from './hair-details';
import { AllergyDetails } from './allergy-details';

export class PatientDetails {

    PatientDemographicsDetails:PatientDemographicsDetails;
    PatientPhysicalDetails:PhysicalDetails ;
    DietDetails:DietDetails;
    MainComplaints:MainComplaints ;
    ENTDetails:ENTDetails;
    Habits:Habits;
    AllergyFactors:AllergyDetails;
    AllergyHistory:AllergyHistory;
    BloodDetails:Blood;
    BowelmomentDetails:Bowel;
    ChestDetails:Chest;
    TestsDetails:Tests;
    Nativity:Nativity;
    FamilyHistory:FamilyHistory;
    FemaleInfo:FemaleInfo;
    GastroIntestinal:GastroIntestine;
    NoseDetails:NoseDetails;
    PastPresentHistoryDetails:PastPresentHistoryDetails;
    SkinDetails:SkinDetails;
    NonAllergicGerdDetails:NonAllergicGerdDetails;
    HairHeadacheDetails:HairDetails;
    // HeadAcheDetails:IHeadAche;
    ThroatDetails: ThroatDetails;
    InitialAndFinalAdvice:FanalInitialAdviseDetails;
    ImmunologyPrescription: PrescriptionImmunoTherapy;
    PrognosisProvisionalDetails:PrognosisDetails;
}


