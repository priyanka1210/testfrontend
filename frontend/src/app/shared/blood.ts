export class Blood {
     /*[Key]*/
     public BloodTransferId: number;
     /*[Required]*/
     public PatientDemographicsId: number;
     public BloodTestPerformed: boolean;
     public BloodTestDetails: string;
     public BloodReceived: boolean;
     public NoofTimesBloodReceived: number;
     public BloodReceivedDate: Date;
     public BloodReceivedImpact: boolean;
     public BloodReceivingProblems: string;
     public BloodDonated: boolean;
     public NoofTimesBloodDonated: number;
     public BloodDonatedDate: Date;
     public BloodDonationImpact: boolean;
     public BloodDonationProblems: string;
     /*[Required]*/
     public CreatedByUserId: number;
     /*[Required]*/
     public CreatedDateTime: Date;
     /*[ForeignKey("PatientDemographicsId")]*/
     public IsActive: boolean;
     public PatientDemographics: string;
}



